﻿using System;
using System.ComponentModel.DataAnnotations;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Models
{
    public class LoginModel : UserTypeModel
    {
        [Required]
        public string Token { get; set; }

        public string username { get; set; }

        public string password { get; set; }
    }
}