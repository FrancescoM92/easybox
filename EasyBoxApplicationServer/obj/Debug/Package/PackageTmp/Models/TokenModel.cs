﻿namespace EasyBoxApplicationServer.Models
{
    public class TokenModel
    {
        public string token { get; set; }
        public string azure { get; set; }
        public string accessToken { get; set; }
        public long id { get; set; }
        public long id_empl { get; set; }
        public long id_comp { get; set; }
    }

    public class CustomJWTokenModel
    {
        public string Result { get; set; }
        public string identity { get; set; }
    }
}