﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class Lavoratore
    {
        public Lavoratore()
        {
            Residenza = new Indirizzo();
            Domicilio = new Indirizzo();
        }
        public static Lavoratore Create(string Nome, string Cognome, string CodiceFiscale, string Email, string Telefono, string CodiceRiferimentoLavoratore)
        {
            return new Lavoratore()
            {
                Nome = Nome,
                Cognome = Cognome,
                CodiceFiscale = CodiceFiscale,
                Email = Email,
                Telefono = Telefono,
                CodiceRiferimentoLavoratore = CodiceRiferimentoLavoratore
            };
        }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string CodiceFiscale { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string CodiceRiferimentoLavoratore { get; set; }
        public Indirizzo Residenza { get; set; }
        public Indirizzo Domicilio { get; set; }
        public string AppRetailAbilitato { get; set; }
    }

    public class Lavoratori
    {
        public List<Lavoratore> lavoratori { get; set; }
    }

    public class DatiFirmaElettronicaAvanzata
    {
        public static DatiFirmaElettronicaAvanzata Create(string Cellulare, string Email)
        {
            return new DatiFirmaElettronicaAvanzata()
            {
                Cellulare = Cellulare,
                Email = Email
            };
        }
        public string Cellulare { get; set; }
        public string Email { get; set; }
    }
}
