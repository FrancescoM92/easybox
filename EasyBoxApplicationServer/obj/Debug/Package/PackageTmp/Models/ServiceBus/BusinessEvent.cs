﻿using EasyBoxApplicationServer.Models.ServiceBus.Classification;
using System;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class BusinessEvent<T>  where T:class 
    {
        public Guid EventId = Guid.NewGuid();
        public DateTime TimeStamp = DateTime.UtcNow;

        public Domains DomainType { get; }
        public EventTypes EventType { get; }
        public T EventData { get; }

        public BusinessEvent(Domains domainType, EventTypes eventType, T eventData)
        {
            this.DomainType= domainType;
            this.EventType = eventType;
            this.EventData = eventData ?? throw new ArgumentNullException(nameof(eventData));
        }  
    }
}
