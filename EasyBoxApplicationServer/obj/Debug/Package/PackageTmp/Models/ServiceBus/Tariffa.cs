﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class Tariffa
    {
        public string TipoContrattoLavoratore { get; set; }
        public string TipoCosto { get; set; }
        public double TassoINAIL { get; set; }
        public List<DettaglioTariffa> DettaglioTariffa { get; set; }
    }

    public class DettaglioTariffa
    {
        public string Categoria { get; set; }
        public string TipoOra { get; set; }
        public string Costo { get; set; }
        public double Margine { get; set; }
        public double Tariffa { get; set; }
        public string UnitaDiMisura { get; set; }
    }
}