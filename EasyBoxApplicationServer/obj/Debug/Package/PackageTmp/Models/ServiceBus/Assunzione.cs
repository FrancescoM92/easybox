﻿using System;
using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class Assunzione
    {
        public Assunzione()
        {
            Proroga = new List<ProrogaAssunzione>();
        }

        public string NumeroAssunzione { get; set; }
        public DateTime DataAssunzione { get; set; }
        public string Matricola { get; set; }
        public string NumeroSomministrazione { get; set; }
        public DateTime DataInizioMissione { get; set; }
        public DateTime? DataFineMissione { get; set; }
        public int CodiceRiferimentoLavoratore { get; set; }
        public string CodiceFiscaleLavoratore { get; set; }
        public List<ProrogaAssunzione> Proroga { get; set; }
        public RetribuzioneLavoratore RetribuzioneLavoratore { get; set; }
        public string IDUnitaDiBusiness { get; set; }
    }
    public class Assunzioni
    {
        public List<Assunzione> assunzioni { get; set; }
    }

}
