﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class BusinessUnit
    {
        public string IDFiliale { get; set; }
        public string IDUnitaDiBusiness { get; set; }
        public string UnitaDiBusiness { get; set; }
        public string CentroDiCosto { get; set; }
        public string CodiceINAIL { get; set; }
        public string CodiceCO { get; set; }
        public string Filiale { get; set; }
        public string Divisione { get; set; }
        public string AreaCommerciale { get; set; }
        public string Telefono { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public Indirizzo Indirizzo { get; set; }
    }
}