﻿using EasyBoxApplicationServer.Models.ServiceBus.Classification;
using System;
using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class Somministrazione
    {
        public string CodiceSomministrazione { get; set; }
        public string TipoSomministrazione { get; set; }
        public string CodiceRiferimentoCliente { get; set; }
        public string RagioneSocialeCliente { get; set; }
        public DateTime DataSomministrazione { get; set; }
        public DateTime DataInizioFornitura { get; set; }
        public DateTime? DataFineFornitura { get; set; }
        public string Qualifica { get; set; }
        public string Livello { get; set; }
        public string Mansione { get; set; }
        public string DettaglioMansione { get; set; }
        public string FullTime { get; set; }
        public SedeCliente SedeLavoro { get; set; }
        public List<Proroga> Proroga { get; set; }
        public RetribuzioneLavoratore RetribuzioneLavoratore { get; set; }
        public Tariffa Tariffa { get; set; }
        public decimal? NumeroOreFullTime { get; set; }
        public TipiPartTime? TipoPartTime { get; set; }
        public decimal? PercentualePartTime { get; set; }
        public decimal? NumeroOrePartTime { get; set; }
        public string IDUnitaDiBusiness { get; set; }

        public Somministrazione()
        {
            SedeLavoro = new SedeCliente();
            Proroga = new List<Proroga>();
        }
    }

    public class Somministrazioni
    {
        public List<Somministrazione> somministrazioni { get; set; }
    }

    public class SedeCliente
    {
        public string Via { get; set; }
        public string Comune { get; set; }
        public string CAP { get; set; }
        public string SiglaProvincia { get; set; }
        public string Nazione { get; set; }
        public string CodiceNazione { get; set; }
        public string CodiceComune { get; set; }
    }

    public class Proroga
    {
        public string NumeroContrattoProroga { get; set; }
        public DateTime DataProroga { get; set; }
        public DateTime DataInizioProroga { get; set; }
        public DateTime DataFineProroga { get; set; }
        public int NumeroLavoratori { get; set; }
    }

    public class ProrogaAssunzione
    {
        public string NumeroProroga { get; set; }
        public DateTime DataProroga { get; set; }
        public DateTime DataInizioProroga { get; set; }
        public DateTime DataFineProroga { get; set; }
    }
}
