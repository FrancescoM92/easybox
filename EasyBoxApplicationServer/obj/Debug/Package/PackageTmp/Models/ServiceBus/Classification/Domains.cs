﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EasyBoxApplicationServer.Models.ServiceBus.Classification
{
    public enum Domains
    {
        Clienti,
        Lavoratori,
        Somministrazioni,
        Assunzioni
    }
}
