﻿using System.ComponentModel;

namespace EasyBoxApplicationServer.Models.ServiceBus.Classification
{ 
    public enum EventTypes
    {
        [Description("NUOVO_CLIENTE")]
        ClienteUtilizzabile,
        LavoratoreUtilizzabile,
        SomministrazioneConsolidata,
        AssunzioneConsolidata
    }
}
