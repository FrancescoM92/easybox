﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class Cliente
    {
        public Cliente()
        {
            ElencoSedi = new List<Sede>();
        }
        public static Cliente Create(string RagioneSociale, string PartitaIva, string CodiceFiscale, string CodiceRiferimentoCliente, string Email, string Telefono, List<Sede> ElencoSedi)
        {
            return new Cliente()
            {
                RagioneSociale = RagioneSociale,
                PartitaIVA = PartitaIva,
                CodiceFiscale = CodiceFiscale,
                CodiceRiferimentoCliente = CodiceRiferimentoCliente,
                Email = Email,
                Telefono = Telefono,
                ElencoSedi = ElencoSedi
            };
        }
        public string RagioneSociale { get; set; }
        public string PartitaIVA { get; set; }
        public string CodiceFiscale { get; set; }
        public string CodiceRiferimentoCliente { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public FirmaElettronicaAvanzata FirmaElettronicaAvanzata { get; set; }
        public List<Sede> ElencoSedi { get; set; }
        public string IDUnitaDiBusiness { get; set; }
    }
    public class Clienti
    {
        public List<Cliente> clienti { get; set; }
    }
    public class Sede
    {
        public string CodiceSede { get; set; }
        public string Via { get; set; }
        public string Comune { get; set; }
        public string CAP { get; set; }
        public string SiglaProvincia { get; set; }
        public string Nazione { get; set; }
        public string CodiceNazione { get; set; }
        public string CodiceComune { get; set; }
        public string SedeLegale { get; set; }
    }

    public class FirmaElettronicaAvanzata 
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Email { get; set; }
        public string Cellulare { get; set; }
    }
}