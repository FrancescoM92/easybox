﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.ServiceBus;
using EasyBoxApplicationServer.Models.ServiceBus.Classification;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class AdministrationModel
    {
        public static AdministrationModel Create(Somministrazione somministrazione)
        {
            long hq = new DBHandler().insertHeadQuarter(somministrazione.SedeLavoro).Item1;
            ExtensionsModel exts = ExtensionsModel.CreateAdministration(somministrazione.Proroga);

            bool fullTime = false;
            if (somministrazione.FullTime.Equals("1"))
                fullTime = true;
            return new AdministrationModel()
            {
                administrationCode = somministrazione.CodiceSomministrazione,
                administrationDate = somministrazione.DataSomministrazione,
                businessNameCustomer = somministrazione.RagioneSocialeCliente,
                customerCode = somministrazione.CodiceRiferimentoCliente,
                Extensions = exts,
                headquarter = hq,
                task = somministrazione.Mansione,
                taskDetail = somministrazione.DettaglioMansione,
                fullTime = fullTime,
                fullTimeHoursNumber = somministrazione.NumeroOreFullTime,
                partTimeType = somministrazione.TipoPartTime != null ? (TipiPartTime)somministrazione.TipoPartTime : TipiPartTime.Nessuno,
                partTimePercent = somministrazione.PercentualePartTime,
                partTimeHoursNumber = somministrazione.NumeroOrePartTime,
                provisionStartDate = somministrazione.DataInizioFornitura,
                provisionEndDate = somministrazione.DataFineFornitura,
                contractLevel = somministrazione.Livello,
                qualification = somministrazione.Qualifica,
                businessUnitID = somministrazione.IDUnitaDiBusiness
            };
        }

        public long ID { get; set; }
        public string administrationCode { get; set; }
        public DateTime administrationDate { get; set; }
        public string customerCode { get; set; }
        public string businessNameCustomer { get; set; }
        public DateTime provisionStartDate { get; set; }
        public DateTime? provisionEndDate { get; set; }
        public string contractLevel { get; set; }
        public string qualification { get; set; }
        public string task { get; set; }
        public string taskDetail { get; set; }
        public bool fullTime { get; set; }
        public decimal? fullTimeHoursNumber { get; set; }
        public TipiPartTime? partTimeType { get; set; }
        public decimal? partTimePercent { get; set; }
        public decimal? partTimeHoursNumber { get; set; }
        public long headquarter { get; set; }
        public string businessUnitID { get; set; }

        public ExtensionsModel Extensions { get; set; }
    }

    public class AdministrationsModel
    {
        public List<AdministrationModel> administrations { get; set; }
    }
}