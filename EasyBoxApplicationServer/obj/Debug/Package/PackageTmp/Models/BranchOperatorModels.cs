﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class BranchOperatorModel
    {
        public long id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string bbToken { get; set; }
    }

    public class BranchOperatorsModel
    {
        public List<BranchOperatorModel> operators { get; set; }
    }
}