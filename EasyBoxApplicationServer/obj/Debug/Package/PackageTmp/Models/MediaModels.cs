﻿using EasyBoxApplicationServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class MediaModel
    {
        public static MediaModel Create(Media media)
        {
            MediaModel result = new MediaModel()
            {
                name = media.name,
                data = media.data,
            };

            if (media.Employee != null)
                result.employee = media.Employee;
            else
                result.company = media.Company;

            return result;
        }
        public string name { get; set; }
        public byte[] data { get; set; }
        public Employee employee { get; set; }
        public Company company { get; set; }
    }

    public class MediasModel
    {
        List<MediaModel> medias { get; set; }
    }
}