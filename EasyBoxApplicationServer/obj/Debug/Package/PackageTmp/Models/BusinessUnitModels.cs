﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.ServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class BusinessUnitModel
    {
        public static UnitaDiBusiness Create(BusinessUnit businessUnit)
        {
            Tuple<long, string> addressId = new DBHandler().insertAddress(new Address()
            {
                city = businessUnit.Indirizzo.Comune,
                street = businessUnit.Indirizzo.Via,
                zip = businessUnit.Indirizzo.CAP,
                provinceInitials = businessUnit.Indirizzo.SiglaProvincia                
            });
            return new UnitaDiBusiness()
            {
                AreaCommerciale = businessUnit.AreaCommerciale,
                CentroDiCosto = businessUnit.CentroDiCosto,
                CodiceCO = businessUnit.CodiceCO,
                CodiceINAIL = businessUnit.CodiceINAIL,
                Email = businessUnit.Email,
                Divisione = businessUnit.Divisione,
                Fax = businessUnit.Fax,
                IDUnitaDiBusiness = businessUnit.IDUnitaDiBusiness,
                Telefono = businessUnit.Telefono,
                Filiale = businessUnit.Filiale,
                IDFiliale = businessUnit.IDFiliale != null ? long.Parse(businessUnit.IDFiliale) : 0,
                UnitaDiBusiness1 = businessUnit.UnitaDiBusiness,
                IDAddress = addressId.Item1
            };
        }
    }
}