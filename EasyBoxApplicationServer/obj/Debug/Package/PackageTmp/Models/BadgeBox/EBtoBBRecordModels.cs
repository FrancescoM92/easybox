﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.BadgeBox
{
    public class EBtoBBRecordModel
    {
        public int id_record { get; set; }
        public int id_empl { get; set; }
        public long checkin { get; set; }
        public int hours { get; set; }
        public int month { get; set; }
        public int year { get; set; }
    }
}