﻿using EasyBoxApplicationServer.Utils;

namespace EasyBoxApplicationServer.Models.BadgeBox.Models
{
    public class BadgeBoxSwitchCompanyModel : BadgeBoxBaseModel
    {
        public BadgeBoxSwitchCompanyModel() : base()
        { }

        public long agency { get; set; }
    }

    public class BadgeBoxCompanyModel : BadgeBoxBaseModel
    {
        public BadgeBoxCompanyModel() : base()
        {
            timezone = Constants.TIMEZONE_IT;
        }

        public string name { get; set; }
        public string timezone { get; set; }
        public string email { get; set; }
        public string vat { get; set; }
    }
    public class BadgeBoxCreateCompanyModel : BadgeBoxBaseModel
    {
        public BadgeBoxCreateCompanyModel() : base()
        {
            timezone = Constants.TIMEZONE_IT;
            category = "25";
            country = "Italy";
        }

        public string name { get; set; }
        public string timezone { get; set; } // ex. Europe/Rome
        public string email { get; set; } // owner email
        public string vat { get; set; }
        public string a_email { get; set; } // agency email
        public string country { get; set; }
        public string city { get; set; }
        public string cap { get; set; }
        public string address { get; set; }
        public string category { get; set; }
    }
}