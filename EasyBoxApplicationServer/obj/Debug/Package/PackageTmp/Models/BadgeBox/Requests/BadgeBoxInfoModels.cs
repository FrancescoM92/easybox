﻿using static EasyBoxApplicationServer.Utils.BadgeBoxConstants;

namespace EasyBoxApplicationServer.Models.BadgeBox.Requests
{
    public class BadgeBoxInfoModel
    {
    }

    public class BadgeBoxCreateInfoModel : BadgeBoxBaseModel
    {
        public int id_empl { get; set; }
        public InfoTypeEnum info { get; set; }
        public string value { get; set; }
    }
}