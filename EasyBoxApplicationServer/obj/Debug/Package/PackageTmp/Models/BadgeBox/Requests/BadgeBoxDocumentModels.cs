﻿using System;

namespace EasyBoxApplicationServer.Models.BadgeBox.Models
{
    public class BadgeBoxDocumentModel : BadgeBoxBaseModel
    {
        public BadgeBoxDocumentModel() : base()
        {
            id_folder = -1;
            type = 3;
        }

        public int id_empl { get; set; }

        public int id_folder { get; set; }

        public int type { get; set; }

        public string title { get; set; }

        public string file { get; set; }
    }

    public class BadgeBoxGetFilesModel : BadgeBoxBaseModel
    {
        public BadgeBoxGetFilesModel() : base()
        { }

        public int id_empl { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
        public string type { get; set; }
    }
}