﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.BadgeBox.Requests
{
    public class BadgeBoxGetLastRecordModel : BadgeBoxBaseModel
    {
        public int type { get; set; } = -5;
        public int id_empl { get; set; }
    }

    public class BadgeBoxCheckInModel : BadgeBoxBaseModel
    {
        public string date { get; set; }
        public string apikey { get; set; }
        public string current_time { get; set; } //DateTime format yyyy-MM-dd HH:mm:ss
        public int id_task { get; set; }
        public int id_empl { get; set; }
    }

    public class BadgeBoxCheckOutModel : BadgeBoxBaseModel
    {
        public string date { get; set; }
        public string apikey { get; set; }
        public int id_empl { get; set; }
        public int id_task { get; set; }
        public int pause { get; set; }
        public string current_year { get; set; } //Date format "yyyy-MM-dd"
    }
    public class BadgeBoxInsertRecordModel : BadgeBoxBaseModel
    {
        public string apikey { get; set; }
        public int id_empl { get; set; }
        public string checkin { get; set; } // DateTime format "yyyy-MM-dd HH:mm:ss"
        public string checkout { get; set; }
    }
}