﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.BadgeBox.Responses
{
    public class BadgeBoxCreateRecordResponse : BadgeBoxGenericResponse
    {
        public int hours { get; set; }
    }

    public class BadgeBoxDeleteRecordResponse : BadgeBoxGenericResponse
    {
        public int hours { get; set; }
    }

    public class BadgeBoxGetRecordResponse : BadgeBoxGenericResponse
    {
        public List<Record> records { get; set; }
        public List<Holyday> holydays { get; set; }
        public int hours { get; set; }
    }

    public class BadgeBoxRecordsResponse
    {
        public List<BadgeBoxRecordResponse> records { get; set; }
    }

    public class BadgeBoxRecordResponse
    {
        public string id { get; set; }
        public string day { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
    }

    public class Holyday
    {
        public string id { get; set; }
        public string recursive { get; set; }
        public string start { get; set; }
        public string end { get; set; }
    }

    public class Record
    {
        public string id { get; set; }
        public string id_agency { get; set; }
        public string id_user { get; set; }
        public string day { get; set; }
        public string total_pause { get; set; }
        public string total_minutes { get; set; }
        public string worked_minutes { get; set; }
        public string checkin { get; set; }
        public string ckin_insert { get; set; }
        public string checkout { get; set; }
        public string ckout_insert { get; set; }
        public string id_stat { get; set; }
        public string total_hours { get; set; }
    }
}