﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.BadgeBox.Responses
{
    public class BadgeBoxGetTimeSheetResponse
    {
        public List<TimeSheetResponse> values { get; set; } = new List<TimeSheetResponse>();
    }

    public class TimeSheetResponse
    {
        public string sync_qb { get; set; }
        public string state { get; set; }
        public string from_user { get; set; }
        public string log { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public int id { get; set; }
        public int level { get; set; }
        public string approved { get; set; }
        public bool isConfirmed { get; set; }
    }

    public class BadgeBoxCreateTimeSheetResponse
    {
        public int id { get; set; }
    }

    public class BadgeBoxGetOverallTimeSheetsResponse
    {
        List<BadgeBoxGetOverallTimeSheetResponse> values { get; set; }
    }

    public class BadgeBoxGetOverallTimeSheetResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public int current_agency { get; set; }
    }

    public class BadgeBoxDownloadTimesheetResponse
    {
        public string src { get; set; }
    }
}