﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.BadgeBox.Responses
{
    public class BBDocument
    {
        public string id { get; set; }

        public string id_owner { get; set; }

        public string id_folder { get; set; }

        public string type { get; set; }

        public string title { get; set; }

        public string src { get; set; }

        public string ext { get; set; }

        public string date { get; set; }

        //public string name { get; set; }
        //public string surname { get; set; }
    }

    public class BadgeBoxFilesResponse
    {
        public List<BBDocument> documents { get; set; }
    }
}