﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.BadgeBox.Responses
{
    public class BadgeBoxGetLastRecordResponse
    {
        public List<Value> values { get; set; }
    }

    public class Value
    {
        public int id { get; set; }
    }
}