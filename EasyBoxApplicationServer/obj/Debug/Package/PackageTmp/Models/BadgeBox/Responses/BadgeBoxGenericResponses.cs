﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.BadgeBox
{
    public class BadgeBoxGenericResponse
    {
        public int id { get; set; }
        public string MESSAGE { get; set; }
    }

    public class BadgeBoxGenericMessageResponse
    {
        public string MESSAGE { get; set; }
    }

    public class BadgeBoxErrorMessageResponse
    {
        public BadgeBoxGenericMessageResponse ERROR { get; set; }
    }
}