﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class AzureLogin
    {
        public string aud { get; set; }
        public string iss { get; set; }
        public int iat { get; set; }
        public int nbf { get; set; }
        public int exp { get; set; }
        public string aio { get; set; }
        public List<string> amr { get; set; }
        public string email { get; set; }
        public string idp { get; set; }
        public string ipaddr { get; set; }
        public string name { get; set; }
        public string nonce { get; set; }
        public string oid { get; set; }
        public string sub { get; set; }
        public string tid { get; set; }
        public string unique_name { get; set; }
        public string uti { get; set; }
        public string ver { get; set; }
    }
}