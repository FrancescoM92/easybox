﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.ServiceBus;
using System;
using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models
{
    public class SetCompanyIsActiveModel
    {
        public long id { get; set; }

        public bool isActive { get; set; }
    }

    public class CompanyModel
    {
        public static CompanyModel Create(Cliente body)
        {
            Tuple<long,string> addressId = new DBHandler().insertAddress(new Address()
            {
                city = body.ElencoSedi[0].Comune,
                street = body.ElencoSedi[0].Via,
                zip = body.ElencoSedi[0].CAP
            });
            return new CompanyModel()
            {
                companyCode = body.CodiceRiferimentoCliente,
                email = body.Email,
                name = body.RagioneSociale,
                addressId = addressId.Item1,
                vat = body.PartitaIVA,
                phone = body.Telefono
            };
        }

        public long id { get; set; }

        public string companyCode { get; set; }

        public string email { get; set; }

        public DateTime endDate { get; set; }

        public string functionsId { get; set; }

        public long addressId { get; set; }

        public string name { get; set; }

        public string logo { get; set; }

        public long ownerId { get; set; }

        public DateTime startDate { get; set; }

        public string vat { get; set; }

        public bool isActive { get; set; }

        public bool isBBSync { get; set; }

        public Address address { get; set; }

        public string phone { get; set; }
    }

    public class CompaniesModel
    {
        public List<CompanyModel> companies { get; set; }
    }

    public class UpdateImageModel
    {
        public int id { get; set; }

        public bool type { get; set; }

        public string token { get; set; }
    }

    public class UpdateTemporaryCompanyToCompanyModel
    {
        public long id { get; set; }

        public long ownerId { get; set; }
    }
}