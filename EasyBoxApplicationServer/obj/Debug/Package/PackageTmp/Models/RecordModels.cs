﻿using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class RecordModel
    {
        public static RecordModel Create(Record rec)
        {
            return new RecordModel()
            {
                day = rec.day,
                hours = int.Parse(rec.total_hours) / 60,
                id = long.Parse(rec.id)
            };
        }

        public long id { get; set; }
        public int hours { get; set; }
        public string day { get; set; }
    }

    public class RecordsModel
    {
        public static RecordsModel Create(BadgeBoxGetRecordResponse bbres)
        {
            List<RecordModel> result = new List<RecordModel>();
            int hourst = 0;
            if (bbres.records != null && bbres.records.Count > 0)
                foreach (Record rec in bbres.records)
                {
                    RecordModel tempRec = RecordModel.Create(rec);
                    result.Add(tempRec);
                    hourst += tempRec.hours;
                }
            return new RecordsModel()
            {
                records = result,
                totalHours = hourst
            };
        }
        public List<RecordModel> records { get; set; }
        public BadgeBoxGetTimeSheetResponse timeSheet { get; set; }
        public int totalHours { get; set; }
    }
}