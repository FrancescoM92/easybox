﻿using EasyBoxApplicationServer.Database;
using System;
using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models
{
    public class MessageModel
    {
        public static MessageModel Create(Message message)
        {
            MessageModel result = new MessageModel()
            {
                timeStamp = message.timeStamp,
                body = message.body,
                idFrom = message.idFrom,
                idTo = message.idTo,
                sentByEmployee = message.sentByEmployee     
            };
            result.employee = message.Employee;

            return result;
        }

        public DateTime timeStamp { get; set; }
        public string body { get; set; }
        public long employeeId { get; set; }
        public Employee employee { get; set; }
        public long idFrom { get; set; }
        public long? idTo { get; set; }
        public bool? sentByEmployee { get; set; }
    }

    public class CompanyMessageModel
    {
        public static CompanyMessageModel Create(CompanyMessage message)
        {
            CompanyMessageModel result = new CompanyMessageModel()
            {
                timeStamp = message.timeStamp,
                body = message.body,
                idFrom = message.idFrom,
                idTo = message.idTo,
                sentByCompany = message.sentByCompany
            };
            result.company = new DBHandler().getCompanyById(message.companyId).Item1;

            return result;
        }

        public DateTime timeStamp { get; set; }
        public string body { get; set; }
        public long companyId { get; set; }
        public Company company { get; set; }
        public long idFrom { get; set; }
        public long? idTo { get; set; }
        public bool? sentByCompany { get; set; }
    }

    public class MessagesModel
    {
        List<MessageModel> messages { get; set; }
    }
}