﻿using EasyBoxApplicationServer.Database;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Models
{
    public class PermissionModel
    {
        public static PermissionModel Create(UserTypeEnum type)
        {
            switch (type)
            {
                case UserTypeEnum.Operator:
                    return new PermissionModel()
                    {
                        CompanyList = 3,
                        CompanyDetail = 3,
                        BranchList = 3,
                        BranchDetail = 3,
                        EmployeeList = 1,
                        EmployeeDetail = 2,
                        Chat = 3,
                        Documents = 3,
                        ProfilePhoto = 0,
                        CompanyPhoto = 0
                    };
                case UserTypeEnum.Owner:
                    return new PermissionModel()
                    {
                        CompanyList = 3,
                        CompanyDetail = 2,
                        BranchList = 0,
                        BranchDetail = 0,
                        EmployeeList = 3,
                        EmployeeDetail = 1,
                        Chat = 0,
                        Documents = 3,
                        ProfilePhoto = 0,
                        CompanyPhoto = 3
                    };
                case UserTypeEnum.Customer:
                    return new PermissionModel()
                    {
                        CompanyList = 3,
                        CompanyDetail = 1,
                        BranchList = 0,
                        BranchDetail = 0,
                        EmployeeList = 3,
                        EmployeeDetail = 1,
                        Chat = 0,
                        Documents = 3,
                        ProfilePhoto = 0,
                        CompanyPhoto = 0
                    };
                case UserTypeEnum.Employee:
                    return new PermissionModel()
                    {
                        CompanyList = 0,
                        CompanyDetail = 0,
                        BranchList = 0,
                        BranchDetail = 0,
                        EmployeeList = 0,
                        EmployeeDetail = 3,
                        Chat = 3,
                        Documents = 3,
                        ProfilePhoto = 3,
                        CompanyPhoto = 0
                    };
                default:
                    return new PermissionModel()
                    {
                        CompanyList = 0,
                        CompanyDetail = 0,
                        BranchList = 0,
                        BranchDetail = 0,
                        EmployeeList = 0,
                        EmployeeDetail = 0,
                        Chat = 0,
                        Documents = 0,
                        ProfilePhoto = 0,
                        CompanyPhoto = 0
                    };
            }
        }
        
        public int CompanyList { get; set; }
        public int CompanyDetail { get; set; }
        public int BranchList { get; set; }
        public int BranchDetail { get; set; }
        public int EmployeeList { get; set; }
        public int EmployeeDetail { get; set; }
        public int Documents { get; set; }
        public int Chat { get; set; }
        public int ProfilePhoto { get; set; }
        public int CompanyPhoto { get; set; }
        public Service service { get; set; }
    }
}