﻿using EasyBoxApplicationServer.Controllers;
using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.RemoteServers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EasyBoxApplicationServer.Models.Responses
{

    public class GetEmployeesResponseModel
    {
        public static GetEmployeesResponseModel Create(List<Employee> employees, string branchName = null)
        {
            List<EmployeeResponseModel> employeesModels = new List<EmployeeResponseModel>();

            if (employees != null)
            {
                foreach (var employee in employees)
                {
                    Engagement engagement = new DBHandler().getEngagementByEmployeeID(employee.ID).Item1;
                    List<Extension> exts = new DBHandler().getExtensionsByEngagementID(engagement.ID).Item1;
                    Company company = new DBHandler().getCompanyByEmployeeCode(employee.employeeCode).Item1;
                    BadgeBoxGetRecordResponse getRecords = new BadgeBoxGetRecordResponse();
                    if (employee.isBBSync)
                    {
                        getRecords = BadgeBoxServer.getRecords(new BadgeBox.Requests.BadgeBoxGetRecordsModel()
                        {
                            id_empl = (long)employee.BBId,
                            user_token = employee.BBToken
                        }).Item1;
                    }
                    employeesModels.Add(EmployeeResponseModel.Create(employee, engagement, exts, company, RecordsModel.Create(getRecords), branchName));
                }
            }

            return new GetEmployeesResponseModel()
            {
                employees = employeesModels
            };
        }
        public List<EmployeeResponseModel> employees { get; set; }
    }

    public class EmployeeResponseModel
    {
        public static EmployeeResponseModel Create(Employee employee, Engagement engagement, List<Extension> exts, Company company, RecordsModel getRecords, string branchName = null)
        {
            int countMess = new DBHandler().getCountNewMessages(Utils.Constants.UserTypeEnum.Operator, employee.ID).Item1;
            int workedHours = 0;
            if (getRecords.records != null)
            {
                workedHours = getRecords.records.Sum(x => x.hours);
            }
            EmployeeResponseModel emprm = new EmployeeResponseModel()
            {
                id = employee.ID,
                name = employee.name,
                lastName = employee.lastName,
                email = employee.email,
                phone = employee.phone,
                branch = branchName,
                countNewMessages = countMess,
                records = getRecords,
                workedHours = workedHours,
                active = (bool)employee.isActive,
                BBId = employee.BBId != null ? (int) employee.BBId : 0 
            };
            if (company != null) emprm.company = company.businessName;
            if (exts.Count > 0)
            {
                emprm.engagementStart = engagement.missionStartDate.Value.ToString("dd/MM/yyyy");
                emprm.engagementEnd = exts.Max(x => x.endDate).Value.ToString("dd/MM/yyyy");
            }
            else
            {
                emprm.engagementStart = engagement.missionStartDate.Value.ToString("dd/MM/yyyy");
                emprm.engagementEnd = engagement.missionStopDate.Value.ToString("dd/MM/yyyy");
            }

            return emprm;
        }

        public RecordsModel records { get; set; }

        public int countNewMessages { get; set; }

        public string engagementStart { get; set; }

        public string engagementEnd { get; set; }

        public long id { get; set; }

        public bool active { get; set; }

        public string name { get; set; }

        public string lastName { get; set; }

        public string company { get; set; }

        public string email { get; set; }

        public string phone { get; set; }

        public string branch { get; set; }

        public string image { get; set; }

        public int workedHours { get; set; }

        public int BBId { get; set; }

    }

    public class GetEmployeeDetailResponseModel
    {
        public static GetEmployeeDetailResponseModel Create(Employee employee, List<Engagement> engagements)
        {
            List<EngagementResponseModel> engagementsResult = new List<EngagementResponseModel>();
            string now = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string from = string.Empty;


            foreach (Engagement engagement in engagements)
            {
                engagementsResult.Add(EngagementResponseModel.Create(engagement, employee));
                DateTime dt = (DateTime)engagement.missionStartDate;
                from = dt.ToString("yyyy-MM-dd HH:mm:ss");
            }
            int workedHours = 0;
            if (employee.isBBSync)
            {
                try
                {
                    workedHours = BadgeBoxServer.getRecords(new BadgeBox.Requests.BadgeBoxGetRecordsModel()
                    {
                        id_empl = (long)employee.BBId,
                        user_token = employee.BBToken,
                        from = from,
                        to = now
                    }).Item1.records.Max(x => int.Parse(x.total_hours));
                }
                catch (Exception) { }
            }

            CompanySummaryResponseModel company = new CompanySummaryResponseModel();
            if (employee != null && employee.companyId != null && employee.companyId > 0)
            {
                Company c = new DBHandler().getCompanyById((long)employee.companyId).Item1;
                company = new CompanySummaryResponseModel()
                {
                    BBCompanyId = c.BBId != null ? (int)c.BBId : 0,
                    id = c.ID
                };
            }

            GetEmployeeDetailResponseModel resp = new GetEmployeeDetailResponseModel()
            {
                employee = new EmployeeResponseModel()
                {
                    email = employee.email,
                    name = employee.name,
                    lastName = employee.lastName,
                    phone = employee.phone,
                    id = employee.ID,
                    workedHours = workedHours,
                    BBId = employee.BBId != null ? (int)employee.BBId : 0
                },
                engagements = new EngagementsResponseModel()
                {
                    engagements = engagementsResult
                },
                company = company
            };
            return resp;
        }

        public EmployeeResponseModel employee { get; set; }
        public EngagementsResponseModel engagements { get; set; }
        public CompanySummaryResponseModel company { get; set; }
    }
}