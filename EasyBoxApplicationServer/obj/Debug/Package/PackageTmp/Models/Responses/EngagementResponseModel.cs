﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.RemoteServers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.Responses
{
    public class EngagementResponseModel
    {
        public static EngagementResponseModel Create(Engagement engagement, Employee employee)
        {
            List<Extension> ext = new DBHandler().getExtensionsByEngagementID(engagement.ID).Item1;
            string fineProroga=string.Empty;
            Company company = new DBHandler().getCompanyByAdministrationNumber(engagement.administrationNumber).Item1;
            string businessName = string.Empty;
            DateTime? end = new DateTime();
            if (company != null)  businessName = company.businessName;
            if (ext.Count > 0)
            {
                end = ext.Max(x => x.endDate);
                fineProroga = end.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                end = engagement.missionStopDate;
            }
            BadgeBoxGetRecordResponse records = new BadgeBoxGetRecordResponse();
            if (employee.isBBSync)
            {
                records = BadgeBoxServer.getRecords(new BadgeBox.Requests.BadgeBoxGetRecordsModel()
                {
                    id_empl = (long)employee.BBId,
                    user_token = employee.BBToken,
                    from = engagement.missionStartDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                    to = end.Value.ToString("yyyy-MM-dd HH:mm:ss")
                }).Item1;
            }
            int workedHours = 0;
            if (records.records != null && records.records.Count > 0)
                workedHours = records.records.Sum(x => int.Parse(x.total_hours)) / 60;
            return new EngagementResponseModel()
            {
                CodiceLavoratore = engagement.employeeCode,
                Matricola = engagement.matriculationNumber,
                NumeroAssunzione = engagement.engagementNumber,
                DataAssunzione = engagement.engagementDate.Value.ToString("dd/MM/yyyy"),
                DataFineMissione = engagement.missionStopDate.Value.ToString("dd/MM/yyyy"),
                DataFineUltimaProroga = fineProroga,
                DataInizioMissione = engagement.missionStartDate.Value.ToString("dd/MM/yyyy"),
                company = businessName,
                workedHours = workedHours
            };
        }
        public string NumeroAssunzione { get; set; }
        public string DataInizioMissione { get; set; }
        public string DataFineMissione { get; set; }
        public string Matricola { get; set; }
        public string DataAssunzione { get; set; }
        public string CodiceLavoratore { get; set; }
        public string DataFineUltimaProroga { get; set; }
        public string company { get; set; }
        public int workedHours { get; set; }
    }

    public class EngagementsResponseModel
    {
        public List<EngagementResponseModel> engagements { get; set; }
    }
}