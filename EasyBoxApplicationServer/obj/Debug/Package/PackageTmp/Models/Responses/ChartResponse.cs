﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Models.Responses
{
    public class ChartResponse
    {
        public List<Tuple<ChartEnum, int>> chart { get; set; }
    }
}