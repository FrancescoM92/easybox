﻿using System;
using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.Responses
{
    public class StringModel
    {
        public string stringa { get; set; }
    }

    public class StringsModel
    {
        public StringsModel()
        {
            stringhe = new List<string>();
        }
        public List<String> stringhe { get; set; }
    }
}