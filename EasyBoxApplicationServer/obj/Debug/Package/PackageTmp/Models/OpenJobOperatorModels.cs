﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models
{
    public class OpenJobOperatorModel
    {
        public long id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string bbToken { get; set; }
    }

    public class OpenJobOperatorsModel
    {
        public List<OpenJobOperatorModel> operators { get; set; }
    }
}