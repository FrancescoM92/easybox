﻿using EasyBoxApplicationServer.Models.ServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class ExtensionModel
    {
        public static ExtensionModel Create(ProrogaAssunzione proroga)
        {
            return new ExtensionModel()
            {
                startDate = proroga.DataInizioProroga,
                endDate = proroga.DataFineProroga,
                extensionDate = proroga.DataProroga,
                extensionNumber = proroga.NumeroProroga
            };
        }
        public static ExtensionModel CreateAdministration(Proroga proroga)
        {
            return new ExtensionModel()
            {
                startDate = proroga.DataInizioProroga,
                endDate = proroga.DataFineProroga,
                extensionDate = proroga.DataProroga,
                extensionNumber = proroga.NumeroContrattoProroga
            };
        }

        public long ID { get; set; }
        public string extensionNumber { get; set; }
        public DateTime extensionDate { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public long administration { get; set; }
        public long engagement { get; set; }
    }
    public class ExtensionsModel
    {
        public static ExtensionsModel Create(List<ProrogaAssunzione> proroghe)
        {
            List<ExtensionModel> exts = new List<ExtensionModel>();
            foreach(ProrogaAssunzione p in proroghe)
            {
                exts.Add(ExtensionModel.Create(p));
            }
            return new ExtensionsModel()
            {
                extensions = exts
            };
        }
        public static ExtensionsModel CreateAdministration(List<Proroga> proroghe)
        {
            List<ExtensionModel> exts = new List<ExtensionModel>();
            foreach (Proroga p in proroghe)
            {
                exts.Add(ExtensionModel.CreateAdministration(p));
            }
            return new ExtensionsModel()
            {
                extensions = exts
            };
        }
        public List<ExtensionModel> extensions { get; set; }
    }
}