﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Models.Requests
{
    public class MainModel
    {
        public UserTypeEnum type { get; set; }
        public string username { get; set; }
    }
}