﻿namespace EasyBoxApplicationServer.Models.Requests
{
    public class MessageRequest
    {
        public MessageModel message { get; set; }
        public string token { get; set; }
        public string azure { get; set; }
    }

    public class CompanyMessageRequest
    {
        public CompanyMessageModel message { get; set; }
        public string token { get; set; }
        public string azure { get; set; }
    }
}