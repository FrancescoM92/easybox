﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.Requests
{
    public class IdRequest
    {
        public long id { get; set; }
    }

    public class IdsRequest
    {
        public int[] ids { get; set; }
    }
}