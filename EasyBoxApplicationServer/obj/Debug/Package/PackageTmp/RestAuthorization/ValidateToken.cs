﻿using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Text;

namespace EasyBoxApplicationServer.RestAuthorization
{
    public class ValidateToken
    {
        private const int _expirationMinutes = 20;

        public static bool IsTokenValid(string token, string ip, string userAgent)
        {
            bool result = false;
            try
            {
                // Base64 decode the string, obtaining the token:username:timeStamp.
                string[] toReplace = token.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                token = toReplace[0];
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long ticks = long.Parse(parts[2]);
                    DateTime timeStamp = DateTimeOffset.FromUnixTimeMilliseconds(ticks).DateTime.ToLocalTime();
                       // new DateTime(ticks);
                    //if (!timeStamp.IsDaylightSavingTime())
                    //    timeStamp.AddHours(-1);
                    // Ensure the timestamp is valid.
                    bool expired = Math.Abs((DateTime.Now - timeStamp).TotalMinutes) > _expirationMinutes;
                    if (!expired)
                    {
                        // Hash the message with the key to generate a token.
                        string computedToken = EncodePSW.GenerateToken(username, ip, userAgent, ticks);
                        // Compare the computed token with the one supplied and ensure they match.
                        result = (token == computedToken);
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("EXCEPTION MESSAGE: " + e.Message);
            }
            return result;
        }

        public static bool IsAzureTokenValid(string json, string ip)
        {
            try
            {
                DateTime? expiration = null;
                JToken jtoken = JWTDecoder.decodePayload(json);
                if (jtoken.HasValues)
                {
                    string aud = jtoken.Value<string>("appid");
                    int exp = jtoken.Value<int>("exp");
                    string ipAddr = jtoken.Value<string>("ipaddr");
                    if (exp != 0) expiration = MyHelper.GetDateTimeFromTimeStamp(exp);
                    if (/*DateTime.Now < expiration && ip.Equals(ipAddr) &&*/ aud.Equals(ConfigurationManager.AppSettings.Get("ida:ClientID")))
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("EXCEPTION MESSAGE: " + e.Message);
            }
            return false;
        }
    }
}