﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.RestAuthorization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace EasyBoxApplicationServer.Utils
{
    public class MyHelper
    {
        private static Random random = new Random();

        public static Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {

                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                Image image = Image.FromStream(ms, true);
                return image;
            }
        }

        public static string GetEncryptedPassword(string password)
        {
            return GetHashSha256(string.Join(":", password, EncodePSW._salt));
        }

        public static string CreateNewPassword(int length)
        {
            return Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(0, length);
        }

        public static long GetJavascriptMicrosFromTicks(long ticks)
        {
            DateTime someDate = new DateTime(ticks);
            long unixTimestamp = (long)(someDate.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return unixTimestamp * 1000;
        }

        public static string GetHashSha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            HMACSHA256 encrypter = new HMACSHA256()
            {
                Key = Encoding.UTF8.GetBytes(EncodePSW._salt)
            };
            byte[] hash = encrypter.ComputeHash(bytes);
            return Convert.ToBase64String(hash);
        }

        public static string GetLocalizedString(string key)
        {
            return (string)HttpContext.GetGlobalResourceObject("LocalizedText", key);
        }

        public static string ImageToBase64(string path, ImageFormat format)
        {
            using (Image image = Image.FromFile(path))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static void Log(string[] lines)
        {
            var logFile = "C://EasyBoxLogs//EasyBoxLog_" + DateTime.Now.ToString("yyyy_MM_dd-HH") + ".txt";
            if (File.Exists(logFile))
            {
                using (StreamWriter writer = File.AppendText(logFile))
                {
                    foreach (string line in lines)
                        writer.WriteLine(line + "\n");
                }
            }
            else
            {
                string mydocpath = "C://EasyBoxLogs"; //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments, Environment.SpecialFolderOption.Create);
                string logName = DateTime.Now.ToString("yyyy_MM_dd-HH");
                using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\EasyBoxLog_" + logName + ".txt"))
                {
                    foreach (string line in lines)
                        outputFile.WriteLine(line + "\n");
                }
            }
        }

        public static void LogSBMessage(string[] lines)
        {
            var logFile = "C://EasyBoxLogs//ServiceBus_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
            if (File.Exists(logFile))
            {
                using (StreamWriter writer = File.AppendText(logFile))
                {
                    foreach (string line in lines)
                        writer.WriteLine(line + "\n");
                }
            }
            else
            {
                string mydocpath = "C://EasyBoxLogs"; //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments, Environment.SpecialFolderOption.Create);
                string logName = DateTime.Now.ToString("yyyy_MM_dd");
                using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\ServiceBus_" + logName + ".txt"))
                {
                    foreach (string line in lines)
                        outputFile.WriteLine(line + "\n");
                }
            }
        }

        public static void BBLog(string[] lines)
        {
            var logFile = "C://EasyBoxLogs//EasyBoxLog_BB_" + DateTime.Now.ToString("yyyy_MM_dd-HH-mm") + ".txt";
            if (File.Exists(logFile))
            {
                using (StreamWriter writer = File.AppendText(logFile))
                {
                    foreach (string line in lines)
                        writer.WriteLine(line + "\n");
                }
            }
            else
            {
                string mydocpath = "C://EasyBoxLogs"; //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments, Environment.SpecialFolderOption.Create);
                string logName = DateTime.Now.ToString("yyyy_MM_dd-HH-mm");
                using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\EasyBoxLog_BB" + logName + ".txt"))
                {
                    foreach (string line in lines)
                        outputFile.WriteLine(line + "\n");
                }
            }
        }

        public static void AWDLog(string[] lines)
        {
            var logFile = "C://EasyBoxLogs//EasyBoxLog_AWD_" + DateTime.Now.ToString("yyyy_MM_dd-HH") + ".txt";
            if (File.Exists(logFile))
            {
                using (StreamWriter writer = File.AppendText(logFile))
                {
                    foreach (string line in lines)
                        writer.WriteLine(line + "\n");
                }
            }
            else
            {
                string mydocpath = "C://EasyBoxLogs";
                string logName = DateTime.Now.ToString("yyyy_MM_dd-HH");
                using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\EasyBoxLog_AWD_" + logName + ".txt"))
                {
                    foreach (string line in lines)
                        outputFile.WriteLine(line + "\n");
                }
            }
        }

        public static void Log(string line)
        {
            Log(new string[] { line });
        }

        public static void LogSBMessage(string line)
        {
            LogSBMessage(new string[] { line });
        }

        public static void LogEMP(string line)
        {
            var logFile = "C://EasyBoxLogs//EasyBoxLog_Employee.txt";
            if (File.Exists(logFile))
            {
                using (StreamWriter writer = File.AppendText(logFile))
                {
                    writer.WriteLine(line + "\n");
                }
            }
            else
            {
                string mydocpath = "C://EasyBoxLogs"; //Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments, Environment.SpecialFolderOption.Create);
                using (StreamWriter outputFile = new StreamWriter(mydocpath + @"\EasyBoxLog_Employee.txt"))
                {
                    outputFile.WriteLine(line + "\n");
                }
            }
        }

        public static void writeOnDoc(string json)
        {
            var logFile = "C://EasyBoxLogs//RegisteredUsers.txt";
            if (File.Exists(logFile))
            {
                using (StreamWriter outputFile = File.AppendText(logFile))
                {
                    outputFile.WriteLine(json + "::::");
                }
            }
            else
            {
                using (StreamWriter outputFile = new StreamWriter(logFile))
                {
                    outputFile.WriteLine(json + "::::");
                }
            }
        }

        public static string[] readFromDoc()
        {
            var logFile = "C://EasyBoxLogs//RegisteredUsers.txt";
            if (File.Exists(logFile))
            {
                string[] stringSeparators = new string[] { "::::" };
                try
                {
                    string fileText = File.ReadAllText(@"C://EasyBoxLogs//RegisteredUsers.txt");
                    string[] parts = fileText.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                    return parts;
                }
                catch (Exception e)
                {
                    return new string[] { e.Message };
                }
            }
            else
            {
                return null;
            }
        }

        public static DateTime GetDateTimeFromTimeStamp(long timeStamp)
        {
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(timeStamp);
        }

        public static DateTime GetDateTimeFromYearMonthWithFirstOfMonth(int year, int month)
        {
            DateTime epoch = new DateTime(year, month, 1);
            return epoch;
        }

        public static DateTime GetDateTimeFromYearMonthWithLastOfMonth(int year, int month)
        {
            DateTime epoch = new DateTime(year, month, DateTime.DaysInMonth(year, month), 23, 59, 59, 999);
            return epoch;
        }

        public static Tuple<string, int> getCurrentSize(long size)
        {
            long resInt = size;
            for (Unit i = Unit.B; i <= Unit.TB; i++)
            {
                if (resInt > 1024) resInt = resInt / 1024;
                else return new Tuple<string, int>(GetUnitName(i), (int)resInt);
            }
            return null;
        }

        public static string GetUnitName(Unit unit)
        {
            switch (unit)
            {
                case Unit.B:
                    return "B";
                case Unit.KB:
                    return "KB";
                case Unit.MB:
                    return "MB";
                case Unit.GB:
                    return "GB";
                case Unit.TB:
                    return "TB";
                case Unit.PB:
                    return "PB";
                default:
                    return "";
            }
        }

        public static JToken InjectGroupApartenance(string JWT)
        {
            List<string> result = new List<string>();
            HttpClient client = new HttpClient();


            JToken token = JToken.Parse(JWT);
            string accessToken = token.Value<string>("access_token");
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
            var response = client.GetAsync("https://graph.windows.net/me/memberOf?api-version=1.6");
            var jsonResponse = response.Result.Content.ReadAsStringAsync();
            JToken memberOf = JToken.Parse(jsonResponse.Result);
            JArray groupss = memberOf.Value<JArray>("value");
            JEnumerable<JToken> groups = groupss.Children();
            foreach (JToken tk in groups)
            {
                result.Add(tk.Value<string>("displayName"));
            }
            string[] newKeys = result.ToArray();

            string json = JsonConvert.SerializeObject(newKeys);
            token["groups"] = JToken.Parse(json);
            return token;
        }

        public static List<string> getUserBusinessUnits(string json)
        {
            List<string> groups = null;
            if (!string.IsNullOrEmpty(json))
            {
                JToken jtoken = JWTDecoder.decodePayload(json);
                if (jtoken.HasValues)
                {
                    groups = new List<string>(jtoken.Value<JToken>("groups").Values<string>());
                }
            }
            return groups;
        }

        public static string makeRandomPassword(int length)
        {
            string UpperCase = "QWERTYUIOPASDFGHJKLZXCVBNM";
            string LowerCase = "qwertyuiopasdfghjklzxcvbnm";
            string Digits = "1234567890";
            string allCharacters = UpperCase + LowerCase + Digits;
            //Random will give random charactors for given length  
            Random r = new Random();
            String password = "";
            for (int i = 0; i < length; i++)
            {
                double rand = r.NextDouble();
                if (i == 0)
                {
                    password += UpperCase.ToCharArray()[(int)Math.Floor(rand * UpperCase.Length)];
                }
                else
                {
                    password += allCharacters.ToCharArray()[(int)Math.Floor(rand * allCharacters.Length)];
                }
            }
            return password;
        }

        public static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }

    public class MultipartParser
    {
        public MultipartParser(Stream stream)
        {
            this.Parse(stream, Encoding.UTF8);
        }

        public MultipartParser(Stream stream, Encoding encoding)
        {
            this.Parse(stream, encoding);
        }

        private void Parse(Stream stream, Encoding encoding)
        {
            this.Success = false;

            // Read the stream into a byte array
            byte[] data = ToByteArray(stream);

            // Copy to a string for header parsing
            string content = encoding.GetString(data);

            // The first line should contain the delimiter
            int delimiterEndIndex = content.IndexOf("\r\n");

            if (delimiterEndIndex > -1)
            {
                string delimiter = content.Substring(0, content.IndexOf("\r\n"));

                // Look for Content-Type
                Regex re = new Regex(@"(?<=Content\-Type:)(.*?)(?=\r\n\r\n)");
                Match contentTypeMatch = re.Match(content);

                // Look for filename
                re = new Regex(@"(?<=filename\=\"")(.*?)(?=\"")");
                Match filenameMatch = re.Match(content);

                // Did we find the required values?
                if (contentTypeMatch.Success && filenameMatch.Success)
                {
                    // Set properties
                    this.ContentType = contentTypeMatch.Value.Trim();
                    this.Filename = filenameMatch.Value.Trim();

                    // Get the start & end indexes of the file contents
                    int startIndex = contentTypeMatch.Index + contentTypeMatch.Length + "\r\n\r\n".Length;

                    byte[] delimiterBytes = encoding.GetBytes("\r\n" + delimiter);
                    int endIndex = IndexOf(data, delimiterBytes, startIndex);

                    int contentLength = endIndex - startIndex;

                    // Extract the file contents from the byte array
                    byte[] fileData = new byte[contentLength];

                    Buffer.BlockCopy(data, startIndex, fileData, 0, contentLength);

                    this.FileContents = fileData;
                    this.Success = true;
                }
            }
        }

        private int IndexOf(byte[] searchWithin, byte[] searchFor, int startIndex)
        {
            int index = 0;
            int startPos = Array.IndexOf(searchWithin, searchFor[0], startIndex);

            if (startPos != -1)
            {
                while ((startPos + index) < searchWithin.Length)
                {
                    if (searchWithin[startPos + index] == searchFor[index])
                    {
                        index++;
                        if (index == searchFor.Length)
                        {
                            return startPos;
                        }
                    }
                    else
                    {
                        startPos = Array.IndexOf<byte>(searchWithin, searchFor[0], startPos + index);
                        if (startPos == -1)
                        {
                            return -1;
                        }
                        index = 0;
                    }
                }
            }

            return -1;
        }

        private byte[] ToByteArray(Stream stream)
        {
            byte[] buffer = new byte[32768];
            using (MemoryStream ms = new MemoryStream())
            {
                while (true)
                {
                    int read = stream.Read(buffer, 0, buffer.Length);
                    if (read <= 0)
                        return ms.ToArray();
                    ms.Write(buffer, 0, read);
                }
            }
        }

        public bool Success
        {
            get;
            private set;
        }

        public string ContentType
        {
            get;
            private set;
        }

        public string Filename
        {
            get;
            private set;
        }

        public byte[] FileContents
        {
            get;
            private set;
        }
    }

    public enum Unit
    {
        B = 0,
        KB = 1,
        MB = 2,
        GB = 3,
        TB = 4,
        PB = 5
    }

}