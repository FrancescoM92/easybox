﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Utils
{
    public class BadgeBoxConstants
    {
        public enum InfoTypeEnum
        {
            Phone = 0,
            MobilePhone = 1,
            Fax = 2,
            Email = 3,
            CodFiscale = 4,
            Nation = 5,
            City = 6,
            Address = 7,
            IBAN = 8,
            Contract = 9,
            Birthday = 11,
            Mission = 12,
            Code = 13
        }
    }
}