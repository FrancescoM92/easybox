﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.RemoteServers;
using EasyBoxApplicationServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Utils
{
    public class EmailHandler
    {
        DBHandler dbhandler = new DBHandler();

        public void toBeSignedReminder()
        {
            Tuple<List<Customer>, string> customers = dbhandler.getCustomersListForDocument();
            Tuple<List<Employee>, string> employees = dbhandler.getEmployeesActiveList();
            foreach (Customer c in customers.Item1)
            {
                string bu = dbhandler.GetDBContext().servicebus.Where(x => x.companyCode.Equals(c.Company.FirstOrDefault().companyCode)).FirstOrDefault().businessUnitID;
                List<AWDocument> docs = dbhandler.getAWDocuments(new Models.PersonModelBase()
                {
                    id = c.ID
                }).Item1.Where(x => x.signed == false && x.signing == false).ToList();

                if (bu.Contains("AGR"))
                    EmailSender.sendEmail(c.email, "Documenti AgriBox", "Salve, ha N°" + docs.Count + " documenti ancora da firmare, può trovarli nella sezione Documenti > Non Firmati della piattaforma", false);
                else
                    EmailSender.sendEmail(c.email, "Documenti EasyBox", "Salve, ha N°" + docs.Count + " documenti ancora da firmare, può trovarli nella sezione Documenti > Non Firmati della piattaforma", true);

            }
            foreach (Employee e in employees.Item1)
            {
                Company company = dbhandler.getCompanyById((long)e.companyId).Item1;
                List<AWDocument> docs = dbhandler.getAWDocuments(new Models.PersonModelBase()
                {
                    id = e.ID
                }).Item1.Where(x => x.signed == false && x.signing == false && x.signable == true).ToList();

                string bu = dbhandler.GetDBContext().servicebus.Where(x => x.companyCode.Equals(company.companyCode)).FirstOrDefault().businessUnitID;
                if (bu.Contains("AGR"))
                    EmailSender.sendEmail(e.email, "Documenti AgriBox", "Salve" + e.name + ", ha N°" + docs.Count + " documenti ancora da firmare, può trovarli nella sezione Documenti > Non Firmati della piattaforma", false);
                else
                    EmailSender.sendEmail(e.email, "Documenti EasyBox", "Salve" + e.name + ", ha N°" + docs.Count + " documenti ancora da firmare, può trovarli nella sezione Documenti > Non Firmati della piattaforma", true);
            }
        }

        public void toBeSignedReminder(Customer c)
        {
            string bu = dbhandler.GetDBContext().servicebus.Where(x => x.companyCode.Equals(c.Company.FirstOrDefault().companyCode)).FirstOrDefault().businessUnitID;
            List<AWDocument> docs = dbhandler.getAWDocuments(new Models.PersonModelBase()
            {
                id = c.ID
            }).Item1.Where(x => x.signed == false && x.signing == false).ToList();

            if (bu.Contains("AGR"))
                EmailSender.sendEmail(c.email, "Documenti AgriBox", "Salve, ha N°" + docs.Count + " documenti ancora da firmare, può trovarli nella sezione Documenti > Non Firmati della piattaforma", false);
            else
                EmailSender.sendEmail(c.email, "Documenti EasyBox", "Salve, ha N°" + docs.Count + " documenti ancora da firmare, può trovarli nella sezione Documenti > Non Firmati della piattaforma", true);

        }

        public void toBeSignedReminder(Employee e)
        {
            Company company = dbhandler.getCompanyById((long)e.companyId).Item1;
            List<AWDocument> docs = dbhandler.getAWDocuments(new Models.PersonModelBase()
            {
                id = e.ID
            }).Item1.Where(x => x.signed == false && x.signing == false && x.signable == true).ToList();

            string bu = dbhandler.GetDBContext().servicebus.Where(x => x.companyCode.Equals(company.companyCode)).FirstOrDefault().businessUnitID;
            if (bu.Contains("AGR"))
                EmailSender.sendEmail(e.email, "Documenti AgriBox", "Salve" + e.name + ", ha N°" + docs.Count + " documenti ancora da firmare, può trovarli nella sezione Documenti > Non Firmati della piattaforma", false);
            else
                EmailSender.sendEmail(e.email, "Documenti EasyBox", "Salve" + e.name + ", ha N°" + docs.Count + " documenti ancora da firmare, può trovarli nella sezione Documenti > Non Firmati della piattaforma", true);
        }

        public void toSendTimeSheet()
        {
            List<Employee> employees = dbhandler.getEmployeesActiveList().Item1;
            foreach (Employee e in employees)
            {
                if (e.isBBSync)
                {
                    Company company = dbhandler.getCompanyById((long)e.companyId).Item1;
                    string bu = dbhandler.GetDBContext().servicebus.Where(x => x.companyCode.Equals(company.companyCode)).FirstOrDefault().businessUnitID;

                    DateTime startOfMonth = new DateTime(DateTime.Now.Year, (DateTime.Now.Month - 1), 1, 0, 0, 0);
                    DateTime endOfMonth = new DateTime(DateTime.Now.Year, (DateTime.Now.Month - 1), DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month - 1), 23, 59, 59);
                    BadgeBoxGetTimeSheetResponse res = BadgeBoxServer.getTimeSheet(new Models.BadgeBox.Requests.BadgeBoxGetTimeSheetModel()
                    {
                        from = startOfMonth.ToString("yyyy-MM-dd"),
                        to = endOfMonth.ToString("yyyy-MM-dd"),
                        user_token = e.BBToken,
                        id_empl = (long)e.BBId
                    }).Item1;
                    if (res.values.Count < 1)
                    {
                        if (bu.Contains("AGR"))
                            EmailSender.sendEmail(e.email, "Rapportino AgriBox", "Salve" + e.name + ", ha dimenticato di inviare il rapportino del mese scorso, può farlo direttamente dal suo profilo sulla piattaforma AgriBox", false);
                        else
                            EmailSender.sendEmail(e.email, "Rapportino EasyBox", "Salve" + e.name + ", ha dimenticato di inviare il rapportino del mese scorso, può farlo direttamente dal suo profilo sulla piattaforma EasyBox", true);
                    }
                }
            }
        }

        public void toConfirmTimeSheet()
        {
            Tuple<List<Customer>, string> customers = dbhandler.getCustomersListForTimeSheet();
            foreach (Customer c in customers.Item1)
            {
                int count = 0;
                Company comp = dbhandler.getCustomerCompanies(c.ID).Item1[0];
                if (comp != null)
                {
                    List<Employee> emps = dbhandler.getCompanyEmployees(comp.ID).Item1;
                    foreach (Employee e in emps)
                    {
                        if (e.isBBSync)
                        {
                            count += dbhandler.getNotConfirmedTimeSheetCountByEmployee(e.ID).Item1;
                        }
                    }
                }

                string bu = dbhandler.GetDBContext().servicebus.Where(x => x.companyCode.Equals(comp.companyCode)).FirstOrDefault().businessUnitID;

                if (bu.Contains("AGR"))
                    EmailSender.sendEmail(c.email, "Rapportino AgriBox", "Salve" + c.name + ", ha ancora" + count + " rapportini da confermare, può farlo direttamente dalla piattaforma AgriBox", false);
                else
                    EmailSender.sendEmail(c.email, "Rapportino EasyBox", "Salve" + c.name + ", ha ancora" + count + " rapportini da confermare, può farlo direttamente dalla piattaforma EasyBox", true);
            }
        }
    }
}