﻿using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace EasyBoxApplicationServer.Utils
{
    public class Request
    {
        public static StreamReader PostRequest(string url, string json)
        {
            StreamReader reader = null;
            string jsonresponse = string.Empty;
            try
            {
                var httpWReq = (HttpWebRequest)WebRequest.Create(url);
                httpWReq.ContentType = "application/json";
                httpWReq.Method = "POST";
                using (var streamWriter = new StreamWriter(httpWReq.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                reader = new StreamReader(response.GetResponseStream());
            }
            catch (WebException e)
            {
                MyHelper.Log("askServer - ex:" + e.Message);
                if (e.Response != null)
                    using (WebResponse response = e.Response)
                    {
                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                        MyHelper.Log(string.Format("Error code: {0}", httpResponse.StatusCode));
                        using (Stream data = response.GetResponseStream())
                        using (reader = new StreamReader(data))
                        {
                            string text = reader.ReadToEnd();
                            MyHelper.Log(text);
                        }
                    }
                dynamic errjobj = new JObject();
                errjobj.Message = e.Message;
                jsonresponse = ((JObject)errjobj).ToString();
            }
            return reader;
        }
        
        public static Task<string> AzurePostRequest(string url, FormUrlEncodedContent content)
        {
            HttpClient client = new HttpClient();
            try
            {
                client.Timeout = TimeSpan.FromSeconds(10);
                content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");
                var response = client.PostAsync(url, content);

                var jsonresponse = response.Result.Content.ReadAsStringAsync();

                return jsonresponse;
            }
            catch (WebException)
            {
                return null;
            }
        }
    }
}