﻿using Newtonsoft.Json.Linq;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace EasyBoxApplicationServer.Utils
{
    public static class JWTDecoder
    {
        //private static readonly string OJMKey = "MIIDBTCCAe2gAwIBAgIQHJ7yHxNEM7tBeqcRTMBhhTANBgkqhkiG9w0BAQsFADAtMSswKQYDVQQDEyJhY2NvdW50cy5hY2Nlc3Njb250cm9sLndpbmRvd3MubmV0MB4XDTE4MDEwODAwMDAwMFoXDTIwMDEwOTAwMDAwMFowLTErMCkGA1UEAxMiYWNjb3VudHMuYWNjZXNzY29udHJvbC53aW5kb3dzLm5ldDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKSVAfdBDMYjY4JFc+Qiy1LhuW2MIqy9i131bKv552OvOls24p5LxZzbOi76yeg1g+TVdftE6tnmY+1lPB/crZgz7EZydG+PYEQiWDoOvknTnvU5dJQdCASRNWlodznQla2SxOCx+PixiA65MJwlfHShZDtxCFGD2hMrC+IOMC8fkwn6TFhN+1wtPT/pwUVZETXIoa82N/PzAuNjZEXQ8AcmXpl5ECnXZ5+WOaOK1czxqkMt745k4yqJWBCAWGu598hdPOPDBtJN9/G458nHiYJmc48bbZFk1Q4pEF0pXW7t0AqwgJYqW1j1AF2mSvG7mPCz/NBuFP8MeC7mfyq6uTkCAwEAAaMhMB8wHQYDVR0OBBYEFFVWt/4iTcBiWk+EX1dCKZGoAlBQMA0GCSqGSIb3DQEBCwUAA4IBAQCJrLUiCfldfDFhZLG08DpLcwpn8Mvhm61ZpPANyCawRgaoBOsxFJew1qpC2wfXh3yUwJEIkkTGXAsiWENkeqmjd2XXQwZuIHIo5/KyZLfo2m1CmEH/QXL6uRx1f8liPr4efC8H+7/sduf6nPfk0UpNsAHA6RxJFy2jg2wHU+Ux4jK4Gc5d/rJPhJhvyS9Zax1hbTlf+N32ZvS780VMDb/nf2LdtACL0ya/+KSDGVXS3GhS9FLEXrBNjq921ghVIFJhPzm54yfeupIwz+zfISoTHIYw37i7iNUbkvDrm14a27pwLS/tfSuJHKcGPt1sjMu6SS/pf1BlvdoFkKdLLaUb";
        public static JToken decodeToken(string token)
        {
            string[] parts = HttpUtility.UrlDecode(token).Replace(" ","").Split('.');
            string payload = parts[1];
            string payloadJson = string.Empty;
            JToken payloadData = null;
            try
            {
                payloadJson = base64UTF8Decode(payload);
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
            try
            {
                payloadData = JToken.Parse(payloadJson);
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
            //var keyBytes = Convert.FromBase64String(OJMKey); // your key here

            //AsymmetricKeyParameter asymmetricKeyParameter = PublicKeyFactory.CreateKey(keyBytes);
            //RsaKeyParameters rsaKeyParameters = (RsaKeyParameters)asymmetricKeyParameter;
            //RSAParameters rsaParameters = new RSAParameters
            //{
            //    Modulus = rsaKeyParameters.Modulus.ToByteArrayUnsigned(),
            //    Exponent = rsaKeyParameters.Exponent.ToByteArrayUnsigned()
            //};
            //RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            //rsa.ImportParameters(rsaParameters);

            //SHA256 sha256 = SHA256.Create();
            //byte[] hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(parts[0] + '.' + parts[1]));

            //RSAPKCS1SignatureDeformatter rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
            //rsaDeformatter.SetHashAlgorithm("SHA256");
            //if (!rsaDeformatter.VerifySignature(hash, HttpServerUtility.UrlTokenDecode(parts[2])))
            //    throw new ApplicationException(string.Format("Invalid signature"));
            return payloadData;
        }

        public static JToken decodePayload(string payload)
        {
            string payloadJson = string.Empty;
            JToken payloadData = null;
            try
            {
                payloadJson = base64UTF8Decode(payload);
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
            try
            {
                payloadData = JToken.Parse(payloadJson);
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
            return payloadData;
        }

        public static string base64UTF8Decode(string data)
        {
            if (IsBase64String(data))
            {
                try
                {
                    UTF8Encoding encoder = new UTF8Encoding();
                    Decoder utf8Decode = encoder.GetDecoder();

                    byte[] todecode_byte = Convert.FromBase64String(data);
                    int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                    char[] decoded_char = new char[charCount];
                    utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                    string result = new String(decoded_char);
                    return result;
                }
                catch (Exception e)
                {
                    throw new Exception("Error in base64Decode" + e.Message);
                }
            }
            return null;
        }
        public static string base64ASCIIDecode(string data)
        {
            try
            {
                UTF8Encoding encoder = new UTF8Encoding();
                Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Encoding.ASCII.GetBytes(data);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Decode" + e.Message);
            }
        }


        public static bool IsBase64String(string s)
        {
            s = s.Trim();
            s = s.Replace(" ", "");
            return (s.Length % 4 == 0) && Regex.IsMatch(s, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
        }
    }
}