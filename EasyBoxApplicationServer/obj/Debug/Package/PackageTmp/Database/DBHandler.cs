﻿using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.AWDoc;
using EasyBoxApplicationServer.Models.BadgeBox.Requests;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.Models.ServiceBus;
using EasyBoxApplicationServer.RemoteServers;
using EasyBoxApplicationServer.Services;
using EasyBoxApplicationServer.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Database
{
    public class DBHandler
    {
        public void prova()
        {

        }

        private String connectionString;
        private SqlConnection connection;

        public DBHandler()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            connection = new SqlConnection(connectionString);
        }

        public easybox_dbEntities1 GetDBContext()
        {
            return new easybox_dbEntities1();
        }

        public int testDB()
        {
            return GetDBContext().OpenJobOperator.Count();
        }

        #region PASSWORD & TYPE
        public PersonModelBase getUserFromToken(string token, string azure)
        {
            PersonModelBase person = null;
            string username = string.Empty;
            // Base64 decode the string, obtaining the token:username:timeStamp.
            string key = string.Empty;
            if (!string.IsNullOrEmpty(token))
            {
                try
                {
                    key = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                }
                catch (Exception)
                {
                }

                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    username = parts[1];
                }

                person = getUserFromCredential(username).Item1;
            }
            else if (!string.IsNullOrEmpty(azure))
            {
                if (JWTDecoder.IsBase64String(azure))
                {
                    azure = JWTDecoder.base64UTF8Decode(azure);
                }
                string name = string.Empty;
                try
                {
                    JToken jtoken = JToken.Parse(azure);
                    if (jtoken.HasValues)
                    {
                        username = jtoken.Value<string>("unique_name");
                        name = jtoken.Value<string>("name");
                    }
                }
                catch (Exception)
                {
                }
                person = new PersonModelBase()
                {
                    email = username,
                    name = name.Split(' ')[0],
                    lastName = name.Split(' ')[1],
                    type = UserTypeEnum.Operator,
                    fiscalCode = "PROVAFISCALCODE0"
                };
            }
            return person;
        }

        public bool isAgri(PersonModelBase person)
        {
            try
            {
                switch (person.type)
                {
                    case UserTypeEnum.Customer:
                        Company custCompany = getCustomerCompanies(person.id).Item1[0];
                        if (GetDBContext().servicebus.Where(x => x.companyCode.Equals(custCompany.companyCode) && x.businessUnitID.Contains(DIVISIONE_AGRARIA)).FirstOrDefault() != null)
                            return true;
                        break;
                    case UserTypeEnum.Employee:
                        Employee emp = GetDBContext().Employee.Find(person.id);
                        Engagement eng = getEngagementByEmployeeID(emp.ID).Item1;
                        if (GetDBContext().servicebus.Where(x => x.employeeCode.Equals(emp.employeeCode) && x.businessUnitID.Contains(DIVISIONE_AGRARIA)).FirstOrDefault() != null)
                            return true;
                        break;
                    case UserTypeEnum.Owner:
                        Company OwnCompany = getCustomerCompanies(person.id).Item1[0];
                        if (GetDBContext().servicebus.Where(x => x.companyCode.Equals(OwnCompany.companyCode) && x.businessUnitID.Contains(DIVISIONE_AGRARIA)).FirstOrDefault() != null)
                            return true;
                        break;
                    default:
                        return false;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        public bool isActive(PersonModelBase person)
        {
            try
            {
                switch (person.type)
                {
                    case UserTypeEnum.Customer:
                        Customer customer = GetDBContext().Customer.Find(person.id);
                        if ((bool)customer.isActive) return true;
                        break;
                    case UserTypeEnum.Employee:
                        Employee emp = GetDBContext().Employee.Find(person.id);
                        if ((bool)emp.isActive) return true;
                        break;
                    case UserTypeEnum.Owner:
                        Customer owner = GetDBContext().Customer.Find(person.id);
                        if ((bool)owner.isActive) return true;
                        break;
                    default:
                        return false;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        public string getUserPassword(string username)
        {
            string password = string.Empty;

            UserTypeEnum userType = (UserTypeEnum)Enum.Parse(typeof(UserTypeEnum), getUserTypeFromCredential(username).type.ToString());

            switch (userType)
            {
                case UserTypeEnum.Owner:
                    password = getUserPasswordCustomer(username);
                    break;
                case UserTypeEnum.Customer:
                    password = getUserPasswordCustomer(username);
                    break;
                case UserTypeEnum.Operator:
                    password = getUserPasswordOperator(username);
                    break;
                case UserTypeEnum.Employee:
                    password = getUserPasswordEmployee(username);
                    break;
            }

            return password;
        }

        public string getUserToken(string type, long id)
        {
            string token = string.Empty;

            UserTypeEnum userType = (UserTypeEnum)Enum.Parse(typeof(UserTypeEnum), type);

            switch (userType)
            {
                case UserTypeEnum.Customer:
                    token = getUserTokenCustomer(id);
                    break;

                case UserTypeEnum.Operator:
                    token = getUserTokenOperator(id);
                    break;

                case UserTypeEnum.Employee:
                    token = getUserTokenEmployee(id);
                    break;
            }

            return token;
        }

        private string getUserPasswordCustomer(string username)
        {
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    return dbcontext.Customer.FirstOrDefault(x => x.email.Equals(username)).password;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("getUserPasswordCustomer - e:" + e.StackTrace + ":" + e.InnerException);
                    return string.Empty;
                }
            }
        }

        private string getUserTokenCustomer(long id)
        {
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    return dbcontext.Customer.FirstOrDefault((System.Linq.Expressions.Expression<Func<Customer, bool>>)(x => x.ID == id)).BBToken;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("getUserPasswordCustomer - e:" + e.StackTrace + ":" + e.InnerException);
                    return string.Empty;
                }
            }
        }

        private string getUserPasswordOperator(string username)
        {
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    return dbcontext.OpenJobOperator.FirstOrDefault(x => x.username.Equals(username)).password;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("getUserPasswordOperator - e:" + e.StackTrace + ":" + e.InnerException);
                    return e.Message;
                }
            }
        }

        private string getUserTokenOperator(long id)
        {
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    return dbcontext.OpenJobOperator.FirstOrDefault(x => x.ID == id).BBToken;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine("getUserPasswordOperator - e:" + e.StackTrace + ":" + e.InnerException);
                    return string.Empty;
                }
            }
        }

        private string getUserPasswordEmployee(string username)
        {
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    return dbcontext.Employee.FirstOrDefault(x => x.email.Equals(username)).password;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        private string getUserTokenEmployee(long id)
        {
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    return dbcontext.Employee.FirstOrDefault(x => x.ID == id).BBToken;
                }
                catch (Exception)
                {
                    return string.Empty;
                }
            }
        }

        public UserTypeModel getUserTypeFromCredential(string username)
        {
            long id = -1;
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    OpenJobOperator op = dbcontext.OpenJobOperator.FirstOrDefault(x => x.username == username);
                    if (op != null) id = op.ID;
                    if (id != -1) return new UserTypeModel()
                    {
                        type = UserTypeEnum.Operator,
                        id = id
                    };

                    Employee emp = dbcontext.Employee.FirstOrDefault(x => x.email == username);
                    if (emp != null) id = emp.ID;
                    if (id != -1) return new UserTypeModel()
                    {
                        type = UserTypeEnum.Employee,
                        id = id
                    };

                    Customer cus = dbcontext.Customer.FirstOrDefault(x => x.email == username);
                    if (cus != null) id = cus.ID;
                    if (id != -1 && dbcontext.Company.Where(x => x.ownerId == cus.ID).FirstOrDefault() != null) return new UserTypeModel()
                    {
                        type = UserTypeEnum.Owner,
                        id = id
                    };

                    Customer cust = dbcontext.Customer.FirstOrDefault(x => x.email == username);
                    if (cust != null) id = cust.ID;
                    if (id != -1) return new UserTypeModel()
                    {
                        type = UserTypeEnum.Customer,
                        id = id
                    };

                    return null;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public Tuple<PersonModelBase, string> getUserFromCredential(string username)
        {
            string exmsg = string.Empty;
            PersonModelBase person = null;
            UserTypeModel model = getUserTypeFromCredential(username);
            try
            {
                switch (model.type)
                {
                    case UserTypeEnum.Operator:
                        OpenJobOperator oper = GetDBContext().OpenJobOperator.FirstOrDefault(x => x.ID == model.id);
                        person = new PersonModelBase()
                        {
                            id = oper.ID,
                            email = oper.username,
                            password = oper.password,
                            type = model.type,
                            fiscalCode = ""
                        };
                        break;
                    case UserTypeEnum.Customer:
                        Customer cust = GetDBContext().Customer.FirstOrDefault(x => x.ID == model.id);
                        person = new PersonModelBase()
                        {
                            id = cust.ID,
                            email = cust.email,
                            password = cust.password,
                            type = model.type,
                            fiscalCode = "",
                            secondaryId = cust.Company.FirstOrDefault().ID
                        };
                        break;
                    case UserTypeEnum.Employee:
                        Employee empl = GetDBContext().Employee.FirstOrDefault(x => x.ID == model.id);
                        person = new PersonModelBase()
                        {
                            id = empl.ID,
                            email = empl.email,
                            password = empl.password,
                            type = model.type,
                            fiscalCode = empl.fiscalCode,
                            secondaryId = getCompanyByEmployeeCode(empl.employeeCode).Item1.ID
                        };
                        break;
                    case UserTypeEnum.Owner:
                        Customer owner = GetDBContext().Customer.FirstOrDefault(x => x.ID == model.id);
                        person = new PersonModelBase()
                        {
                            id = owner.ID,
                            email = owner.email,
                            password = owner.password,
                            type = model.type,
                            fiscalCode = "",
                            secondaryId = owner.Company.FirstOrDefault().ID
                        };
                        break;
                    default:
                        exmsg = "Utente inesistente";
                        break;
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<PersonModelBase, string>(person, exmsg);
        }

        public string updateUserPasswordCustomer(string email, string password)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Customer customer = dbcontext.Customer.FirstOrDefault(x => x.email.Equals(email));

                        if (customer != null)
                            customer.password = MyHelper.GetEncryptedPassword(password);

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string updateUserPasswordEmployee(string email, string password)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Employee employee = dbcontext.Employee.FirstOrDefault(x => x.email.Equals(email));

                        if (employee != null)
                            employee.password = MyHelper.GetEncryptedPassword(password);

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string resetUserPassword(PersonModelBase person)
        {
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    string newPassword = string.Empty;
                    string newHashedPassword = string.Empty;
                    try
                    {
                        switch (person.type)
                        {
                            case UserTypeEnum.Customer:

                                newPassword = MyHelper.makeRandomPassword(8);
                                newHashedPassword = MyHelper.GetEncryptedPassword(newPassword);
                                Customer customer = dbcontext.Customer.Find(person.id);
                                if (customer != null)
                                    customer.password = newHashedPassword;
                                dbcontext.SaveChanges();
                                dbContextTransaction.Commit();
                                break;

                            case UserTypeEnum.Employee:

                                newPassword = MyHelper.makeRandomPassword(8);
                                newHashedPassword = MyHelper.GetEncryptedPassword(newPassword);
                                Employee employee = dbcontext.Employee.Find(person.id);
                                if (employee != null)
                                    employee.password = newHashedPassword;
                                dbcontext.SaveChanges();
                                dbContextTransaction.Commit();
                                break;

                            case UserTypeEnum.Owner:

                                newPassword = MyHelper.makeRandomPassword(8);
                                newHashedPassword = MyHelper.GetEncryptedPassword(newPassword);
                                Customer owner = dbcontext.Customer.Find(person.id);
                                if (owner != null)
                                    owner.password = newHashedPassword;
                                dbcontext.SaveChanges();
                                dbContextTransaction.Commit();
                                break;
                        }
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        newPassword = null;
                    }
                    return newPassword;
                }
            }
        }
        #endregion

        #region TEMPORARY COMPANY TO COMPANY
        public Tuple<Company, string> updateTemporaryCompanyToCompany(long companyId, long customerId)
        {
            Company company = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        /* 1) get TemporaryCompany */
                        company = dbcontext.Company.FirstOrDefault(x => x.ID == companyId);

                        /* 2) set ownership */
                        if (company != null)
                        {
                            /* set ownerId and insert services */
                            company.isActive = true;
                            company.ownerId = customerId;
                            company.isTemporary = false;
                            company.serviceId = company.serviceId;

                            /* 3) save */
                            dbcontext.SaveChanges();

                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<Company, string>(company, exmsg);
        }
        #endregion

        #region OPERATOR
        public string insertOperator(OpenJobOperatorModel model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        OpenJobOperator tc = new OpenJobOperator()
                        {
                            username = model.username,
                            password = model.password
                        };
                        dbcontext.OpenJobOperator.Add(tc);
                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return exmsg;
        }

        public bool isOpenJobOperatorAlreadyExistant(OpenJobOperatorModel model)
        {
            bool isAlreadyExistant = false;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    OpenJobOperator openJobOperator = dbcontext.OpenJobOperator.First(x => x.username.Equals(model.username));
                    if (openJobOperator != null)
                        isAlreadyExistant = true;
                }
                catch (Exception)
                { }
            }

            return isAlreadyExistant;
        }

        public Tuple<Customer, string> getCustomerByUsername(string username)
        {
            Customer customer = null;
            string exmsg = string.Empty;
            try
            {
                customer = GetDBContext().Customer.Where(x => x.email.Equals(username, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Customer, string>(customer, exmsg);
        }

        #endregion

        #region COMPANIES SUMMARY
        public Tuple<List<CompanySummaryResponseModel>, string> getCompaniesSummary(List<string> groups)
        {

            HashSet<UnitaDiBusiness> udbs = getUDBS(groups);
            List<CompanySummaryResponseModel> companiesList = new List<CompanySummaryResponseModel>();
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Administration> admins = new List<Administration>();

                    foreach (UnitaDiBusiness udb in udbs)
                    {
                        admins.AddRange(dbcontext.Administration.Where(x => x.businessUnitID.Equals(udb.IDUnitaDiBusiness)).ToList());
                    }

                    admins = admins.GroupBy(y => y.customerCode).Select(z => z.First()).ToList();

                    foreach (Administration ad in admins)
                    {
                        Company x = getCompanyByAdministrationNumber(ad.administrationCode).Item1;
                        Customer y = null;
                        if (x != null)
                        {
                            if (x.ownerId != null && x.ownerId != 0)
                            {
                                y = getOwner((long)x.ownerId).Item1;
                            }
                            CompanySummaryResponseModel comp = CompanySummaryResponseModel.Create(x, y);
                            if (comp != null && !companiesList.Contains(comp))
                            {
                                companiesList.Add(comp);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }
            return new Tuple<List<CompanySummaryResponseModel>, string>(companiesList, exmsg);
        }

        public Tuple<List<CompanySummaryResponseModel>, string> getCompaniesSummary()
        {
            List<CompanySummaryResponseModel> companiesList = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Company> companies = dbcontext.Company.ToList();

                    if (companies != null)
                    {
                        companiesList = new List<CompanySummaryResponseModel>();
                        foreach (var company in companies)
                        {
                            Customer customer = null;

                            if (company.Customer != null)
                                customer = company.Customer;

                            companiesList.Add(CompanySummaryResponseModel.Create(company, customer));
                        }
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<List<CompanySummaryResponseModel>, string>(companiesList, exmsg);
        }

        public Tuple<List<TemporaryCompanySummaryResponseModel>, string> getTemporaryCompaniesSummary()
        {
            List<TemporaryCompanySummaryResponseModel> companiesList = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Company> companies = dbcontext.Company.Where(x => x.ownerId == null).ToList();

                    if (companies != null)
                    {
                        companiesList = new List<TemporaryCompanySummaryResponseModel>();
                        foreach (var company in companies) companiesList.Add(TemporaryCompanySummaryResponseModel.Create(company));
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<List<TemporaryCompanySummaryResponseModel>, string>(companiesList, exmsg);
        }

        public Tuple<List<TemporaryCompanySummaryResponseModel>, string> getTemporaryCompaniesSummary(List<string> groups)
        {
            List<TemporaryCompanySummaryResponseModel> companiesList = new List<TemporaryCompanySummaryResponseModel>();
            HashSet<UnitaDiBusiness> udbs = getUDBS(groups);
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Administration> admins = new List<Administration>();
                    foreach (UnitaDiBusiness udb in udbs)
                    {
                        admins.AddRange(dbcontext.Administration.Where(x => x.businessUnitID.Equals(udb.IDUnitaDiBusiness)).ToList());
                    }

                    foreach (Administration ad in admins)
                    {
                        Company x = getCompanyByAdministrationNumber(ad.administrationCode).Item1;
                        if (x != null && (x.ownerId == null || x.ownerId == 0))
                        {
                            companiesList.Add(TemporaryCompanySummaryResponseModel.Create(x));
                        }
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<List<TemporaryCompanySummaryResponseModel>, string>(companiesList, exmsg);
        }

        public Tuple<List<Company>, string> getCustomerCompanies(long customerId)
        {
            List<Company> companies = new List<Company>();
            List<CustomerCompanyRelation> relations = new List<CustomerCompanyRelation>();
            string exmsg = string.Empty;
            using (var context = GetDBContext())
            {
                try
                {
                    relations = context.CustomerCompanyRelation.Where(x => x.idCustomer == customerId).ToList();
                    foreach (CustomerCompanyRelation r in relations)
                    {
                        companies.Add(context.Company.Where(x => x.ID == r.idCompany).FirstOrDefault());
                    }
                    if (companies.Count == 0)
                    {
                        companies.Add(context.Company.Where(x => x.ownerId == customerId).FirstOrDefault());
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            return new Tuple<List<Company>, string>(companies, exmsg);
        }

        #endregion

        #region COMPANY
        public Tuple<GetCompanyDetailResponseModel, string> getCompanyDetail(long companyId)
        {
            GetCompanyDetailResponseModel response = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    Company company = dbcontext.Company.First(x => x.ID == companyId);
                    Administration administration = getAdministrationByCompanyID(companyId).Item1;
                    List<Customer> customers = null;
                    List<Employee> employees = null;
                    Tuple<Customer, string> result = getOwner((long)company.ownerId);
                    if (string.IsNullOrEmpty(result.Item2))
                    {
                        List<CustomerCompanyRelation> relations = dbcontext.CustomerCompanyRelation.Where(x => x.idCompany == company.ID).ToList();
                        if (relations != null && relations.Count > 0)
                        {
                            customers = new List<Customer>();
                            foreach (var relation in relations)
                            {
                                customers.Add(dbcontext.Customer.Find(relation.idCustomer));
                            }
                        }

                        employees = getCompanyEmployees(company.ID).Item1;
                    }
                    else
                        exmsg = result.Item2;

                    response = GetCompanyDetailResponseModel.Create(company, administration, result.Item1, customers, employees);
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<GetCompanyDetailResponseModel, string>(response, exmsg);
        }

        public Tuple<Company, string> getCompanyFromCode(string code)
        {
            Company company = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    company = dbcontext.Company.FirstOrDefault(x => x.companyCode.Equals(code));
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<Company, string>(company, exmsg);
        }

        public Tuple<Company, string> getCompanyById(long id)
        {
            Company company = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    company = dbcontext.Company.Find(id);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<Company, string>(company, exmsg);
        }

        public Tuple<Company, string> getCompanyByEmployeeCode(string empCode)
        {
            Company company = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    company = dbcontext.Company.FirstOrDefault(x => x.companyCode == dbcontext.servicebus.FirstOrDefault(y => y.employeeCode.Equals(empCode)).companyCode);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<Company, string>(company, exmsg);
        }

        public Tuple<Company, string> getCompanyByAdministrationNumber(string administrationNumber)
        {
            Company company = null;
            string exmsg = string.Empty;
            Administration administration = null;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    administration = dbcontext.Administration.Where(x => x.administrationCode.Equals(administrationNumber)).FirstOrDefault();
                    company = dbcontext.Company.FirstOrDefault(x => x.companyCode.Equals(administration.customerCode));
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<Company, string>(company, exmsg);
        }

        public Tuple<Company, string> getcompanyByServiceId(long id)
        {
            Company company = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    company = dbcontext.Company.FirstOrDefault(x => x.serviceId == id);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<Company, string>(company, exmsg);
        }

        public Tuple<long, string> insertTemporaryCompany(CompanyModel model, bool isEasy = false)
        {
            long id = -1;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        bool newCompany = false;
                        Company tempcompany = dbcontext.Company.Where(x => x.companyCode == model.companyCode).FirstOrDefault();
                        if (tempcompany == null)
                        {
                            Company company = new Company()
                            {
                                businessName = model.name,
                                companyCode = model.companyCode,
                                email = model.email ?? "",
                                vat = model.vat ?? "",
                                addressId = model.addressId,
                                isActive = false,
                                isBBSync = false,
                                isTemporary = true,
                                phone = model.phone,
                                serviceId = insertService().Item1
                            };

                            tempcompany = dbcontext.Company.Add(company);

                            dbcontext.SaveChanges();

                            id = tempcompany.ID;
                            newCompany = true;
                        }
                        else
                        {
                            tempcompany.businessName = model.name;
                            tempcompany.companyCode = model.companyCode;
                            tempcompany.email = model.email;
                            tempcompany.vat = model.vat;
                            tempcompany.addressId = model.addressId;
                            tempcompany.phone = model.phone;
                            dbcontext.SaveChanges();
                        }
                        dbContextTransaction.Commit();
                        if (newCompany)
                            EmailSender.sendEmailMichele("Nuovo Cliente", "Salve, è stato inserito il cliente: " + tempcompany.businessName + ", Codice Numero: " + tempcompany.companyCode, isEasy);
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(id, exmsg);
        }

        public bool isCompanyAlreadyExistant(CompanyModel model)
        {
            bool isExistant = false;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    Company company = dbcontext.Company.FirstOrDefault(x => x.businessName.Equals(model.name) && x.companyCode.Equals(model.companyCode));

                    if (company != null)
                        isExistant = true;
                }
                catch (Exception)
                { }
            }

            return isExistant;
        }

        public bool isCompanyOwnerAssigned(long companyId)
        {
            bool isAssigned = false;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    Company company = dbcontext.Company.FirstOrDefault(x => x.ID == companyId);

                    if (company.ownerId != null)
                        isAssigned = true;
                }
                catch (Exception)
                { }
            }

            return isAssigned;
        }

        public string updateCompany(CompanyModel model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Company company = dbcontext.Company.FirstOrDefault(x => x.ID == model.id);

                        /* company */
                        company.businessName = model.name;
                        company.email = model.email;
                        company.companyCode = model.companyCode;
                        company.vat = model.vat;

                        /* company address */
                        dbcontext.Entry(company).Reference(x => x.Address).Load();
                        company.Address.street = model.address.street;
                        company.Address.city = model.address.city;
                        company.Address.zip = model.address.zip;
                        company.Address.state = model.address.state;
                        company.Address.country = model.address.country;

                        //dbcontext.Entry(company).Reference(x => x.Phone).Load();
                        /* company phones */
                        //if (model.phones != null && model.phones.Count() > 0)
                        //{
                        //    foreach (var phone in model.phones)
                        //    {
                        //        company.Phone.FirstOrDefault(x => x.label.Equals(phone.label)).number = phone.number;
                        //    }
                        //}

                        /* company contract */
                        DateTime now = DateTime.Now.Date;
                        Administration currentContract = (from x in dbcontext.Administration
                                                          join y in dbcontext.Company on x.customerCode equals y.companyCode
                                                          where now > x.provisionStartDate && now < x.provisionEndDate
                                                          select x).FirstOrDefault();
                        if (currentContract != null)
                        {
                            currentContract.provisionStartDate = model.startDate;
                            currentContract.provisionEndDate = model.endDate;
                        }

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string updateImage(UpdateImageModel model, byte[] file)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        if (model.type)
                        {
                            Media media = getMediaByCompanyId(model.id).Item1;
                            if (media != null)
                            {
                                media = dbcontext.Media.Find(media.ID);
                                media.data = file;
                            }
                            else
                            {
                                media = new Media()
                                {
                                    companyId = model.id,
                                    data = file
                                };
                                dbcontext.Media.Add(media);
                            }
                            dbcontext.SaveChanges();
                        }
                        else
                        {
                            Media media = getMediaByEmployeeId(model.id).Item1;
                            if (media != null)
                            {
                                media = dbcontext.Media.Find(media.ID);
                                media.data = file;
                            }
                            else
                            {
                                media = new Media()
                                {
                                    employeeId = model.id,
                                    data = file
                                };
                                dbcontext.Media.Add(media);
                            }
                            dbcontext.SaveChanges();
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string setCompanyIsActive(SetCompanyIsActiveModel model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        /* 1) get Temporarycompany */
                        Company c = dbcontext.Company.FirstOrDefault(x => x.ID == model.id);

                        c.isActive = model.isActive;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string setCompanyBBSync(long companyId, long BBId)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Company c = dbcontext.Company.Find(companyId);
                        c.isBBSync = true;
                        c.BBId = BBId;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return exmsg;
        }
        #endregion

        #region CUSTOMER
        public Tuple<Customer, string> getCustomer(long customerId)
        {
            Customer customer = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    customer = dbcontext.Customer.Find(customerId);
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<Customer, string>(customer, exmsg);
        }

        public Tuple<Customer, string> getOwner(long ownerId)
        {
            Customer customer = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    customer = dbcontext.Customer.Find(ownerId);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<Customer, string>(customer, exmsg);
        }

        public Tuple<Customer, string> getOwnerFromEmployee(Employee emp)
        {
            Customer customer = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    servicebus sb = dbcontext.servicebus.Where(y => y.employeeCode == emp.employeeCode).FirstOrDefault();
                    Company c = dbcontext.Company.Where(co => co.companyCode.Equals(sb.companyCode)).FirstOrDefault();
                    customer = dbcontext.Customer.Where(x => x.ID == c.ownerId).FirstOrDefault();
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<Customer, string>(customer, exmsg);
        }

        public Tuple<List<CustomerModel>, string> getCustomers()
        {
            List<CustomerModel> customers = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Customer> cs = dbcontext.Customer.ToList();
                    if (cs != null && cs.Count > 0)
                    {
                        customers = new List<CustomerModel>();
                        foreach (var c in cs)
                            customers.Add(CustomerModel.Create(c));
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<List<CustomerModel>, string>(customers, exmsg);
        }

        public Tuple<List<Customer>, string> getCustomersListForDocument()
        {
            List<Customer> result = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Customer> customers = new List<Customer>();
                    customers = dbcontext.Customer.ToList();
                    foreach (Customer c in customers)
                    {
                        Service service = dbcontext.Service.Find(c.serviceId);
                        if (service.documenti == true)
                        {
                            result.Add(c);
                        }
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<List<Customer>, string>(result, exmsg);
        }

        public Tuple<List<Customer>, string> getCustomersListForTimeSheet()
        {
            List<Customer> result = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Customer> customers = new List<Customer>();
                    customers = dbcontext.Customer.ToList();
                    foreach (Customer c in customers)
                    {
                        Service service = dbcontext.Service.Find(c.serviceId);
                        if (service.rapportini == true)
                        {
                            result.Add(c);
                        }
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<List<Customer>, string>(result, exmsg);
        }

        public Tuple<string, string> getCustomerBBToken(long customerId)
        {
            string token = string.Empty;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    token = dbcontext.Customer.FirstOrDefault((System.Linq.Expressions.Expression<Func<Customer, bool>>)(x => x.ID == customerId)).BBToken;
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<string, string>(token, exmsg);
        }

        public Tuple<string, string> getCustomerEmail(long customerId)
        {
            string email = string.Empty;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    email = dbcontext.Customer.FirstOrDefault(x => x.ID == customerId).email;
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<string, string>(email, exmsg);
        }

        public Tuple<long, string> insertCustomer(CustomerModel model, long feaId)
        {
            string exmsg = string.Empty;
            long customerId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Company comp = dbcontext.Company.Find(model.idCompany);
                        Customer cus = dbcontext.Customer.Where(x => x.email.Equals(model.email)).FirstOrDefault();
                        if (cus != null)
                        {
                            exmsg = "Utente non valido o già esistente";
                            return new Tuple<long, string>(customerId, exmsg);
                        }
                        Customer c = new Customer()
                        {
                            name = model.name,
                            lastName = model.lastName,
                            email = model.email,
                            password = model.password,
                            isBBSync = false,
                            isActive = true,
                            feaId = feaId
                        };

                        if (model.roleId != 0)
                            c.serviceId = insertService(model.documenti, model.rapportini, model.firma).Item1;
                        else
                            c.serviceId = comp.serviceId;

                        Customer addedCustomer = dbcontext.Customer.Add(c);
                        dbcontext.SaveChanges();
                        customerId = addedCustomer.ID;

                        if (model.roleId != 0)
                        {
                            CustomerCompanyRelation ccr = new CustomerCompanyRelation()
                            {
                                idCustomer = customerId,
                                idCompany = model.idCompany
                            };

                            dbcontext.CustomerCompanyRelation.Add(ccr);
                            dbcontext.SaveChanges();
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception)
                    {
                        exmsg = "Utente non valido o già esistente";
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(customerId, exmsg);
        }

        public bool isCustomerAlreadyExistant(CustomerModel model)
        {
            bool isExistant = false;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    Customer customer = dbcontext.Customer.FirstOrDefault(x => x.name.Equals(model.name) && x.lastName.Equals(model.lastName));

                    if (customer != null)
                        isExistant = true;
                }
                catch (Exception)
                { }
            }

            return isExistant;
        }

        public string setCustomerBBSync(long customerId, BadgeBoxCreateUserResponse bbres)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Customer c = dbcontext.Customer.FirstOrDefault(x => x.ID == customerId);
                        c.isBBSync = true;
                        c.BBToken = bbres.token;
                        c.BBId = bbres.id;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                        return bbres.token;
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string setCustomerIsActive(SetCustomerIsActiveModel model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        /* 1) get Temporarycompany */
                        Customer c = dbcontext.Customer.FirstOrDefault(x => x.ID == model.id);

                        c.isActive = model.isActive;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string updateCustomer(CustomerModel model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Customer customer = dbcontext.Customer.Find(model.id);

                        customer.name = model.name;
                        customer.lastName = model.lastName;
                        //customer.email = model.email;
                        //customer.password = model.password;

                        Service service = dbcontext.Service.Find(customer.serviceId);

                        service.documenti = model.documenti;
                        service.rapportini = model.rapportini;
                        service.firma = model.firma;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public Tuple<List<Customer>, string> getCompanyCustomers(long companyId)
        {
            List<CustomerCompanyRelation> ccrels = null;
            string exmsg = string.Empty;
            try
            {
                ccrels = GetDBContext().CustomerCompanyRelation.Where(x => x.idCompany == companyId).ToList();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            List<Customer> customers = new List<Customer>();
            foreach (CustomerCompanyRelation ccrel in ccrels)
            {
                customers.Add(ccrel.Customer);
            }
            return new Tuple<List<Customer>, string>(customers, exmsg);
        }
        #endregion

        #region EMPLOYEE
        public Tuple<long, string> insertEmployee(EmployeeModel model)
        {
            string exmsg = string.Empty;
            long employeeId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Employee e = new Employee()
                        {
                            email = model.email,
                            password = model.password,
                            fiscalCode = model.fiscalCode,
                            isActive = true,
                            lastName = model.lastName,
                            name = model.name
                        };

                        Employee addedEmployee = dbcontext.Employee.Add(e);

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();

                        employeeId = addedEmployee.ID;
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<long, string>(employeeId, exmsg);
        }

        public Tuple<long, string> insertEmployee(Employee model)
        {
            string exmsg = string.Empty;
            long employeeId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    string pass = MyHelper.CreateNewPassword(8);

                    try
                    {
                        Employee emptemp = dbcontext.Employee.Where(x => x.employeeCode == model.employeeCode).FirstOrDefault();
                        if (emptemp == null)
                        {
                            EmailSender.sendEmailMichele("Password Lavoratore", "PASS: " + pass, true);
                            MyHelper.LogEMP(model.email + " || " + pass);
                            //inserire l'invio della mail quì con la password generata prima dell'encrypt
                            model.password = MyHelper.GetEncryptedPassword(pass);
                            model.isActive = false;
                            emptemp = dbcontext.Employee.Add(model);
                            dbcontext.SaveChanges();
                            employeeId = emptemp.ID;
                            dbContextTransaction.Commit();

                            EmailSender.sendEmail(model.email, "Nuova Utenza", "Salve, è stata creata ed abilitata la sua nuova utenza, User: " + model.email + ", Password: " + pass, true);
                        }
                        else
                        {
                            emptemp = dbcontext.Employee.Find(emptemp.ID);
                            emptemp.name = model.name;
                            emptemp.lastName = model.lastName;
                            if (model.FirmaElettronicaAvanzata != null)
                                emptemp.fea = model.fea;
                            emptemp.fiscalCode = model.fiscalCode;
                            emptemp.phone = model.phone;

                            dbcontext.SaveChanges();
                            employeeId = emptemp.ID;

                            dbContextTransaction.Commit();
                        }
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<long, string>(employeeId, exmsg);
        }

        public bool isEmployeeAlreadyExistant(EmployeeModel model)
        {
            bool isAlreadyExistant = false;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    Employee employee = dbcontext.Employee.First(x => x.name.Equals(model.name) && x.lastName.Equals(model.lastName) && x.fiscalCode.Equals(model.fiscalCode));
                }
                catch (Exception)
                { }
            }

            return isAlreadyExistant;
        }

        public Tuple<Employee, string> getEmployee(long employeeId)
        {
            Employee employee = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    employee = dbcontext.Employee.Find(employeeId);
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<Employee, string>(employee, exmsg);
        }

        public Tuple<List<Employee>, string> getEmployeesActiveList()
        {
            List<Employee> employees = new List<Employee>();
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    employees = dbcontext.Employee.Where(x => x.isActive == true).ToList();
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<List<Employee>, string>(employees, exmsg);
        }

        public string setEmployeeBBSync(long employeeId, string bbToken, long bbId)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Employee e = dbcontext.Employee.First(x => x.ID == employeeId);
                        e.isBBSync = true;
                        e.BBToken = bbToken;
                        e.BBId = bbId;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return exmsg;
        }

        public Tuple<Employee, string> getEmployeeByUsername(string username)
        {
            Employee employee = null;
            string exmsg = string.Empty;
            try
            {
                employee = GetDBContext().Employee.Where(x => x.email.Equals(username)).FirstOrDefault();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Employee, string>(employee, exmsg);
        }

        public Tuple<Employee, string> getEmployeeByCode(string code)
        {
            Employee employee = null;
            string exmsg = string.Empty;
            try
            {
                employee = GetDBContext().Employee.Where(x => x.employeeCode == code).FirstOrDefault();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Employee, string>(employee, exmsg);
        }

        public Tuple<List<Employee>, string> getCompanyEmployees(long companyId)
        {
            Company company = GetDBContext().Company.Find(companyId);
            List<string> employeesCodes = null;
            List<Employee> employees = new List<Employee>();
            string exmsg = string.Empty;
            try
            {
                employeesCodes = GetDBContext().servicebus.Where(x => x.companyCode == company.companyCode).Select(y => y.employeeCode).ToList();
                foreach (string s in employeesCodes)
                {
                    employees.Add(getEmployeeByCode(s).Item1);
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Employee>, string>(employees, exmsg);
        }

        public Tuple<List<Employee>, string> getCustomerEmployees(TokenModel model)
        {
            PersonModelBase person = getUserFromToken(model.token, null);
            List<Employee> employees = new List<Employee>();
            string exmsg = string.Empty;
            try
            {
                using (var dbcontext = GetDBContext())
                {
                    Company comp = null;

                    if (person.type == UserTypeEnum.Owner)
                        comp = dbcontext.Company.Where(x => x.ownerId == person.id).FirstOrDefault();
                    else
                        comp = dbcontext.Company.Where(c => c.ID == (dbcontext.CustomerCompanyRelation.Where(y => y.idCustomer == person.id).Select(z => z.idCompany).FirstOrDefault())).FirstOrDefault();
                    List<Administration> admins = dbcontext.Administration.Where(z => z.customerCode.Equals(comp.companyCode)).ToList();
                    Administration admin = admins.Where(adm => adm.provisionEndDate == admins.Max(m => m.provisionEndDate)).First();
                    List<Engagement> engage = dbcontext.Engagement.Where(y => y.administrationNumber.Equals(admin.administrationCode)).ToList();
                    foreach (Engagement e in engage)
                    {
                        List<Extension> exts = dbcontext.Extension.Where(ext => ext.engagementId == e.ID).ToList();
                        DateTime? last = exts.Max(ex => ex.endDate);
                        if (last == null) last = e.missionStopDate;
                        //if (e.missionStartDate < DateTime.Now && DateTime.Now < last)
                        Employee empl = dbcontext.Employee.Where(emp => emp.employeeCode.Equals(e.employeeCode)).FirstOrDefault();
                        if (empl != null)
                            employees.Add(empl);
                    }
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Employee>, string>(employees, exmsg);
        }

        public Tuple<List<EmployeeResponseModel>, string> getOperatorEmployees()
        {
            List<EmployeeResponseModel> employeesModels = null;
            string exmsg = string.Empty;
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Employee> employees = GetDBContext().Employee.ToList();

                    if (employees != null && employees.Count > 0)
                    {
                        employeesModels = new List<EmployeeResponseModel>();

                        foreach (var employee in employees)
                        {
                            Engagement engagement = getEngagementByEmployeeID(employee.ID).Item1;
                            if (engagement != null)
                            {
                                List<Extension> exts = getExtensionsByEngagementID(engagement.ID).Item1;
                                Company company = getCompanyByEmployeeCode(employee.employeeCode).Item1;
                                BadgeBoxGetRecordResponse getRecords = new BadgeBoxGetRecordResponse();
                                if (employee.isBBSync)
                                {
                                    getRecords = BadgeBoxServer.getRecords(new BadgeBoxGetRecordsModel()
                                    {
                                        user_token = employee.BBToken,
                                        id_empl = (long)employee.BBId
                                    }).Item1;
                                }
                                string branchName = string.Empty;
                                if (!string.IsNullOrEmpty(engagement.businessUnitID))
                                {
                                    UnitaDiBusiness udb = dbcontext.UnitaDiBusiness.Where(y => y.IDUnitaDiBusiness.Equals(engagement.businessUnitID)).FirstOrDefault();
                                    if (udb != null)
                                        branchName = udb.Filiale;
                                }
                                if (getRecords.records != null)
                                    employeesModels.Add(EmployeeResponseModel.Create(employee, engagement, exts, company, RecordsModel.Create(getRecords), branchName));
                                else
                                    employeesModels.Add(EmployeeResponseModel.Create(employee, engagement, exts, company, new RecordsModel(), branchName));
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }
            return new Tuple<List<EmployeeResponseModel>, string>(employeesModels, exmsg);
        }

        public Tuple<List<EmployeeResponseModel>, string> getOperatorEmployees(List<string> groups)
        {
            List<EmployeeResponseModel> employeesModels = null;
            HashSet<UnitaDiBusiness> udbs = getUDBS(groups);
            string exmsg = string.Empty;
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Engagement> engagements = new List<Engagement>();
                    List<Employee> employees = new List<Employee>();
                    List<Tuple<Employee, Engagement>> empEng = new List<Tuple<Employee, Engagement>>();

                    foreach (UnitaDiBusiness udb in udbs)
                    {
                        engagements.AddRange(dbcontext.Engagement.Where(x => x.businessUnitID.Equals(udb.IDUnitaDiBusiness)).ToList());
                    }

                    engagements = engagements.GroupBy(x => x.employeeCode).Select(x => x.First()).ToList();

                    foreach (Engagement e in engagements)
                    {
                        Employee temp = getEmployeeByCode(e.employeeCode).Item1;
                        if (temp != null)
                        {
                            employees.Add(temp);
                            empEng.Add(new Tuple<Employee, Engagement>(temp, e));
                        }
                    }

                    if (employees != null && employees.Count > 0)
                    {
                        employeesModels = new List<EmployeeResponseModel>();

                        foreach (var employeeEngagement in empEng)
                        {
                            Engagement engagement = employeeEngagement.Item2;
                            List<Extension> exts = getExtensionsByEngagementID(engagement.ID).Item1;
                            Employee employee = employeeEngagement.Item1;
                            Company company = getCompanyByEmployeeCode(employee.employeeCode).Item1;
                            BadgeBoxGetRecordResponse getRecords = new BadgeBoxGetRecordResponse();
                            if (employee.isBBSync)
                            {
                                getRecords = BadgeBoxServer.getRecords(new BadgeBoxGetRecordsModel()
                                {
                                    user_token = employee.BBToken,
                                    id_empl = (long)employee.BBId
                                }).Item1;
                            }
                            string branchName = dbcontext.UnitaDiBusiness.FirstOrDefault(x => x.IDUnitaDiBusiness.Equals(engagement.businessUnitID)).Filiale;
                            if (getRecords.records != null)
                                employeesModels.Add(EmployeeResponseModel.Create(employee, engagement, exts, company, RecordsModel.Create(getRecords), branchName));
                            else
                                employeesModels.Add(EmployeeResponseModel.Create(employee, engagement, exts, company, new RecordsModel(), branchName));
                        }
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }
            return new Tuple<List<EmployeeResponseModel>, string>(employeesModels, exmsg);
        }

        public Tuple<List<EmployeeResponseModel>, string> getCompanyMonthlyEmployees(TokenModel model)
        {
            string exmsg = string.Empty;
            List<EmployeeResponseModel> employeesResponse = new List<EmployeeResponseModel>();
            try
            {
                using (var dbcontext = GetDBContext())
                {
                    List<Employee> employees = null;
                    if (model.azure != null && model.azure != "")
                    {
                        employees = dbcontext.Employee.Where(x => x.companyId == model.id).ToList();
                        foreach (Employee e in employees)
                        {
                            Engagement engagement = dbcontext.Engagement.FirstOrDefault(y => y.employeeId == e.ID && y.missionStartDate.Value < DateTime.Now && dbcontext.Extension.First(z => z.engagementId == y.ID).endDate > DateTime.Now);
                            if (engagement != null)
                            {
                                Extension ext = dbcontext.Extension.FirstOrDefault(x => x.engagementId == engagement.ID);
                                employeesResponse.Add(new EmployeeResponseModel()
                                {
                                    name = e.name,
                                    lastName = e.lastName,
                                    engagementStart = engagement.missionStartDate.ToString(),
                                    engagementEnd = ext.endDate.ToString(),
                                    //aggiungere ore lavorate
                                });
                            }
                        }
                        return new Tuple<List<EmployeeResponseModel>, string>(employeesResponse, exmsg);
                    }
                    PersonModelBase person = getUserFromToken(model.token, model.azure);
                    switch (person.type)
                    {
                        case UserTypeEnum.Operator:
                            employees = dbcontext.Employee.Where(x => x.companyId == model.id).ToList();
                            foreach (Employee e in employees)
                            {
                                Engagement engagement = dbcontext.Engagement.FirstOrDefault(y => y.employeeId == e.ID && y.missionStartDate.Value < DateTime.Now && dbcontext.Extension.First(z => z.engagementId == y.ID).endDate > DateTime.Now);
                                if (engagement != null)
                                {
                                    Extension ext = dbcontext.Extension.FirstOrDefault(x => x.engagementId == engagement.ID);
                                    employeesResponse.Add(new EmployeeResponseModel()
                                    {
                                        name = e.name,
                                        lastName = e.lastName,
                                        engagementStart = engagement.missionStartDate.ToString(),
                                        engagementEnd = ext.endDate.ToString(),
                                        //aggiungere ore lavorate
                                    });
                                }
                            }
                            break;
                        case UserTypeEnum.Customer:
                            Company company = getCustomerCompanies(person.id).Item1.FirstOrDefault();
                            employees = dbcontext.Employee.Where(x => x.companyId == company.ID).ToList();
                            foreach (Employee e in employees)
                            {
                                Engagement engagement = dbcontext.Engagement.FirstOrDefault(y => y.employeeId == e.ID && y.missionStartDate.Value < DateTime.Now && dbcontext.Extension.First(z => z.engagementId == y.ID).endDate > DateTime.Now);
                                if (engagement != null)
                                {
                                    Extension ext = dbcontext.Extension.FirstOrDefault(x => x.engagementId == engagement.ID);
                                    employeesResponse.Add(new EmployeeResponseModel()
                                    {
                                        name = e.name,
                                        lastName = e.lastName,
                                        engagementStart = engagement.missionStartDate.ToString(),
                                        engagementEnd = ext.endDate.ToString(),
                                        //aggiungere ore lavorate
                                    });
                                }
                            }
                            break;
                        case UserTypeEnum.Owner:
                            Company companyOwner = dbcontext.Company.FirstOrDefault(x => x.ownerId == person.id);
                            employees = dbcontext.Employee.Where(x => x.companyId == companyOwner.ID).ToList();
                            foreach (Employee e in employees)
                            {
                                Engagement engagement = dbcontext.Engagement.FirstOrDefault(y => y.employeeId == e.ID && y.missionStartDate.Value < DateTime.Now && dbcontext.Extension.First(z => z.engagementId == y.ID).endDate > DateTime.Now);
                                if (engagement != null)
                                {
                                    Extension ext = dbcontext.Extension.FirstOrDefault(x => x.engagementId == engagement.ID);
                                    employeesResponse.Add(new EmployeeResponseModel()
                                    {
                                        name = e.name,
                                        lastName = e.lastName,
                                        engagementStart = engagement.missionStartDate.ToString(),
                                        engagementEnd = ext.endDate.ToString(),
                                        //aggiungere ore lavorate
                                    });
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<EmployeeResponseModel>, string>(employeesResponse, exmsg);
        }

        public Tuple<List<Employee>, string> getEmployeesFromIDUnitaDiBusiness(string IDUnitaDiBusiness)
        {
            List<Employee> employees = new List<Employee>();
            string exmsg = string.Empty;
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    bool active = false;
                    List<Engagement> engagements = dbcontext.Engagement.Where(x => x.businessUnitID.Equals(IDUnitaDiBusiness)).ToList();
                    foreach (Engagement eng in engagements)
                    {
                        List<Extension> exts = dbcontext.Extension.Where(x => x.engagementId == eng.ID).ToList();
                        if (exts.Count > 0)
                        {
                            foreach (Extension ext in exts)
                            {
                                if (!active && eng.missionStartDate < DateTime.Now && ext.endDate > DateTime.Now) active = true;
                            }
                        }
                        else
                        {
                            if (!active && eng.missionStartDate < DateTime.Now && eng.missionStopDate > DateTime.Now) active = true;
                        }
                        if (active)
                            employees.Add(getEmployeeByCode(eng.employeeCode).Item1);
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            return new Tuple<List<Employee>, string>(employees, exmsg);
        }

        public Tuple<GetEmployeeDetailResponseModel, string> getEmployeeDetail(long employeeId)
        {
            GetEmployeeDetailResponseModel response = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    Employee employee = dbcontext.Employee.Find(employeeId);
                    List<Engagement> engagements = dbcontext.Engagement.Where(y => y.employeeCode.Equals(employee.employeeCode)).ToList();


                    response = GetEmployeeDetailResponseModel.Create(employee, engagements);
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<GetEmployeeDetailResponseModel, string>(response, exmsg);
        }

        public Tuple<Employee, string> getEmployeeFromEmail(string email)
        {
            Employee employee = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    employee = dbcontext.Employee.FirstOrDefault(x => x.email.Equals(email));
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<Employee, string>(employee, exmsg);
        }

        public string setEmployeeIsActive(EmployeeModel model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Employee e = dbcontext.Employee.FirstOrDefault(x => x.ID == model.id);

                        e.isActive = model.isActive;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string setEmployeeIsActive(long id, bool isActive)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        //string newPass = MyHelper.CreateNewPassword(8);
                        Employee e = dbcontext.Employee.Find(id);
                        //e.password = MyHelper.GetEncryptedPassword(newPass);
                        e.isActive = isActive;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();

                        // EmailSender.sendEmailMichele("Employee abilitato", "Salve, questa è la sua nuova utenza: " + e.email + ", Password: " + newPass);
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public string updateEmployeePhoto(UpdateEmployeePhotoModel model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Media media = new Media()
                        {
                            Employee = dbcontext.Employee.Find(model.employeeId),
                            data = model.image
                        };

                        dbcontext.Media.Add(media);
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }
        #endregion

        #region BRANCH
        public Tuple<List<BranchResponseModel>, string> getBranches(List<string> groups)
        {
            List<BranchResponseModel> branchesModels = null;
            HashSet<UnitaDiBusiness> udbs = getUDBS(groups);
            string exmsg = "";

            if (udbs != null && udbs.Count() > 0)
            {
                branchesModels = new List<BranchResponseModel>();
                foreach (var branch in udbs)
                {
                    MyHelper.Log("CREAZIONE " + branch.UnitaDiBusiness1);
                    branchesModels.Add(BranchResponseModel.Create(branch, getEmployeesFromIDUnitaDiBusiness(branch.IDUnitaDiBusiness).Item1.Count));
                    MyHelper.Log("AGGIUNTO " + branch.UnitaDiBusiness1);
                }
            }
            else exmsg = "Nessun Risultato";
            return new Tuple<List<BranchResponseModel>, string>(branchesModels, exmsg);
        }

        public Tuple<List<BranchResponseModel>, string> getBranches()
        {
            List<BranchResponseModel> branchesModels = null;
            string exmsg = string.Empty;
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<UnitaDiBusiness> branches = dbcontext.UnitaDiBusiness.ToList();

                    if (branches != null && branches.Count() > 0)
                    {
                        branchesModels = new List<BranchResponseModel>();
                        foreach (var branch in branches)
                            branchesModels.Add(BranchResponseModel.Create(branch, getEmployeesFromIDUnitaDiBusiness(branch.IDUnitaDiBusiness).Item1.Count));
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            return new Tuple<List<BranchResponseModel>, string>(branchesModels, exmsg);
        }

        public Tuple<UnitaDiBusiness, string> getBranch(long branchID)
        {
            string exmsg = string.Empty;
            UnitaDiBusiness branch = null;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    branch = dbcontext.UnitaDiBusiness.Find(branchID);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            return new Tuple<UnitaDiBusiness, string>(branch, exmsg);
        }

        public Tuple<GetBranchDetailResponseModel, string> getBranchDetail(long branchId)
        {
            GetBranchDetailResponseModel response = null;
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    UnitaDiBusiness branch = dbcontext.UnitaDiBusiness.FirstOrDefault(x => x.ID == branchId);
                    Address address = dbcontext.Address.FirstOrDefault(y => y.ID == branch.IDAddress);
                    Tuple<List<Employee>, string> branchEmployeesResult = getEmployeesFromIDUnitaDiBusiness(branch.IDUnitaDiBusiness);

                    if (branch != null && branchEmployeesResult.Item1 != null)
                    {
                        response = GetBranchDetailResponseModel.Create(branch, address, branchEmployeesResult.Item1);
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }

            return new Tuple<GetBranchDetailResponseModel, string>(response, exmsg);
        }

        //public string updateBranch(BranchModel model)
        //{
        //    string exmsg = string.Empty;

        //    using (var dbcontext = GetDBContext())
        //    {
        //        using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                Branch branch = dbcontext.Branch.First(x => x.ID == model.id);

        //                branch.name = model.name;

        //                /* customer address */
        //                dbcontext.Entry(branch).Reference(x => x.Address).Load();
        //                branch.Address.street = model.address.Street;
        //                branch.Address.city = model.address.City;
        //                branch.Address.zip = model.address.Zip;
        //                branch.Address.state = model.address.State;
        //                branch.Address.country = model.address.Country;

        //                dbcontext.SaveChanges();
        //                dbContextTransaction.Commit();
        //            }
        //            catch (Exception e)
        //            {
        //                exmsg = e.StackTrace + ":" + e.InnerException;
        //                dbContextTransaction.Rollback();
        //            }
        //        }
        //    }

        //    return exmsg;
        //}

        #endregion

        #region BUSINESSUNIT

        public Tuple<long, string> insertBusinessUnit(UnitaDiBusiness unitaDiBusiness)
        {
            string exmsg = null;
            long id = 0;
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        UnitaDiBusiness tempudb = dbcontext.UnitaDiBusiness.Where(x => x.IDUnitaDiBusiness.Equals(unitaDiBusiness.IDUnitaDiBusiness)).FirstOrDefault();
                        if (tempudb == null)
                            tempudb = dbcontext.UnitaDiBusiness.Add(unitaDiBusiness);
                        dbcontext.SaveChanges();

                        dbContextTransaction.Commit();
                        id = tempudb.ID;
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<long, string>(id, exmsg);
        }

        #endregion

        #region MESSAGE
        public Tuple<long, string> insertMessage(MessageModel model, UserTypeModel userType)
        {
            string exmsg = string.Empty;
            long messageId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Message m = new Message()
                        {
                            timeStamp = DateTime.Now,
                            body = model.body,
                            idFrom = userType.id
                        };
                        if (userType.type == UserTypeEnum.Employee)
                        {
                            m.sentByEmployee = true;
                            m.employeeId = userType.id;
                        }
                        else
                        {
                            m.sentByEmployee = false;
                            m.employeeId = (long)model.idTo;
                            m.idTo = (long)model.idTo;
                        }
                        Message addedMessage = dbcontext.Message.Add(m);

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();

                        messageId = addedMessage.ID;
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(messageId, exmsg);
        }

        public Tuple<List<Message>, string> getMessagesByEmployeeId(long id)
        {
            List<Message> messages = null;
            string exmsg = string.Empty;
            try
            {
                messages = GetDBContext().Message.Where(x => x.employeeId == id).ToList();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Message>, string>(messages, exmsg);
        }

        public Tuple<int, string> getCountNewMessages(UserTypeEnum userType, long id)
        {
            string exmsg = string.Empty;
            int count = -1;
            switch (userType)
            {
                case UserTypeEnum.Operator:
                    count = GetDBContext().Message.Where(x => x.idFrom == id && x.hasBeenRead == false).Count();
                    break;
                case UserTypeEnum.Employee:
                    count = GetDBContext().Message.Where(x => x.idTo == id && x.hasBeenRead == false).Count();
                    break;
                default:
                    exmsg = "errore nella richiesta";
                    break;
            }
            return new Tuple<int, string>(count, exmsg);
        }

        public CountsModel getOperatorEmployeesCountMessages()
        {
            List<long> ids = GetDBContext().Employee.Select(x => x.ID).ToList();
            List<CountModel> counts = new List<CountModel>();
            foreach (long id in ids)
            {
                counts.Add(CountModel.Create(id, getCountNewMessages(UserTypeEnum.Operator, id).Item1));
            }
            return new CountsModel() { counts = counts };
        }

        public string setMessageRead(Message message)
        {
            string exmsg = string.Empty;
            using (var dbContext = GetDBContext())
            {
                using (var dbContextTransaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        Message mod = dbContext.Message.Find(message.ID);
                        mod.hasBeenRead = true;

                        dbContext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return exmsg;
        }
        #endregion

        #region COMPANY_MESSAGE
        public Tuple<long, string> insertCompanyMessage(CompanyMessageModel model, UserTypeModel userType)
        {
            string exmsg = string.Empty;
            long messageId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        CompanyMessage m = new CompanyMessage()
                        {
                            timeStamp = DateTime.Now,
                            body = model.body,
                            idFrom = userType.secondaryId
                        };
                        if (userType.type == UserTypeEnum.Customer || userType.type == UserTypeEnum.Owner)
                        {
                            m.sentByCompany = true;
                            m.companyId = userType.secondaryId;
                        }
                        else
                        {
                            m.sentByCompany = false;
                            m.companyId = (long)model.idTo;
                            m.idTo = (long)model.idTo;
                        }
                        CompanyMessage addedMessage = dbcontext.CompanyMessage.Add(m);

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();

                        messageId = addedMessage.ID;
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(messageId, exmsg);
        }

        public Tuple<List<CompanyMessage>, string> getMessagesByCompanyId(long id)
        {
            List<CompanyMessage> messages = null;
            string exmsg = string.Empty;
            try
            {
                messages = GetDBContext().CompanyMessage.Where(x => x.companyId == id).ToList();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<CompanyMessage>, string>(messages, exmsg);
        }

        public Tuple<int, string> getCompanyNewMessagesCount(UserTypeEnum userType, long id)
        {
            string exmsg = string.Empty;
            int count = -1;
            switch (userType)
            {
                case UserTypeEnum.Operator:
                    count = GetDBContext().CompanyMessage.Where(x => x.idFrom == id && x.hasBeenRead == false).Count();
                    break;
                case UserTypeEnum.Owner:
                    count = GetDBContext().CompanyMessage.Where(x => x.idTo == id && x.hasBeenRead == false).Count();
                    break;
                case UserTypeEnum.Customer:
                    count = GetDBContext().CompanyMessage.Where(x => x.idTo == id && x.hasBeenRead == false).Count();
                    break;
                default:
                    exmsg = "errore nella richiesta";
                    break;
            }
            return new Tuple<int, string>(count, exmsg);
        }

        public CountsModel getOperatorCompaniesCountMessages()
        {
            List<long> ids = GetDBContext().Company.Select(x => x.ID).ToList();
            List<CountModel> counts = new List<CountModel>();
            foreach (long id in ids)
            {
                counts.Add(CountModel.Create(id, getCompanyNewMessagesCount(UserTypeEnum.Operator, id).Item1));
            }
            return new CountsModel() { counts = counts };
        }

        public string setCompanyMessageRead(CompanyMessage message)
        {
            string exmsg = string.Empty;
            using (var dbContext = GetDBContext())
            {
                using (var dbContextTransaction = dbContext.Database.BeginTransaction())
                {
                    try
                    {
                        CompanyMessage mod = dbContext.CompanyMessage.Find(message.ID);
                        mod.hasBeenRead = true;

                        dbContext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return exmsg;
        }
        #endregion

        #region SERVICE
        public Tuple<long, string> insertService()
        {
            string exmsg = string.Empty;
            long serviceId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Service addedService = dbcontext.Service.Add(new Service());

                        dbcontext.SaveChanges();

                        serviceId = addedService.ID;

                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(serviceId, exmsg);
        }

        public Tuple<long, string> insertService(bool documenti, bool rapportini, bool firma)
        {
            string exmsg = string.Empty;
            long serviceId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Service addedService = dbcontext.Service.Add(new Service()
                        {
                            documenti = documenti,
                            firma = firma,
                            rapportini = rapportini
                        });

                        dbcontext.SaveChanges();

                        serviceId = addedService.ID;

                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(serviceId, exmsg);
        }

        public Tuple<Service, string> getServiceByCustomerId(long id)
        {
            string exmsg = string.Empty;
            Service service = null;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    long servId = (long)dbcontext.Customer.Find(id).serviceId;
                    service = dbcontext.Service.Find(servId);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            return new Tuple<Service, string>(service, exmsg);
        }

        public Tuple<Service, string> getService(long id)
        {
            string exmsg = string.Empty;
            Service service = null;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    service = dbcontext.Service.Find(id);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            return new Tuple<Service, string>(service, exmsg);
        }

        public string updateServiceStatus(ServiceModel model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Service service = dbcontext.Service.Find(model.id);

                        service.documenti = model.documenti;
                        service.rapportini = model.rapportini;
                        service.firma = model.firma;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }



        public string updateServiceStatusMod(ServiceModel model, long id)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Service service = dbcontext.Service.Find(id);

                        if (!model.documenti)
                            service.documenti = model.documenti;
                        if (!model.rapportini)
                            service.rapportini = model.rapportini;
                        if (!model.firma)
                            service.firma = model.firma;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }
        #endregion

        #region DOCUMENT
        public Tuple<long, string> insertDocument(DocumentModel model, bool isEasy = false)
        {
            string exmsg = string.Empty;
            long id = -1;
            Document d = null;
            MemoryStream target = new MemoryStream();
            model.data.InputStream.CopyTo(target);
            byte[] data = target.ToArray();
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        if (model.employeeId != null && model.employeeId > 0)
                        {
                            Employee emp = dbcontext.Employee.Find(model.employeeId);
                            d = new Document()
                            {
                                nome = model.data.FileName,
                                documento = data,
                                employeeId = model.employeeId,
                                category = model.category
                            };
                            EmailSender.sendEmail(emp.email, isEasy ? "Documenti EasyBox" : "Documenti AgriBox", "Hai un nuovo documento disponibile nella sezione Categorizzati", isEasy);
                        }
                        else
                        {
                            Company comp = getCompanyById((long)model.companyId).Item1;
                            List<Customer> customers = getCompanyCustomers(comp.ID).Item1;
                            Customer owner = getOwner((long)comp.ownerId).Item1;
                            d = new Document()
                            {
                                nome = model.fileName,
                                documento = data,
                                companyId = model.companyId,
                                category = model.category
                            };
                            foreach (Customer c in customers)
                            {
                                Service s = dbcontext.Service.Find(c.serviceId);
                                if (s.documenti == true)
                                {
                                    EmailSender.sendEmail(c.email, isEasy ? "Documenti EasyBox" : "Documenti AgriBox", "Hai un nuovo documento disponibile nella sezione Categorizzati", isEasy);
                                }
                            }
                            EmailSender.sendEmail(owner.email, isEasy ? "Documenti EasyBox" : "Documenti AgriBox", "Hai un nuovo documento disponibile nella sezione Categorizzati", isEasy);
                        }

                        Document addedDocument = dbcontext.Document.Add(d);

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();

                        id = addedDocument.ID;
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
                return new Tuple<long, string>(id, exmsg);
            }

        }

        public Tuple<string, string> deleteDocument(DeleteDocumentModel model)
        {
            string exmsg = string.Empty;
            string response = string.Empty;
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Document document = dbcontext.Document.Find(model.id);
                        dbcontext.Document.Remove(document);
                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                        response = "OK";
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<string, string>(response, exmsg);
        }

        public Tuple<List<DocumentModel>, string> getCompanyDocuments(long id)
        {
            List<DocumentModel> documentsList = new List<DocumentModel>();
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Document> documents = dbcontext.Document.Where(x => x.companyId == id).ToList();

                    foreach (Document d in documents)
                    {
                        documentsList.Add(DocumentModel.Create(d));
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }

            return new Tuple<List<DocumentModel>, string>(documentsList, exmsg);
        }

        public Tuple<Document, string> getDocument(long id)
        {
            string exmsg = string.Empty;
            Document doc = null;
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        doc = dbcontext.Document.Find(id);
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<Document, string>(doc, exmsg);
        }

        public Tuple<List<string>, string> getDocumentCategories(long companyId, long employeeId)
        {
            string exmsg = string.Empty;
            List<string> categories = null;

            try
            {
                if (companyId > 0)
                {
                    categories = GetDBContext().Document.Where(x => x.companyId == companyId).Select(x => x.category).Distinct().ToList();
                }
                else
                {
                    Employee emp = getEmployee(employeeId).Item1;
                    if (emp != null)
                    {
                        Company company = getCompanyByEmployeeCode(emp.employeeCode).Item1;
                        if (company != null)
                            categories = GetDBContext().Document.Where(x => x.employeeId == emp.ID).Select(x => x.category).Distinct().ToList();
                    }
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            if (categories == null || categories.Count == 0) categories = new List<string> { "Generico" };
            else if (!categories.Contains("Generico"))
                categories.Add("Generico");

            return new Tuple<List<string>, string>(categories, exmsg);
        }

        public Tuple<List<DocumentModel>, string> getEmployeeDocuments(long id)
        {
            List<DocumentModel> documentsList = new List<DocumentModel>();
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                try
                {
                    List<Document> documents = dbcontext.Document.Where(x => x.employeeId == id).ToList();

                    foreach (Document d in documents)
                    {
                        documentsList.Add(DocumentModel.Create(d));
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.StackTrace + ":" + e.InnerException;
                }
            }
            return new Tuple<List<DocumentModel>, string>(documentsList, exmsg);
        }
        #endregion

        #region ARCHIVE_DOCUMENT

        public void insertDocumentInfo(List<ArchiveDocuments> docInfoList)
        {
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (ArchiveDocuments doc in docInfoList)
                        {
                            try
                            {
                                dbcontext.ArchiveDocuments.Add(doc);
                            }
                            catch (Exception ex)
                            {
                                MyHelper.Log(ex.Message);
                            }
                        }
                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        dbContextTransaction.Rollback();
                        MyHelper.Log(e.ToString());
                    }
                }
            }
        }


        public Tuple<List<ArchiveDocuments>, string> getArchiveDocumentsEmployee(long id)
        {
            List<ArchiveDocuments> documents = new List<ArchiveDocuments>();
            string exmsg = string.Empty;

            Employee e = getEmployee(id).Item1;

            using (var dbContext = GetDBContext())
            {
                documents.AddRange(dbContext.ArchiveDocuments.Where(x => x.num_matricola.Equals(e.employeeCode)).ToList());
            }

            return new Tuple<List<ArchiveDocuments>, string>(documents, exmsg);
        }

        public ArchiveDocuments findArchiveDocument(long id)
        {
            if (id > 0)
            {
                using (var dbcontext = GetDBContext())
                {
                    return dbcontext.ArchiveDocuments.Find(id);
                }
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region MEDIA
        public Tuple<long, string> insertMedia(MediaModel model, IdentityModel identity)
        {
            string exmsg = string.Empty;
            long mediaId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Media s = new Media()
                        {
                            name = model.name,
                            data = model.data,

                        };

                        if (identity.CompanyId != 0)
                            s.Company = dbcontext.Company.Find(identity.CompanyId);
                        else
                            s.Employee = dbcontext.Employee.Find(identity.EmployeeId);

                        Media addedMedia = dbcontext.Media.Add(s);

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();

                        mediaId = addedMedia.ID;
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(mediaId, exmsg);
        }

        public Tuple<Media, string> getMediaByCompanyId(long id)
        {
            Media media = null;
            string exmsg = string.Empty;
            try
            {
                media = GetDBContext().Media.FirstOrDefault(x => x.companyId == id);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Media, string>(media, exmsg);
        }

        public Tuple<Media, string> getMediaByEmployeeId(long id)
        {
            Media media = null;
            string exmsg = string.Empty;
            try
            {
                media = GetDBContext().Media.FirstOrDefault(x => x.employeeId == id);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Media, string>(media, exmsg);
        }

        #endregion

        #region UTILITIES

        public Tuple<long, string> insertAddress(Address address)
        {
            string exmsg = string.Empty;
            long addressId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Address taddress = dbcontext.Address.Where(x => x.city.Equals(address.city) && x.street.Equals(address.street)).FirstOrDefault();
                        if (taddress == null)
                        {
                            Address tempAddress = dbcontext.Address.Add(address);

                            dbcontext.SaveChanges();
                            dbContextTransaction.Commit();
                            addressId = tempAddress.ID;
                        }
                        else addressId = taddress.ID;
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(addressId, exmsg);
        }

        private Tuple<List<string>, List<string>, List<string>> getFinalGroups(List<string> groups)
        {
            List<string> subBranches = new List<string>();
            List<string> branches = new List<string>();
            List<string> divisions = new List<string>();
            foreach (string s in groups)
            {
                var branch = GetDBContext().UnitaDiBusiness.Where(x => x.IDUnitaDiBusiness.Equals(s, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                if (branch != null)
                {
                    if (branch.Divisione.Equals("VARIE", StringComparison.InvariantCultureIgnoreCase))
                    {
                        MyHelper.Log(s);
                        //AGGIUNGO FILIALE es. RM01
                        branches.Add(s);
                    }
                    else
                    {
                        MyHelper.Log(s);
                        //AGGIUNGO SOTTOFILIALE es. ICTRM01
                        subBranches.Add(s);
                    }
                }
                else
                {
                    branch = GetDBContext().UnitaDiBusiness.Where(y => y.Divisione.Equals(s.Replace("EB", ""), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                    if (branch != null)
                    {
                        MyHelper.Log(s);
                        //AGGIUNGO DIVISIONE es. ICT || Elimino la parte iniziale 'EB'
                        divisions.Add(s.Replace("EB", ""));
                    }
                }
            }
            return new Tuple<List<string>, List<string>, List<string>>(branches, divisions, subBranches);
        }

        private HashSet<UnitaDiBusiness> getUDBS(List<string> groups)
        {
            string exmsg = string.Empty;
            var finalGroups = getFinalGroups(groups);
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    HashSet<UnitaDiBusiness> branches = new HashSet<UnitaDiBusiness>();

                    foreach (string s in finalGroups.Item1)
                    {

                        UnitaDiBusiness udb = dbcontext.UnitaDiBusiness.Where(y => y.IDUnitaDiBusiness.Equals(s, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                        if (udb != null)
                        {
                            List<UnitaDiBusiness> udbs = dbcontext.UnitaDiBusiness.Where(x => x.IDUnitaDiBusiness.Contains(s) && x.CodiceINAIL.Equals(udb.CodiceINAIL, StringComparison.InvariantCultureIgnoreCase)).ToList();
                            if (udbs.Count > 0)
                            {
                                MyHelper.Log("FILIALI : " + udbs.Count);
                                branches.UnionWith(udbs);
                            }
                        }
                    }

                    foreach (string s in finalGroups.Item2)
                    {
                        List<UnitaDiBusiness> udbs = dbcontext.UnitaDiBusiness.Where(x => x.Divisione.Equals(s, StringComparison.InvariantCultureIgnoreCase)).ToList();
                        if (udbs.Count > 0)
                        {
                            MyHelper.Log("DIVISIONI : " + udbs.Count);
                            branches.UnionWith(udbs);
                        }
                    }

                    foreach (string s in finalGroups.Item3)
                    {
                        List<UnitaDiBusiness> udbs = dbcontext.UnitaDiBusiness.Where(x => x.IDUnitaDiBusiness.Equals(s, StringComparison.InvariantCultureIgnoreCase)).ToList();
                        if (udbs.Count > 0)
                        {
                            MyHelper.Log("SOTTOFILIALI : " + udbs.Count);
                            branches.UnionWith(udbs);
                        }
                    }

                    return branches;
                }
                catch
                {
                    return new HashSet<UnitaDiBusiness>();
                }
            }
        }
        #endregion

        #region ADMINISTRATION
        public Tuple<long, string> insertAdministration(AdministrationModel administration)
        {
            string exmsg = string.Empty;
            long administrationId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Administration tempadmin = dbcontext.Administration.Where(x => x.administrationCode.Equals(administration.administrationCode)).FirstOrDefault();
                        if (tempadmin == null)
                        {
                            Administration admin = new Administration()
                            {
                                administrationCode = administration.administrationCode,
                                administrationDate = administration.administrationDate,
                                businessNameCustomer = administration.businessNameCustomer,
                                customerCode = administration.customerCode,
                                fullTime = administration.fullTime,
                                fullTimeHoursNumber = administration.fullTimeHoursNumber,
                                headquarter = administration.headquarter,
                                partTimeHoursNumber = administration.partTimeHoursNumber,
                                partTimePercent = administration.partTimePercent,
                                partTimeType = administration.partTimeType.ToString(),
                                provisionStartDate = administration.provisionStartDate,
                                provisionEndDate = administration.provisionEndDate,
                                task = administration.task,
                                taskDetail = administration.taskDetail,
                                contractLevel = administration.contractLevel,
                                qualification = administration.qualification,
                                businessUnitID = administration.businessUnitID
                            };

                            Administration tempadm = dbcontext.Administration.Add(admin);

                            dbcontext.SaveChanges();
                            administrationId = tempadm.ID;

                            if (administration.Extensions != null)
                            {
                                foreach (ExtensionModel ext in administration.Extensions.extensions)
                                {
                                    Extension tempext = dbcontext.Extension.Where(x => x.extensionNumber == ext.extensionNumber).FirstOrDefault();
                                    if (tempext == null)
                                    {
                                        dbcontext.Extension.Add(new Extension()
                                        {
                                            administrationId = administrationId,
                                            startDate = ext.startDate,
                                            endDate = ext.endDate,
                                            extensionDate = ext.extensionDate,
                                            extensionNumber = ext.extensionNumber
                                        });
                                        dbcontext.SaveChanges();
                                    }
                                    else
                                    {
                                        Extension extension = dbcontext.Extension.Find(tempext.ID);
                                        extension.startDate = ext.startDate;
                                        extension.endDate = ext.endDate;

                                        dbcontext.SaveChanges();
                                    }
                                }
                            }
                        }
                        else
                        {
                            Administration admin = dbcontext.Administration.Find(tempadmin.ID);
                            admin.customerCode = administration.customerCode;
                            admin.fullTime = administration.fullTime;
                            admin.headquarter = administration.headquarter;
                            admin.partTimeHoursNumber = administration.partTimeHoursNumber;
                            admin.partTimePercent = administration.partTimePercent;
                            admin.partTimeType = administration.partTimeType.ToString();
                            admin.task = administration.task;
                            admin.taskDetail = administration.taskDetail;

                            dbcontext.SaveChanges();
                            administrationId = admin.ID;

                            if (administration.Extensions != null)
                            {
                                foreach (ExtensionModel ext in administration.Extensions.extensions)
                                {
                                    Extension tempext = dbcontext.Extension.Where(x => x.extensionNumber == ext.extensionNumber).First();
                                    if (tempext == null)
                                    {
                                        dbcontext.Extension.Add(new Extension()
                                        {
                                            administrationId = administrationId,
                                            startDate = ext.startDate,
                                            endDate = ext.endDate,
                                            extensionDate = ext.extensionDate,
                                            extensionNumber = ext.extensionNumber
                                        });
                                        dbcontext.SaveChanges();
                                    }
                                    else
                                    {
                                        Extension extension = dbcontext.Extension.Find(tempext.ID);
                                        extension.startDate = ext.startDate;
                                        extension.endDate = ext.endDate;

                                        dbcontext.SaveChanges();
                                    }
                                }
                            }
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<long, string>(administrationId, exmsg);
        }

        public string insertAdministration(Somministrazione model)
        {
            string exmsg = string.Empty;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        bool fullTime = false;
                        if (model.FullTime.Equals("1"))
                            fullTime = true;
                        Administration cc = new Administration()
                        {
                            administrationCode = model.CodiceSomministrazione,
                            businessNameCustomer = model.RagioneSocialeCliente,
                            customerCode = model.CodiceRiferimentoCliente,
                            administrationDate = model.DataSomministrazione,
                            fullTime = fullTime,
                            partTimeHoursNumber = model.NumeroOrePartTime,
                            partTimePercent = model.PercentualePartTime,
                            partTimeType = model.TipoPartTime.ToString(),
                            provisionEndDate = model.DataFineFornitura,
                            provisionStartDate = model.DataInizioFornitura,
                            task = model.Mansione,
                            taskDetail = model.DettaglioMansione
                        };
                        if (model.NumeroOreFullTime != null)
                            cc.fullTimeHoursNumber = model.NumeroOreFullTime;
                        dbcontext.Administration.Add(cc);

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return exmsg;
        }

        public Tuple<Administration, string> getAdministrationByAdministrationCode(string code)
        {
            Administration administration = null;
            string exmsg = string.Empty;
            try
            {
                administration = GetDBContext().Administration.Where(x => x.administrationCode.Equals(code)).FirstOrDefault();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Administration, string>(administration, exmsg);
        }

        public Tuple<Administration, string> getAdministrationByCompanyID(long id)
        {
            Company company = GetDBContext().Company.Find(id);
            Administration administration = null;
            string exmsg = string.Empty;
            try
            {
                administration = GetDBContext().Administration.Where(x => x.customerCode == company.companyCode).OrderByDescending(y => y.administrationDate).FirstOrDefault();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Administration, string>(administration, exmsg);
        }

        public Tuple<Administration, string> getAdministrationByCompany(string code)
        {
            Administration administration = null;
            string exmsg = string.Empty;
            try
            {
                administration = GetDBContext().Administration.Where(x => x.customerCode == code).OrderByDescending(y => y.administrationDate).FirstOrDefault();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Administration, string>(administration, exmsg);
        }

        public Tuple<List<Administration>, string> getAdministrationsByCompany(string code)
        {
            List<Administration> administration = null;
            string exmsg = string.Empty;
            try
            {
                administration = GetDBContext().Administration.Where(x => x.customerCode == code).OrderByDescending(y => y.administrationDate).ToList();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Administration>, string>(administration, exmsg);
        }
        #endregion

        #region HEADQUARTER
        public Tuple<long, string> insertHeadQuarter(SedeCliente sede)
        {
            string exmsg = string.Empty;
            long headQuarterId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Headquarter temphq = dbcontext.Headquarter.Where(x => x.city.Equals(sede.Comune) && x.street.Equals(sede.Via)).FirstOrDefault();
                        if (temphq == null)
                        {

                            Headquarter headQuarter = new Headquarter()
                            {
                                city = sede.Comune,
                                street = sede.Via,
                                zip = sede.CAP,
                                district = sede.SiglaProvincia,
                                state = sede.Nazione,
                                stateCode = sede.CodiceNazione,
                                cityCode = sede.CodiceComune
                            };

                            Headquarter resulthq = dbcontext.Headquarter.Add(headQuarter);

                            dbcontext.SaveChanges();
                            headQuarterId = resulthq.ID;
                        }
                        else
                        {
                            Headquarter headQuarter = dbcontext.Headquarter.Find(temphq.ID);
                            headQuarter.street = sede.Via;
                            headQuarter.zip = sede.CAP;
                            headQuarter.city = sede.Comune;
                            headQuarter.district = sede.SiglaProvincia;
                            headQuarter.state = sede.Nazione;
                            headQuarter.stateCode = sede.CodiceNazione;
                            headQuarter.cityCode = sede.CodiceComune;

                            dbcontext.SaveChanges();
                            headQuarterId = headQuarter.ID;
                        }
                        dbContextTransaction.Commit();

                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }

            return new Tuple<long, string>(headQuarterId, exmsg);
        }
        #endregion

        #region ADDRESS
        public Tuple<Address, string> getAddressByID(long? id)
        {
            Address address = null;
            string exmsg = string.Empty;
            try
            {
                address = GetDBContext().Address.Find(id);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Address, string>(address, exmsg);
        }
        #endregion

        #region ENGAGEMENT
        public Tuple<long, string> insertEngagement(EngagementModel eng)
        {
            string exmsg = string.Empty;
            long engagementId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        Engagement tempeng = dbcontext.Engagement.Where(x => x.engagementNumber.Equals(eng.engagementNumber)).FirstOrDefault();
                        if (tempeng == null)
                        {
                            Engagement engagement = new Engagement()
                            {
                                administrationNumber = eng.administrationNumber,
                                businessUnitID = eng.businessUnitID,
                                employeeCode = eng.employeeCode,
                                employeeFiscalCode = eng.employeeFiscalCode,
                                engagementDate = eng.engagementDate,
                                engagementNumber = eng.engagementNumber,
                                matriculationNumber = eng.matriculationNumber,
                                missionStartDate = eng.missionStartDate,
                                missionStopDate = eng.missionStopDate
                            };

                            Engagement tempEng = dbcontext.Engagement.Add(engagement);
                            dbcontext.SaveChanges();

                            if (eng.Extension != null)
                                foreach (ExtensionModel ext in eng.Extension)
                                {
                                    dbcontext.Extension.Add(new Extension()
                                    {
                                        engagementId = tempEng.ID,
                                        startDate = ext.startDate,
                                        endDate = ext.endDate,
                                        extensionDate = ext.extensionDate,
                                        extensionNumber = ext.extensionNumber
                                    });
                                }
                            dbcontext.SaveChanges();
                            engagementId = tempEng.ID;
                        }
                        else
                        {
                            Engagement engm = dbcontext.Engagement.Find(tempeng.ID);
                            if (eng.Extension != null)
                                foreach (ExtensionModel ext in eng.Extension)
                                {
                                    Extension tempext = dbcontext.Extension.Where(x => x.extensionNumber.Equals(ext.extensionNumber)).FirstOrDefault();
                                    if (tempext == null)
                                        dbcontext.Extension.Add(new Extension()
                                        {
                                            engagementId = engm.ID,
                                            startDate = ext.startDate,
                                            endDate = ext.endDate,
                                            extensionDate = ext.extensionDate,
                                            extensionNumber = ext.extensionNumber
                                        });
                                }
                            engm.employeeFiscalCode = eng.employeeFiscalCode;
                            engm.businessUnitID = eng.businessUnitID;
                            engm.matriculationNumber = eng.matriculationNumber;

                            dbcontext.SaveChanges();
                            engagementId = engm.ID;
                        }
                        dbContextTransaction.Commit();

                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<long, string>(engagementId, exmsg);
        }

        public Tuple<Engagement, string> getEngagementByNumber(string number)
        {
            Engagement engagement = null;
            string exmsg = string.Empty;
            try
            {
                engagement = GetDBContext().Engagement.Where(x => x.engagementNumber.Equals(number)).FirstOrDefault();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Engagement, string>(engagement, exmsg);
        }

        public Tuple<Engagement, string> getEngagementByEmployeeID(long id)
        {
            Engagement engagement = null;
            Employee emp = getEmployee(id).Item1;
            string exmsg = string.Empty;
            try
            {
                engagement = GetDBContext().Engagement.Where(x => x.employeeCode == emp.employeeCode).OrderByDescending(y => y.missionStartDate).FirstOrDefault();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Engagement, string>(engagement, exmsg);
        }

        public Tuple<List<Engagement>, string> getEngagementsByEmployeeID(long id)
        {
            List<Engagement> engagement = null;
            Employee emp = getEmployee(id).Item1;
            string exmsg = string.Empty;
            try
            {
                engagement = GetDBContext().Engagement.Where(x => x.employeeCode == emp.employeeCode).OrderByDescending(y => y.missionStartDate).ToList();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Engagement>, string>(engagement, exmsg);
        }

        public Tuple<Engagement, Extension> getCurrentEngagementWithExtensionByEmployeeID(long id)
        {
            Engagement engagement = null;
            Extension extension = null;
            Employee emp = getEmployee(id).Item1;
            string exmsg = string.Empty;
            try
            {
                List<Engagement> engagements = GetDBContext().Engagement.Where(x => x.employeeCode == emp.employeeCode).ToList();
                engagement = engagements.Where(eng => eng.missionStartDate == (engagements.Max(en => en.missionStartDate))).FirstOrDefault();
                while (engagement.missionStartDate > DateTime.Now)
                {
                    engagements.Remove(engagement);
                    engagement = engagements.Where(eng => eng.missionStartDate == (engagements.Max(en => en.missionStartDate))).FirstOrDefault();
                }
                List<Extension> extensions = GetDBContext().Extension.Where(y => y.engagementId == engagement.ID).ToList();
                Extension currentExt = extensions.Where(ext => ext.endDate == (extensions.Max(ex => ex.endDate))).FirstOrDefault();
                if (currentExt.endDate > DateTime.Now) extension = currentExt;
            }
            catch (Exception)
            {
            }
            return new Tuple<Engagement, Extension>(engagement, extension);
        }

        public Tuple<List<Engagement>, string> getEngagementsByCompanyId(long id)
        {
            Company company = null;
            Administration admin = null;
            List<Engagement> engagements = null;
            string exmsg = string.Empty;
            try
            {
                using (var dbContext = GetDBContext())
                {
                    company = dbContext.Company.Find(id);
                    admin = GetDBContext().Administration.Where(x => x.customerCode == company.companyCode).FirstOrDefault();
                    engagements = GetDBContext().Engagement.Where(x => x.administrationNumber == admin.administrationCode).ToList();
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Engagement>, string>(engagements, exmsg);
        }
        #endregion

        #region EXTENSION
        public Tuple<List<Extension>, string> getExtensionsByEngagementID(long id)
        {
            List<Extension> extensions = null;
            string exmsg = string.Empty;
            try
            {
                extensions = GetDBContext().Extension.Where(x => x.engagementId == id).ToList();//.OrderByDescending(y => y.extensionDate);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Extension>, string>(extensions, exmsg);
        }

        public Tuple<Extension, string> getExtensionByNumber(string number)
        {
            Extension extension = null;
            string exmsg = string.Empty;
            try
            {
                extension = GetDBContext().Extension.Where(x => x.extensionNumber.Equals(number)).FirstOrDefault();//.OrderByDescending(y => y.extensionDate);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Extension, string>(extension, exmsg);
        }

        public Tuple<List<Extension>, string> getExtensionsByAdministration(long id)
        {
            List<Extension> extensions = null;
            string exmsg = string.Empty;
            try
            {
                extensions = GetDBContext().Extension.Where(x => x.administrationId == id).ToList();//.OrderByDescending(y => y.extensionDate);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Extension>, string>(extensions, exmsg);
        }
        #endregion

        #region FIRMA ELETTRONICA AVANZATA
        public Tuple<long, string> insertFEA(FirmaElettronicaAvanzata firmaElettronicaAvanzata)
        {
            string exmsg = string.Empty;
            long feaId = -1;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        FirmaElettronicaAvanzata tempfea = null;
                        if (firmaElettronicaAvanzata.email != null)
                            tempfea = dbcontext.FirmaElettronicaAvanzata.Where(x => x.email.Equals(firmaElettronicaAvanzata.email)).FirstOrDefault();
                        if (tempfea == null)
                        {
                            firmaElettronicaAvanzata = dbcontext.FirmaElettronicaAvanzata.Add(firmaElettronicaAvanzata);

                            dbcontext.SaveChanges();
                            feaId = firmaElettronicaAvanzata.ID;
                        }
                        else
                        {
                            FirmaElettronicaAvanzata feaf = dbcontext.FirmaElettronicaAvanzata.Find(tempfea.ID);
                            feaf.cellulare = firmaElettronicaAvanzata.cellulare;

                            dbcontext.SaveChanges();
                            feaId = feaf.ID;
                        }
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.StackTrace + ":" + e.InnerException;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<long, string>(feaId, exmsg);
        }

        public Tuple<FirmaElettronicaAvanzata, string> getFEA(long id)
        {
            FirmaElettronicaAvanzata fea = null;
            string exmsg = string.Empty;
            try
            {
                fea = GetDBContext().FirmaElettronicaAvanzata.Find(id);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
                throw;
            }
            return new Tuple<FirmaElettronicaAvanzata, string>(fea, exmsg);
        }
        #endregion

        #region BBSYNC
        public Tuple<List<Employee>, string> getNonSyncEmployees()
        {
            string exmsg = string.Empty;
            List<Employee> nonSyncEmployees = null;
            try
            {
                nonSyncEmployees = GetDBContext().Employee.Where(x => x.isBBSync == false).ToList();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Employee>, string>(nonSyncEmployees, exmsg);
        }

        public Tuple<List<Customer>, string> getOwners()
        {
            string exmsg = string.Empty;
            List<Customer> owners = new List<Customer>();
            List<long?> ownersId = GetDBContext().Company.Where(y => y.ownerId != null && y.ownerId != 0).Select(x => x.ownerId).ToList();
            try
            {
                foreach (long l in ownersId)
                {
                    owners.Add(GetDBContext().Customer.Where(x => x.ID == l).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<Customer>, string>(owners, exmsg);
        }

        public Tuple<Tuple<List<Customer>, List<Company>>, string> getNonSyncOwners()
        {
            string exmsg = string.Empty;
            List<Customer> nonSyncOwners = new List<Customer>();
            List<Company> companies = GetDBContext().Company.Where(y => y.ownerId != null && y.ownerId != 0).ToList();
            try
            {
                foreach (Company c in companies)
                {
                    nonSyncOwners.Add(GetDBContext().Customer.Where(x => x.ID == c.ownerId && x.isBBSync == false).FirstOrDefault());
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<Tuple<List<Customer>, List<Company>>, string>(new Tuple<List<Customer>, List<Company>>(nonSyncOwners, companies), exmsg);
        }
        #endregion

        #region CHART
        public Tuple<ChartResponse, string> getEngagedEmployees(int year, long companyId)
        {
            string exmsg = string.Empty;
            ChartResponse chart = new ChartResponse() { chart = new List<Tuple<ChartEnum, int>>() };
            List<Employee> employees = null;
            List<Engagement> companyEngagements = new List<Engagement>();
            try
            {
                Administration admin = getAdministrationByCompanyID(companyId).Item1;
                if (admin != null)
                {
                    using (var dbContext = GetDBContext())
                    {
                        for (int month = 1; month < 13; month++)
                        {
                            employees = new List<Employee>();
                            DateTime date = new DateTime(year, month, 2);
                            companyEngagements = dbContext.Engagement.Where(x => x.administrationNumber == admin.administrationCode && x.missionStartDate < date && (x.Extension.Max(y => y.endDate) > date || x.missionStopDate > date)).ToList();
                            foreach (Engagement eng in companyEngagements)
                            {
                                Employee emp = getEmployeeByCode(eng.employeeCode).Item1;
                                if (emp != null && !employees.Contains(emp)) employees.Add(emp);
                            }
                            chart.chart.Add(new Tuple<ChartEnum, int>((ChartEnum)month, employees.Count()));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<ChartResponse, string>(chart, exmsg);
        }

        public Tuple<ChartResponse, string> getBranchEngagedEmployees(int year, long branchId)
        {
            string exmsg = string.Empty;
            ChartResponse chart = new ChartResponse() { chart = new List<Tuple<ChartEnum, int>>() };
            List<Employee> engagedEmployees = null;
            List<Engagement> branchEngagements = new List<Engagement>();
            try
            {
                using (var dbContext = GetDBContext())
                {
                    for (int month = 1; month < 13; month++)
                    {
                        engagedEmployees = new List<Employee>();
                        UnitaDiBusiness filiale = dbContext.UnitaDiBusiness.Find(branchId);
                        DateTime date = new DateTime(year, month, 2);
                        branchEngagements = dbContext.Engagement.Where(x => x.businessUnitID.Equals(filiale.IDUnitaDiBusiness) && x.missionStartDate < date && (x.Extension.Max(y => y.endDate) > date || x.missionStopDate > date)).ToList();
                        foreach (Engagement eng in branchEngagements)
                        {
                            Employee emp = getEmployeeByCode(eng.employeeCode).Item1;
                            if (emp != null && !engagedEmployees.Contains(emp)) engagedEmployees.Add(emp);
                        }
                        chart.chart.Add(new Tuple<ChartEnum, int>((ChartEnum)month, engagedEmployees.Count()));
                    }
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<ChartResponse, string>(chart, exmsg);
        }

        public Tuple<ChartResponse, string> getEmployeeWorkedHours(int year, long employeeId)
        {
            string exmsg = string.Empty;
            ChartResponse chart = new ChartResponse() { chart = new List<Tuple<ChartEnum, int>>() };
            try
            {
                using (var dbContext = GetDBContext())
                {
                    Employee emp = dbContext.Employee.Find(employeeId);
                    if (emp.isBBSync)
                    {
                        for (int month = 1; month < 13; month++)
                        {
                            int workedHours = 0;
                            BadgeBoxGetRecordResponse records = BadgeBoxServer.getRecords(new BadgeBoxGetRecordsModel()
                            {
                                id_empl = (long)emp.BBId,
                                user_token = emp.BBToken,
                                from = new DateTime(year, month, 1).ToString("yyyy-MM-dd HH:mm:ss"),
                                to = MyHelper.GetDateTimeFromYearMonthWithLastOfMonth(year, month).ToString("yyyy-MM-dd HH:mm:ss")
                            }).Item1;
                            if (records.records != null && records.records.Count > 0)
                                workedHours = records.records.Sum(x => int.Parse(x.total_hours)) / 60;
                            chart.chart.Add(new Tuple<ChartEnum, int>((ChartEnum)month, workedHours));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<ChartResponse, string>(chart, exmsg);
        }
        #endregion

        #region TIMESHEET
        public Tuple<TimeSheet, string> getTimeSheet(long id)
        {
            string exmsg = string.Empty;
            TimeSheet ts = null;
            try
            {
                using (var dbcontext = GetDBContext())
                {
                    ts = dbcontext.TimeSheet.Find(id);
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<TimeSheet, string>(ts, exmsg);
        }

        public Tuple<TimeSheet, string> getTimeSheetByBBID(long BBid)
        {
            string exmsg = string.Empty;
            TimeSheet ts = null;
            try
            {
                using (var dbcontext = GetDBContext())
                {
                    ts = dbcontext.TimeSheet.Where(x => x.BBID.Equals(BBid)).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<TimeSheet, string>(ts, exmsg);
        }

        public Tuple<int, string> getNotConfirmedTimeSheetCountByEmployee(long employeeId)
        {
            string exmsg = string.Empty;
            int count = 0;
            try
            {
                using (var dbcontext = GetDBContext())
                {
                    count = dbcontext.TimeSheet.Where(x => x.employeeId == employeeId && x.isConfirmed == false).Count();
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<int, string>(count, exmsg);
        }

        public bool setTimeSheetConfirmed(long BBid, bool isEasy = false)
        {
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        TimeSheet temp = dbcontext.TimeSheet.Where(x => x.BBID == BBid).FirstOrDefault();
                        temp.isConfirmed = true;
                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                        Employee emp = dbcontext.Employee.Find(temp.employeeId);
                        EmailSender.sendEmail(emp.email, "Rapportino AgriBox", "Salve" + emp.name + ", il suo datore di lavoro ha confermato il rapportino che ha inviato, può ricontrollare lo stato dei rapportini quando vuole dalla piattaforma AgriBox", isEasy);
                        return true;
                    }
                    catch (Exception)
                    {
                        dbContextTransaction.Rollback();
                        return false;
                    }
                }
            }
        }

        public Tuple<long, string> insertTimeSheet(TimeSheet ts)
        {
            string exmsg = string.Empty;
            long idTimeSheet = 0;

            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        TimeSheet temp = dbcontext.TimeSheet.Add(ts);
                        dbcontext.SaveChanges();
                        idTimeSheet = temp.ID;
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<long, string>(idTimeSheet, exmsg);
        }

        #endregion

        #region AWDOCUMENT

        public void pollingAWDocuments(Documenti docs)
        {
            List<Documento> tempList = new List<Documento>();
            tempList.AddRange(docs.ListaDocumenti.element);
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    foreach (Documento d in docs.ListaDocumenti.element)
                    {
                        var dbDoc = dbcontext.AWDocument.Find(Guid.Parse(d.UID));
                        MyHelper.Log("DB -> insert " + d.IDDocumento);
                        try
                        {
                            //CONTROLLO DOCUMENTI ASSUNZIONE PER ABILITAZIONE ISTANTANEA
                            if (dbDoc == null)
                            {
                                MyHelper.Log("CREO NUOVO DOCUMENTO CON: "
                                    + docs.Sorgente.ToString()
                                    + Guid.Parse(d.UID)
                                    + d.IDDocumento
                                    + d.TipoDocumento
                                    );

                                AWDocument td;

                                if (d.TipoDocumento.Equals(TipoDocumento.Assunzione.ToString()) || d.TipoDocumento.Equals(TipoDocumento.ProrogaAssunzione.ToString()))
                                {
                                    Engagement e = getEngagementByNumber(d.IDDocumento).Item1;
                                    AWDocument somDoc = getAWDocumentsByCode(e.administrationNumber).Item1.Where(x => x.signed == false).FirstOrDefault();
                                    if (somDoc == null)
                                    {
                                        td = dbcontext.AWDocument.Add(new AWDocument()
                                        {
                                            source = docs.Sorgente.ToString(),
                                            UID = Guid.Parse(d.UID),
                                            IDDocumento = d.IDDocumento,
                                            tipo = d.TipoDocumento,
                                            signable = true
                                        });
                                        new EmailHandler().toBeSignedReminder(getEmployeeByCode(e.employeeCode).Item1);

                                    }
                                    else
                                    {
                                        td = dbcontext.AWDocument.Add(new AWDocument()
                                        {
                                            source = docs.Sorgente.ToString(),
                                            UID = Guid.Parse(d.UID),
                                            IDDocumento = d.IDDocumento,
                                            tipo = d.TipoDocumento
                                        });
                                    }
                                }
                                else
                                {
                                    Administration adm = getAdministrationByAdministrationCode(d.IDDocumento).Item1;
                                    Company comp = getCompanyByAdministrationNumber(adm.administrationCode).Item1;
                                    

                                    td = dbcontext.AWDocument.Add(new AWDocument()
                                    {
                                        source = docs.Sorgente.ToString(),
                                        UID = Guid.Parse(d.UID),
                                        IDDocumento = d.IDDocumento,
                                        tipo = d.TipoDocumento
                                    });

                                    if (comp.ownerId != null)
                                    {
                                        new EmailHandler().toBeSignedReminder(getCustomer((long)comp.ownerId).Item1);
                                    }
                                }

                                dbcontext.SaveChanges();
                                if (td.ID > 0) tempList.Remove(d);
                            }
                            else
                            {
                                MyHelper.Log("AGGIORNO : " + dbDoc.IDDocumento);
                                dbDoc.tipo = d.TipoDocumento.ToString();
                                dbDoc.IDDocumento = d.IDDocumento;
                                dbcontext.SaveChanges();
                                tempList.Remove(d);
                            }
                        }
                        catch (Exception e)
                        {
                            MyHelper.AWDLog(new string[] { " UUID: " + d.UID + " ERROR: ", e.StackTrace });
                        }
                    }
                    dbContextTransaction.Commit();
                }
            }
        }

        //public void pollingAWDocuments(Documenti docs)
        //{
        //    Documento d = new Documento();
        //    d = docs.ListaDocumenti.element;
        //    using (var dbcontext = GetDBContext())
        //    {
        //        using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
        //        {
        //            var dbDoc = dbcontext.AWDocument.Find(Guid.Parse(d.UID));
        //            try
        //            {
        //                if (dbDoc == null)
        //                {
        //                    var td = dbcontext.AWDocument.Add(new AWDocument()
        //                    {
        //                        source = docs.Sorgente.ToString(),
        //                        UID = Guid.Parse(d.UID),
        //                        IDDocumento = d.IDDocumento,
        //                        tipo = d.TipoDocumenti.ToString()
        //                    });
        //                    dbcontext.SaveChanges();
        //                }
        //                else
        //                {
        //                    dbDoc.tipo = d.TipoDocumenti.ToString();
        //                    dbDoc.IDDocumento = d.IDDocumento;
        //                    dbcontext.SaveChanges();
        //                }
        //                dbContextTransaction.Commit();

        //            }
        //            catch (Exception e)
        //            {
        //                MyHelper.AWDLog(new string[] { " UUID: " + d.UID + " ERROR: ", e.Message });
        //            }
        //        }
        //    }
        //}

        public Tuple<string, string> deleteAWDocument(Documento doc)
        {
            string exmsg = string.Empty;
            string response = string.Empty;
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        AWDocument document = dbcontext.AWDocument.Find(Guid.Parse(doc.UID));
                        dbcontext.AWDocument.Remove(document);
                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                        response = "OK";
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                        dbContextTransaction.Rollback();
                    }
                }
            }
            return new Tuple<string, string>(response, exmsg);
        }

        public Tuple<AWDocument, string> getAWDocument(Guid guid)
        {
            string exmsg = string.Empty;
            AWDocument awd = null;
            using (var dbcontext = GetDBContext())
            {
                try
                {
                    awd = dbcontext.AWDocument.Find(guid);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            return new Tuple<AWDocument, string>(awd, exmsg);
        }

        public Tuple<List<AWDocument>, string> getAWDocuments(PersonModelBase person)
        {
            List<AWDocument> documents = new List<AWDocument>();
            List<Company> companies = null;
            string exmsg = string.Empty;
            try
            {
                switch (person.type)
                {
                    case UserTypeEnum.Customer:
                        companies = getCustomerCompanies(person.id).Item1;
                        foreach (Company c in companies)
                        {
                            //DA SISTEMARE

                            List<Administration> adms = getAdministrationsByCompany(c.companyCode).Item1;
                            foreach (Administration adm in adms)
                            {
                                documents.AddRange(getAWDocumentsByCode(adm.administrationCode).Item1.Where(x => x.tipo != null && x.tipo.Equals(TipoDocumento.Somministrazione.ToString())).ToList());
                                List<Extension> admExts = getExtensionsByAdministration(adm.ID).Item1;
                                foreach (Extension ext in admExts)
                                {
                                    documents.AddRange(getAWDocumentsByCode(ext.extensionNumber).Item1.Where(x => x.tipo != null && x.tipo.Equals(TipoDocumento.ProrogaSomministrazione.ToString())).ToList());
                                }
                            }
                        }
                        break;
                    case UserTypeEnum.Employee:
                        List<Engagement> engs = getEngagementsByEmployeeID(person.id).Item1;
                        foreach (Engagement eng in engs)
                        {
                            documents.AddRange(getAWDocumentsByCode(eng.engagementNumber).Item1.Where(x => x.tipo != null && x.signable == true && x.tipo.Equals(TipoDocumento.Assunzione.ToString())).ToList());
                            List<Extension> engExts = getExtensionsByEngagementID(eng.ID).Item1;
                            foreach (Extension ext in engExts)
                            {
                                documents.AddRange(getAWDocumentsByCode(ext.extensionNumber).Item1.Where(x => x.tipo != null && x.signable == true && x.tipo.Equals(TipoDocumento.ProrogaAssunzione.ToString())).ToList());
                            }
                        }
                        break;
                    case UserTypeEnum.Owner:
                        companies = getCustomerCompanies(person.id).Item1;
                        foreach (Company c in companies)
                        {
                            List<Administration> adms = getAdministrationsByCompany(c.companyCode).Item1;
                            foreach (Administration adm in adms)
                            {
                                documents.AddRange(getAWDocumentsByCode(adm.administrationCode).Item1.Where(x => x.tipo != null && x.tipo.Equals(TipoDocumento.Somministrazione.ToString())).ToList());
                                List<Extension> admExts = getExtensionsByAdministration(adm.ID).Item1;
                                foreach (Extension ext in admExts)
                                {
                                    documents.AddRange(getAWDocumentsByCode(ext.extensionNumber).Item1.Where(x => x.tipo != null && x.tipo.Equals(TipoDocumento.ProrogaSomministrazione.ToString())).ToList());
                                }
                            }
                        }
                        break;
                }
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
                exmsg = e.Message;
            }
            return new Tuple<List<AWDocument>, string>(documents, exmsg);
        }

        public Tuple<List<AWDocument>, string> getNotSignedAWDocuments()
        {
            List<AWDocument> documents = new List<AWDocument>();
            string exmsg = string.Empty;
            try
            {
                documents = GetDBContext().AWDocument.Where(x => x.signed == false && x.signing == false).ToList();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<AWDocument>, string>(documents, exmsg);
        }

        public Tuple<List<AWDocument>, string> getAWDocumentsEmployee(long id)
        {
            List<AWDocument> documents = new List<AWDocument>();
            string exmsg = string.Empty;

            //prendere engagement e da codice recuperare tutti i documenti associati, ricordarsi le proroghe
            Engagement eng = getEngagementByEmployeeID(id).Item1;
            documents.AddRange(getAWDocumentsByCode(eng.engagementNumber).Item1);
            List<Extension> engExts = getExtensionsByEngagementID(eng.ID).Item1;
            foreach (Extension ext in engExts)
            {
                documents.AddRange(getAWDocumentsByCode(ext.extensionNumber).Item1);
            }

            return new Tuple<List<AWDocument>, string>(documents, exmsg);
        }

        public Tuple<List<AWDocument>, string> getAWDocumentsByCode(string code)
        {
            string exmsg = string.Empty;
            List<AWDocument> documents = new List<AWDocument>();
            try
            {
                documents = GetDBContext().AWDocument.Where(x => x.IDDocumento == code).ToList();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<List<AWDocument>, string>(documents, exmsg);
        }

        public void setAWDocumentsSigning(FEAModelToken model)
        {
            using (var dbcontext = GetDBContext())
            {
                using (var transaction = dbcontext.Database.BeginTransaction())
                {
                    foreach (string uuid in model.docUuids)
                    {
                        try
                        {
                            AWDocument awd = dbcontext.AWDocument.Find(new Guid(uuid));
                            awd.signing = true;
                            awd.signingTime = DateTime.Now;
                            dbcontext.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                        }
                    }

                }
            }
        }

        public AWDocument setAWDocumentsUnsigned(Guid uuid)
        {
            AWDocument awd = null;
            using (var dbcontext = GetDBContext())
            {
                using (var transaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        awd = dbcontext.AWDocument.Find(uuid);
                        awd.signing = false;
                        awd.signingTime = null;
                        dbcontext.SaveChanges();
                        transaction.Commit();
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                    }
                }
            }
            return awd;
        }

        public void updateAWDocument(Guid id, AWTech.AWDoc.Protocolbuffer.Document document)
        {
            string exmsg = string.Empty;
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        AWDocument awd = null;

                        awd = dbcontext.AWDocument.Find(id);

                        MyHelper.Log("UUID DOC: " + awd.UID.ToString());
                        MyHelper.Log("DOCUMENT TITLE : " + document.Title);

                        Tuple<string, int> tupla = MyHelper.getCurrentSize((long)document.Size);

                        awd.size = tupla.Item2 + " " + tupla.Item1;
                        awd.fileName = document.Title;
                        awd.extension = document.MimeType;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        MyHelper.Log(e.Message);
                        dbContextTransaction.Rollback();
                    }
                }
            }
        }

        public void updateSignabilityAWDocument(AWDocument document)
        {
            string exmsg = string.Empty;
            AWDocument awd = null;
            using (var dbcontext = GetDBContext())
            {
                using (var dbContextTransaction = dbcontext.Database.BeginTransaction())
                {
                    try
                    {
                        awd = dbcontext.AWDocument.Find(document.ID);
                        awd.signable = document.signable;

                        dbcontext.SaveChanges();
                        dbContextTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        MyHelper.Log(exmsg = e.StackTrace + ":" + e.InnerException);
                        dbContextTransaction.Rollback();
                    }
                }
            }
        }
        #endregion
    }
}