//------------------------------------------------------------------------------
// <auto-generated>
//     Codice generato da un modello.
//
//     Le modifiche manuali a questo file potrebbero causare un comportamento imprevisto dell'applicazione.
//     Se il codice viene rigenerato, le modifiche manuali al file verranno sovrascritte.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EasyBoxApplicationServer.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class ArchiveDocuments
    {
        public long ID { get; set; }
        public string file_name { get; set; }
        public string file_path { get; set; }
        public string archive_name { get; set; }
        public string num_matricola { get; set; }
    }
}
