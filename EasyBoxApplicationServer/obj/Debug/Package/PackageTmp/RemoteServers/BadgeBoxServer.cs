﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.BadgeBox;
using EasyBoxApplicationServer.Models.BadgeBox.Models;
using EasyBoxApplicationServer.Models.BadgeBox.Requests;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace EasyBoxApplicationServer.RemoteServers
{
    public class BadgeBoxServer
    {
        #region DOCUMENT FILES
        //public static Tuple<BadgeBoxFilesResponse, string> getFiles(BadgeBoxGetFilesModel model)
        //{
        //    string exmsg = string.Empty;
        //    BadgeBoxFilesResponse filesResponse = null;

        //    try
        //    {
        //        string endpoint = "user/files";
        //        JObject jobj = JObject.FromObject(model);
        //        StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
        //        string resp = response.ReadToEnd();
        //        filesResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxFilesResponse>(resp);
        //    }
        //    catch (Exception e)
        //    {
        //        exmsg = e.Message;
        //    }

        //    return new Tuple<BadgeBoxFilesResponse, string>(filesResponse, exmsg);
        //}

        //public static Tuple<string, string> insertDocument(BadgeBoxDocumentModel model)
        //{
        //    string exmsg = string.Empty;
        //    string responseText = string.Empty;

        //    try
        //    {
        //        string endpoint = "upload/create";
        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Constants.BADGEBOX_BASE_URL + endpoint);
        //        //request.KeepAlive = true;
        //        //request.Credentials = System.Net.CredentialCache.DefaultCredentials;

        //        string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
        //        request.ContentType = string.Format("multipart/form-data; boundary={0}", boundary);
        //        request.Method = "POST";

        //        string header = string.Format("--{0}", boundary);
        //        string footer = header + "--";

        //        StringBuilder contents = new StringBuilder();
        //        // key
        //        contents.AppendLine(header);
        //        contents.AppendLine("Content-Disposition: form-data; name=\"key\"");
        //        contents.AppendLine();
        //        contents.AppendLine(Constants.BADGEBOX_APIKEY);
        //        // type
        //        contents.AppendLine(header);
        //        contents.AppendLine("Content-Disposition: form-data; name=\"type\"");
        //        contents.AppendLine();
        //        contents.AppendLine("8");
        //        // id_folder
        //        contents.AppendLine(header);
        //        contents.AppendLine("Content-Disposition: form-data; name=\"id_folder\"");
        //        contents.AppendLine();
        //        contents.AppendLine("-1");
        //        // id_company
        //        contents.AppendLine(header);
        //        contents.AppendLine("Content-Disposition: form-data; name=\"id_empl\""); 
        //        contents.AppendLine();
        //        contents.AppendLine(model.id_empl.ToString());
        //        // title
        //        contents.AppendLine(header);
        //        contents.AppendLine("Content-Disposition: form-data; name=\"title\"");
        //        contents.AppendLine();
        //        contents.AppendLine(model.title);
        //        // file
        //        contents.AppendLine(header);
        //        contents.AppendLine(string.Format("Content-Disposition: form-data; name=\"files\"; filename=\"{0}\"", model.title));
        //        contents.AppendLine("Content-Type: text/html");
        //        contents.AppendLine();
        //        contents.AppendLine(model.file);
        //        // footer
        //        contents.AppendLine(footer);

        //        byte[] contentbytes = Encoding.UTF8.GetBytes(contents.ToString());

        //        request.ContentLength = contentbytes.Length;

        //        using (Stream requestStream = request.GetRequestStream())
        //        {
        //            requestStream.Write(contentbytes, 0, contentbytes.Length);
        //            requestStream.Flush();
        //            requestStream.Close();

        //            using (WebResponse response = request.GetResponse())
        //            {
        //                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
        //                {
        //                    responseText = reader.ReadToEnd();
        //                }
        //            }
        //        }
        //        System.Diagnostics.Debug.WriteLine("responseText:" + responseText);
        //    }
        //    catch (Exception e)
        //    {
        //        exmsg = e.Message;
        //    }

        //    return new Tuple<string, string>(responseText, exmsg);
        //}
        #endregion

        #region USER
        public static Tuple<BadgeBoxCreateUserResponse, string> createUser(BadgeBoxUserModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxCreateUserResponse cuResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "api/createUser";
                MyHelper.Log(jobj.ToString());
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                MyHelper.Log(resp);
                cuResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxCreateUserResponse>(resp);
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
            return new Tuple<BadgeBoxCreateUserResponse, string>(cuResponse, exmsg);
        }

        public static Tuple<BadgeBoxAddEmployeeResponse, string> addEmployee(BadgeBoxAddEmployeeModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxAddEmployeeResponse cuResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "employee/create";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                cuResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxAddEmployeeResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxAddEmployeeResponse, string>(cuResponse, exmsg);
        }

        public static Tuple<BadgeBoxDeleteEmployeeResponse, string> deleteEmployee(BadgeBoxDeleteEmployeeModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxDeleteEmployeeResponse cuResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "agency/deleteEmployee";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                cuResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxDeleteEmployeeResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxDeleteEmployeeResponse, string>(cuResponse, exmsg);
        }

        public static Tuple<BadgeBoxGenericResponse, string> updateUser(BadgeBoxUpdateUserModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxGenericResponse cuResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "user/update";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                cuResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGenericResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxGenericResponse, string>(cuResponse, exmsg);
        }

        public static Tuple<bool, string> isUserExisting(BadgeBoxGetUserModel model)
        {
            string exmsg = string.Empty;
            bool exist = true;
            BadgeBoxGetUserResponse guResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "user/get";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                try
                {
                    guResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGetUserResponse>(resp);
                    if (guResponse == null || guResponse.user == null || Int32.Parse(guResponse.user.id) == 0)
                        exist = false;
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                    exist = false;
                }
            }
            catch (Exception e)
            {
                exist = false;
                exmsg = e.Message;
            }
            return new Tuple<bool, string>(exist, exmsg);
        }

        public static Tuple<BadgeBoxGetUserResponse, string> getBBUser(BadgeBoxGetUserModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxGetUserResponse guResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "user/get";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                try
                {
                    guResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGetUserResponse>(resp);
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxGetUserResponse, string>(guResponse, exmsg);
        }

        public static Tuple<BadgeBoxSwitchCompanyResponse, string> switchCompany(BadgeBoxSwitchCompanyModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxSwitchCompanyResponse scResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);

                string endpoint = "user/updateAgency";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                scResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxSwitchCompanyResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxSwitchCompanyResponse, string>(scResponse, exmsg);
        }

        //public static string deleteUsers(CustomersModel model)
        //{
        //    BadgeBoxGenericResponse bbResponse = null;
        //    string endpoint = "api/deleteUser";
        //    string exmsg = string.Empty;

        //    foreach (CustomerModel customer in model.customers)
        //    {
        //        BadgeBoxUserModel bbuser = new BadgeBoxUserModel()
        //        {
        //            name = customer.name,
        //            surname = customer.lastName,
        //            email = customer.email,
        //            key = Constants.BADGEBOX_APIKEY
        //        };
        //        try
        //        {
        //            JObject jobj = JObject.FromObject(bbuser);
        //            StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
        //            string resp = response.ReadToEnd();
        //            bbResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGenericResponse>(resp);
        //        }
        //        catch (Exception e)
        //        {
        //            exmsg = e.Message;
        //        }
        //    }
        //    return exmsg;
        //}
        #endregion

        #region COMPANY
        public static Tuple<BadgeBoxCreateCompanyResponse, string> createCompany(BadgeBoxCreateCompanyModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxCreateCompanyResponse cuResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);

                string endpoint = "agency/create";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                cuResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxCreateCompanyResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxCreateCompanyResponse, string>(cuResponse, exmsg);
        }

        public static Tuple<BadgeBoxGenericResponse, string> updateCompany(BadgeBoxCompanyModel model)
        {
            return null; //TODO
        }

        #endregion

        #region INFO & MISSION
        public static Tuple<BadgeBoxInfoResponse, string> createInfo(BadgeBoxCreateInfoModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxInfoResponse cuResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);

                string endpoint = "info/create";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                cuResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxInfoResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxInfoResponse, string>(cuResponse, exmsg);
        }
        #endregion

        #region RAPPORTINO
        public static Tuple<BadgeBoxCreateRecordResponse, string> createRecord(BadgeBoxCreateRecordModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxCreateRecordResponse crResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "record/create";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                crResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxCreateRecordResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxCreateRecordResponse, string>(crResponse, exmsg);
        }

        public static Tuple<string, string> createRecordAPI(BadgeBoxCreateRecordModel model)
        {
            string exmsg = string.Empty;
            string crResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "record/create";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                crResponse = response.ReadToEnd();
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<string, string>(crResponse, exmsg);
        }

        public static Tuple<BadgeBoxDeleteRecordResponse, string> deleteRecord(BadgeBoxDeleteRecordModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxDeleteRecordResponse drResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "record/delete";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                drResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxDeleteRecordResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxDeleteRecordResponse, string>(drResponse, exmsg);
        }

        public static Tuple<BadgeBoxGetRecordResponse, string> getRecords(BadgeBoxGetRecordsModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxGetRecordResponse grResponse = new BadgeBoxGetRecordResponse();
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "record/all";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                grResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGetRecordResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxGetRecordResponse, string>(grResponse, exmsg);
        }
        #endregion

        #region TIMESHEET
        public static Tuple<BadgeBoxCreateTimeSheetResponse, string> createTimeSheet(BadgeBoxCreateTimeSheetModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxCreateTimeSheetResponse grResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "timesheet/create";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                grResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxCreateTimeSheetResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxCreateTimeSheetResponse, string>(grResponse, exmsg);
        }

        public static Tuple<BadgeBoxGetTimeSheetResponse, string> getTimeSheet(BadgeBoxGetTimeSheetModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxGetTimeSheetResponse grResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "timesheet/search";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                grResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGetTimeSheetResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxGetTimeSheetResponse, string>(grResponse, exmsg);

            //string exmsg = string.Empty;
            //BadgeBoxGetTimeSheetResponse grResponse = null;
            //try
            //{
            //    JObject jobj = JObject.FromObject(model);
            //    string endpoint = "timesheet/downloadData";
            //    StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
            //    string resp = response.ReadToEnd();
            //    grResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGetTimeSheetResponse>(resp);
            //}
            //catch (Exception e)
            //{
            //    exmsg = e.Message;
            //}
            //return new Tuple<BadgeBoxGetTimeSheetResponse, string>(grResponse, exmsg);
        }

        public static Tuple<BadgeBoxGetOverallTimeSheetsResponse, string> getOverall(BadgeBoxGetOverallTimeSheetModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxGetOverallTimeSheetsResponse grResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "timesheet/overall";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                grResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGetOverallTimeSheetsResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxGetOverallTimeSheetsResponse, string>(grResponse, exmsg);
        }

        public static Tuple<BadgeBoxDownloadTimesheetResponse, string> download(BadgeBoxDownloadTimesheetModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxDownloadTimesheetResponse grResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "timesheet/download";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                grResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxDownloadTimesheetResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxDownloadTimesheetResponse, string>(grResponse, exmsg);
        }
        #endregion

        #region PUSHING
        private static Tuple<BadgeBoxGetLastRecordResponse, string> lastRecord(BadgeBoxGetLastRecordModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxGetLastRecordResponse lastRecordResponse = null;
            try
            {
                JObject jobj = JObject.FromObject(model);
                string endpoint = "track/lastRecord";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                lastRecordResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGetLastRecordResponse>(resp);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return new Tuple<BadgeBoxGetLastRecordResponse, string>(lastRecordResponse, exmsg);
        }

        public static Tuple<BadgeBoxGenericMessageResponse, string> checkIn(BadgeBoxCheckInModel model)
        {
            string MESSAGE = string.Empty;
            BadgeBoxGenericMessageResponse checkInResponse = null;
            try
            {
                DBHandler dbh = new DBHandler();
                Employee emp = dbh.getEmployee(model.id_empl).Item1;
                Company comp = dbh.getCompanyByEmployeeCode(emp.employeeCode).Item1;
                Customer cust = dbh.getOwner((long)comp.ownerId).Item1;
                BadgeBoxGetLastRecordResponse last = lastRecord(new BadgeBoxGetLastRecordModel()
                {
                    id_empl = (int)emp.BBId,
                    user_token = cust.BBToken
                }).Item1;
                model.id_empl = (int)emp.BBId;
                model.id_task = last.values[0].id;
                model.user_token = cust.BBToken;
                model.id_agency = (int)comp.BBId;
                JObject jobj = JObject.FromObject(model);
                string endpoint = "track/checkinDate";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                checkInResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGenericMessageResponse>(resp);
            }
            catch (NullReferenceException)
            {
                MESSAGE = "The requested object is not existing";
            }
            catch (Exception e)
            {
                MESSAGE = e.Message;
            }
            return new Tuple<BadgeBoxGenericMessageResponse, string>(checkInResponse, MESSAGE);
        }

        public static Tuple<BadgeBoxGenericMessageResponse, string> checkOut(BadgeBoxCheckOutModel model)
        {
            string MESSAGE = string.Empty;
            BadgeBoxGenericMessageResponse checkOutResponse = null;
            try
            {
                DBHandler dbh = new DBHandler();
                Employee emp = dbh.getEmployee(model.id_empl).Item1;
                Company comp = dbh.getCompanyByEmployeeCode(emp.employeeCode).Item1;
                Customer cust = dbh.getOwner((long)comp.ownerId).Item1;
                BadgeBoxGetLastRecordResponse last = lastRecord(new BadgeBoxGetLastRecordModel()
                {
                    id_empl = (int)emp.BBId,
                    user_token = cust.BBToken
                }).Item1;
                model.id_task = last.values[0].id;
                model.user_token = cust.BBToken;
                model.id_agency = (int)comp.BBId;
                JObject jobj = JObject.FromObject(model);
                string endpoint = "track/checkoutDate";
                StreamReader response = Request.PostRequest(Constants.BADGEBOX_BASE_URL + endpoint, jobj.ToString());
                string resp = response.ReadToEnd();
                checkOutResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGenericMessageResponse>(resp);
            }
            catch (NullReferenceException)
            {
                MESSAGE = "The requested object is not existing";
            }
            catch (Exception e)
            {
                MESSAGE = e.Message;
            }
            return new Tuple<BadgeBoxGenericMessageResponse, string>(checkOutResponse, MESSAGE);
        }
        #endregion
    }
}