﻿using Quartz;

namespace EasyBoxApplicationServer.Quartz.Jobs
{
    public class BadgeBoxSyncJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            new RemoteServers.BadgeBoxSyncController().syncronizeBB();
        }
    }
}