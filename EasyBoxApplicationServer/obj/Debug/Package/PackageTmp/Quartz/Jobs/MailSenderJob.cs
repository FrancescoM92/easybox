﻿using EasyBoxApplicationServer.Utils;
using Quartz;

namespace EasyBoxApplicationServer.Quartz.Jobs
{
    public class MailSenderJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            new EmailHandler().toBeSignedReminder();
        }
    }
}