﻿using EasyBoxApplicationServer.Quartz.Jobs;
using Quartz;
using Quartz.Impl;
using System;

namespace EasyBoxApplicationServer.Quartz
{
    public class Quartz
    {
        public static void Scheduler()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();

            IScheduler scheduler = schedulerFactory.GetScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<BadgeBoxSyncJob>()
                .WithIdentity("job_sync_badgebox")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("trigger_sync_badgebox")
                .StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInMinutes(1)
                .RepeatForever())
                .Build();

            scheduler.ScheduleJob(job, trigger);

            IJobDetail jobEmailDaily = JobBuilder.Create<MailSenderJob>()
                .WithIdentity("job_email_daily")
                .Build();
            DateTime tomorrowAt14 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0);
            tomorrowAt14 = tomorrowAt14.AddDays(1);
            ITrigger triggerEmailDaily = TriggerBuilder.Create()
                .WithIdentity("trigger_email_daily")
                .StartAt(tomorrowAt14)
                //.StartNow()
                .WithSimpleSchedule(x => x.WithIntervalInHours(24)
                //.WithSimpleSchedule(x => x.WithIntervalInMinutes(10)
                .RepeatForever())
                .Build();

            scheduler.ScheduleJob(jobEmailDaily, triggerEmailDaily);
        }
    }
}