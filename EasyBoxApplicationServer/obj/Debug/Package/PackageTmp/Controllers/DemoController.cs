﻿using EasyBoxApplicationServer.Utils;
using System;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    public class DemoController : BaseController
    {
        public void insertUser(string json)
        {
            if (!String.IsNullOrEmpty(json))
            {
                MyHelper.writeOnDoc(json);
                Response.StatusCode = 200;
            }
            else
                Response.StatusCode = 400;
        }

        public JsonResult getUsers()
        {
            return getJsonResult(null, MyHelper.readFromDoc(), true);
        }
    }
}
