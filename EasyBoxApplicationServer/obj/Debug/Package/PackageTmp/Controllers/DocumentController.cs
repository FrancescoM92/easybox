﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.ServiceBus;
using EasyBoxApplicationServer.RemoteServers;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class DocumentController : BaseController
    {
        public void startBBSync()
        {
            new BadgeBoxSyncController().syncronizeBB();
        }

        public JsonResult getDocuments(getDocumentsModel model)
        {
            DocumentsModel response = null;
            string exmsg = string.Empty;
            PersonModelBase person = getDBHandler().getUserFromToken(model.token, null);
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.companyId != null && model.companyId > 0)
                    {
                        response = new DocumentsModel()
                        {
                            documents = getDBHandler().getCompanyDocuments((long)model.companyId).Item1
                        };
                        if (person != null)
                        {
                            List<AWDocument> AWDocs = getDBHandler().getAWDocuments(person).Item1;
                            List<DocumentModel> docModel = new List<DocumentModel>();
                            foreach (AWDocument awd in AWDocs)
                            {
                                if (awd.source != Sorgente.DataBook.ToString())
                                {
                                    docModel.Add(new DocumentModel
                                    {
                                        category = awd.source,
                                        fileName = awd.fileName,
                                        size = awd.size,
                                        extension = awd.extension,
                                        Guid = awd.UID
                                    });
                                }
                            }
                            response.documents.AddRange(docModel);
                        }
                    }
                    else if (model.employeeId != null && model.employeeId > 0)
                    {
                        response = new DocumentsModel()
                        {
                            documents = getDBHandler().getEmployeeDocuments((long)model.employeeId).Item1
                        };
                        if (person != null)
                        {
                            List<AWDocument> AWDocs = getDBHandler().getAWDocumentsEmployee((long)model.employeeId).Item1;
                            List<DocumentModel> docModel = new List<DocumentModel>();
                            foreach (AWDocument awd in AWDocs)
                            {
                                if (awd.source != Sorgente.DataBook.ToString())
                                {
                                    docModel.Add(new DocumentModel
                                    {
                                        category = awd.source,
                                        fileName = awd.fileName,
                                        size = awd.size,
                                        extension = awd.extension,
                                        Guid = awd.UID
                                    });
                                }
                            }
                            response.documents.AddRange(docModel);
                        }
                    }
                    else if ((model.companyId == null || model.companyId == 0) && (model.employeeId == null || model.employeeId == 0))
                    {
                        response = new DocumentsModel()
                        {
                            documents = getDBHandler().getEmployeeDocuments(person.id).Item1
                        };
                        if (person != null)
                        {
                            List<AWDocument> AWDocs = getDBHandler().getAWDocuments(person).Item1;
                            List<DocumentModel> docModel = new List<DocumentModel>();
                            foreach (AWDocument awd in AWDocs)
                            {
                                if (awd.source != Sorgente.DataBook.ToString())
                                {
                                    docModel.Add(new DocumentModel
                                    {
                                        category = awd.source,
                                        fileName = awd.fileName,
                                        size = awd.size,
                                        extension = awd.extension,
                                        Guid = awd.UID
                                    });
                                }
                            }
                            response.documents.AddRange(docModel);
                        }
                    }
                }
                catch (Exception e)
                {
                    exmsg = e.Message;
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values);
            }
            return getJsonResult(exmsg, response);
        }

        public JsonResult getAdministrativeDocuments(getDocumentsModel model)
        {
            DocumentsModel documents = new DocumentsModel();
            documents.documents = new List<DocumentModel>();
            if (model.employeeId != null && model.employeeId > 0)
            {
                List<ArchiveDocuments> docs = getDBHandler().getArchiveDocumentsEmployee((long)model.employeeId).Item1;
                if (docs.Count > 0)
                {
                    foreach (ArchiveDocuments doc in docs)
                    {
                        documents.documents.Add(new DocumentModel()
                        {
                            employeeId = model.employeeId,
                            fileName = doc.file_name,
                            id = doc.ID
                        });
                    }
                }
                return getJsonResult(null, documents);
            }
            else return getJsonResult("Identificativo lavoratore assente", null);

        }

        public JsonResult getCategories(getDocumentsModel model)
        {
            string exmsg = string.Empty;
            Tuple<List<string>, string> categories = null;
            if (model.companyId > 0)
                categories = getDBHandler().getDocumentCategories((long)model.companyId, 0);
            else
            {
                PersonModelBase person = getDBHandler().getUserFromToken(model.token, null);
                categories = getDBHandler().getDocumentCategories(0, person.id);
            }
            exmsg = categories.Item2;
            return getJsonResult(exmsg, categories.Item1);
        }

        [HttpGet]
        public void getDocument()
        {
            long id = long.Parse(Request.Params["id"]);
            Document file = getDBHandler().getDocument(id).Item1;
            if (file.nome.Split('.')[1].Equals("pdf"))
                Response.ContentType = "application/pdf";
            else
                Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", @"filename=" + file.nome);
            MemoryStream memoryStream = new MemoryStream(file.documento);
            memoryStream.CopyTo(Response.OutputStream);
            memoryStream.Close();
        }

        [HttpGet]
        public void getAdministrativeDocument()
        {
            long id = long.Parse(Request.Params["id"]);
            ArchiveDocuments file = getDBHandler().findArchiveDocument(id);
            if (file.file_name.Split('.')[1].Equals("pdf"))
                Response.ContentType = "application/pdf";
            else
                Response.ContentType = "application/octet-stream";
            Response.AddHeader("Content-Disposition", @"filename=" + file.file_name);
            try
            {
                MemoryStream memoryStream = new MemoryStream(System.IO.File.ReadAllBytes(file.file_path));
                memoryStream.CopyTo(Response.OutputStream);
                memoryStream.Close();
            }
            catch (Exception ex)
            {
                MyHelper.Log(ex.ToString());
            }
        }

        public JsonResult Upload(DocumentModel model, bool isEasy)
        {
            Tuple<long, string> result = null;
            if (ModelState.IsValid)
            {
                result = getDBHandler().insertDocument(model, isEasy);
            }
            else
                result = new Tuple<long, string>(0, getErrorMessage(ModelState.Values));

            return getJsonResult(null, null);
        }

        public JsonResult UploadArchive(DocumentModel model, bool isEasy)
        {
            List<ArchiveDocuments> docInfoList = new List<ArchiveDocuments>();
            string easyboxResourcesPath = Path.Combine("C:\\", "EasyBoxResources");

            DirectoryInfo infoDocuments;
            string infoArchives = Path.Combine(easyboxResourcesPath, "archives");
            try
            {
                string archivioPath = Path.Combine(infoArchives, model.data.FileName);

                if (System.IO.File.Exists(archivioPath))
                {
                    return getJsonResult("ZIP già inserito precedentemente", null);
                }

                MyHelper.Log("PATH ARCHIVI: " + infoArchives);
                MyHelper.Log("PATH ARCHIVIO: " + archivioPath);


                Stream dataInput = model.data.InputStream;
                using (var reader = new BinaryReader(dataInput))
                {
                    System.IO.File.WriteAllBytes(archivioPath, reader.ReadBytes(model.data.ContentLength));
                }

                MyHelper.Log("Scrittura ZIP : " + archivioPath + " completata");

                using (var fileStream = System.IO.File.OpenRead(archivioPath))
                {
                    using (ZipArchive archive = new ZipArchive(fileStream))
                    {
                        foreach (ZipArchiveEntry entry in archive.Entries)
                        {
                            if (entry.Name != null && entry.Name != "")
                            {
                                MyHelper.Log("ENTRY: " + entry.Name);
                                string extractFileName;
                                try
                                {
                                    string creaDirectoryDocuments = Path.Combine(Path.Combine(easyboxResourcesPath, "documents"), entry.Name);
                                    MyHelper.Log(creaDirectoryDocuments);

                                    string directoryName = creaDirectoryDocuments.Substring(0, creaDirectoryDocuments.IndexOf("."));
                                    infoDocuments = Directory.CreateDirectory(directoryName);

                                    extractFileName = model.data.FileName.Replace(model.data.FileName.Substring(model.data.FileName.IndexOf('.')), entry.Name.Substring(entry.Name.IndexOf('.')));
                                    entry.ExtractToFile(Path.Combine(infoDocuments.FullName, extractFileName), true);
                                }
                                catch (Exception e)
                                {
                                    MyHelper.Log(e.StackTrace);
                                    return getJsonResult("Errore nella creazione o estrazione del file");
                                }

                                try
                                {
                                    ArchiveDocuments docInfo = new ArchiveDocuments()
                                    {
                                        archive_name = model.data.FileName,
                                        file_name = extractFileName,
                                        file_path = Path.Combine(infoDocuments.FullName, extractFileName),
                                        num_matricola = entry.Name.Replace(entry.Name.Substring(entry.Name.IndexOf('.')), "")
                                    };
                                    docInfoList.Add(docInfo);
                                }
                                catch (Exception ex)
                                {
                                    MyHelper.Log("ARCHIVE: " + infoArchives);
                                    MyHelper.Log("Info File : " + entry.Name + " non scrivibile, procedere manualmente");
                                    MyHelper.Log("ERROR: " + ex.Message);
                                }
                            }
                        }
                    }
                }

                MyHelper.Log("COUNT LISTA DOC DA INSERIRE: " + docInfoList.Count);

                if (docInfoList.Count > 0)
                {
                    getDBHandler().insertDocumentInfo(docInfoList);
                }
            }
            catch (FileNotFoundException fex)
            {
                return getJsonResult("Errore in scrittura file, file zip non trovato");
            }
            catch (Exception ex)
            {
                MyHelper.Log(ex.StackTrace);
                MyHelper.Log(ex.Message);

                return getJsonResult("Attenzione, file zip non valido", null);
            }

            return getJsonResult(null, true);
        }

        public JsonResult UploadDocumentData(DocumentModel model, bool isEasy)
        {
            Tuple<long, string> result = null;
            if (ModelState.IsValid)
            {
                result = getDBHandler().insertDocument(model, isEasy);
            }
            else
                result = new Tuple<long, string>(0, getErrorMessage(ModelState.Values));

            return getJsonResult(null, null);
        }

        public JsonResult delete(DeleteDocumentModel model)
        {
            string response = string.Empty;
            string exmsg = string.Empty;
            if (ModelState.IsValid)
            {
                Tuple<string, string> result = getDBHandler().deleteDocument(model);
                response = result.Item1;
                exmsg = result.Item2;
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values);
            }
            return getJsonResult(exmsg, response);
        }

    }
}