﻿using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RestAuthorization;
using System;
using System.Linq;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class BranchController : BaseController
    {
        public JsonResult getBranchDetail(GetBranchDetailModel model)
        {
            string exmsg = string.Empty;
            GetBranchDetailResponseModel response = null;

            if (ModelState.IsValid)
            {
                Tuple<GetBranchDetailResponseModel, string> result = getDBHandler().getBranchDetail(model.branchId);

                if (string.IsNullOrEmpty(result.Item2))
                    response = result.Item1;
                else
                    exmsg = result.Item2;
            }
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());

            return getJsonResult(exmsg, response);
        }

        public JsonResult getBranchDetailChart(ChartModel model)
        {
            Tuple<ChartResponse, string> response = getDBHandler().getBranchEngagedEmployees(model.year, model.id);
            return getJsonResult(response.Item2, response.Item1);
        }
    }
}