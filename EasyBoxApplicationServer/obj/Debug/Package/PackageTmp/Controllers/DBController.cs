﻿using EasyBoxApplicationServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    public class DBController : BaseController
    {
        public JsonResult testDB()
        {
            return Json(new DBHandler().testDB(), JsonRequestBehavior.AllowGet);
        }
    }
}
