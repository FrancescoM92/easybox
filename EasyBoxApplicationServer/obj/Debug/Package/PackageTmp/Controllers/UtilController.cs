﻿using AWTech.AWDoc.Protocolbuffer;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.ServiceBus;
using EasyBoxApplicationServer.RemoteServers.AWDoc;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Services;
using EasyBoxApplicationServer.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{

    public class UtilController : BaseController
    {
        [RESTAuthorize]
        [HttpPost]
        public JsonResult updateImage()
        {
            var file = HttpContext.Request.Files.Count > 0 ? HttpContext.Request.Files[0] : null;
            var token = HttpContext.Request.Params["token"];
            var id = HttpContext.Request.Params["id"];
            var type = HttpContext.Request.Params["type"];

            string exmsg = string.Empty;

            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                var fileLength = file.ContentLength;

                if (fileLength <= Constants.MAXDIMFILE)
                {
                    try
                    {
                        Stream stream = file.InputStream;
                        Image immagine = Image.FromStream(stream);
                        ImageConverter _imageConverter = new ImageConverter();
                        byte[] xByte = (byte[])_imageConverter.ConvertTo(immagine, typeof(byte[]));

                        string base64 = Convert.ToBase64String(xByte);

                        getDBHandler().updateImage(new UpdateImageModel()
                        {
                            id = int.Parse(id),
                            token = token,
                            type = bool.Parse(type)
                        }, xByte);
                    }
                    catch (Exception e)
                    {
                        exmsg = e.Message;
                    }
                }
                else exmsg = "File troppo grande";
            }
            return getJsonResult(exmsg);
        }

        public JsonResult confirmRecoverPassword(string email, bool isEasy = false)
        {
            string result = string.Empty;
            PersonModelBase person = getDBHandler().getUserFromCredential(email).Item1;
            if (person.email != null && person.password != null)
            {
                EmailSender.sendEmail(person.email, "Conferma Recupero Password", "Questo è il link di conferma di reset della sua password: http://easybox.staging.openjobmetis.it/login.html?user=" + person.email + "&recover=true", isEasy);
                result = "Le è stata inviata una mail di conferma";
            }
            else
            {
                result = "Utente inesistente";
            }
            return getJsonResult(null, result);
        }

        [HttpPost]
        public JsonResult recoverPassword(string email, bool isEasy = false)
        {
            PersonModelBase person = getDBHandler().getUserFromCredential(email).Item1;
            string password = getDBHandler().resetUserPassword(person);
            EmailSender.sendEmail(person.email, "Nuova Password EasyBox", "Questa è la tua nuova password: " + password + ", utilizzala per entrare nel portale EasyBox", isEasy);
            return getJsonResult(null, string.IsNullOrEmpty(password) ? "Errore nel recupero della password" : "Password resettata correttamente, controlli la sua e-mail");
        }

        public JsonResult getAWDocCategories()
        {
            List<Category> cats = new AWTest().getcategories();
            return getJsonResult(null, cats);
        }

        public void testSomministrazione(string jsonn)
        {
            JToken json = JToken.Parse(jsonn);
            long id = 0;

            Debug.Write(json.ToString());
            json = json.First.First;
            JArray jar = new JArray();
            var tok = json.SelectToken("Proroga");
            if (tok != null && ((tok is JArray == false) || !tok.ToString().Contains("[")))
            {
                jar.Add(tok);
                json.SelectToken("Proroga").Replace(jar);
            }
            var body = json.ToObject<Somministrazione>();
            if (body != null)
                id = getDBHandler().insertAdministration(AdministrationModel.Create(body)).Item1;
        }

        public void testCliente(string jsonn)
        {
            JToken json = JToken.Parse(jsonn);
            long id = 0;

            Debug.Write(json.ToString());
            json = json.First.First;
            JArray jar = new JArray();
            var tok = json.SelectToken("ElencoSedi");
            if ((tok is JArray == false) || !tok.ToString().Contains("["))
            {
                jar.Add(tok);
                json.SelectToken("ElencoSedi").Replace(jar);
            }
            var body = json.ToObject<Cliente>();
            if (body != null)
                id = getDBHandler().insertTemporaryCompany(CompanyModel.Create(body)).Item1;
        }
        public void testLavoratore(string jsonn)
        {
            JToken json = JToken.Parse(jsonn);
            long id = 0;

            Debug.Write(json.ToString());
            json = json.First.First;
            var body = json.ToObject<Lavoratore>();
            if (body != null)
                id = getDBHandler().insertEmployee(EmployeeModel.CreateEmployee(body)).Item1;
        }
        public void testAssunzione(string jsonn)
        {
            JToken json = JToken.Parse(jsonn);
            long id = 0;

            Debug.Write(json.ToString());
            json = json.First.First;
            JArray jar = new JArray();
            var tok = json.SelectToken("Proroga");
            if (tok != null && ((tok is JArray == false) || !tok.ToString().Contains("[")))
            {
                jar.Add(tok);
                json.SelectToken("Proroga").Replace(jar);
            }
            var body = json.ToObject<Assunzione>();
            if (body != null)
                id = getDBHandler().insertEngagement(EngagementModel.Create(body)).Item1;
        }
        public void testFiliale(string jsonn)
        {
            JToken json = JToken.Parse(jsonn);
            long id = 0;
            Debug.Write(json.ToString());
            json = json.First.First;
            var body = json.ToObject<BusinessUnit>();
            if (body != null)
                id = getDBHandler().insertBusinessUnit(BusinessUnitModel.Create(body)).Item1;
        }
        public void testDocs(string jsonn)
        {
            JToken json = JToken.Parse(jsonn);

            Debug.Write(json.ToString());
            json = json.First.First;
            var body = json.ToObject<Documenti>();
            if (body != null)
            {
                getDBHandler().pollingAWDocuments(body);
            }
        }

        [HttpPost]
        public JsonResult serviceBusMessages()
        {
            try
            {
                var todayFile = "C://EasyBoxLogs//ServiceBus_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
                if (System.IO.File.Exists(todayFile))
                {
                    return getJsonResult(null, new List<string>(System.IO.File.ReadAllLines(todayFile)));
                }
                else
                {
                    return getJsonResult(null, new List<string>(new string[] { "NESSUN MESSAGGIO TROVATO" }));
                }
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
                return getJsonResult(null, new List<string>(new string[] { "NESSUN MESSAGGIO TROVATO" }));
            }
        }
    }
}
