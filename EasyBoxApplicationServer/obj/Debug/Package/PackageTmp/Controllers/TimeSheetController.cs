﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.BadgeBox;
using EasyBoxApplicationServer.Models.BadgeBox.Requests;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RemoteServers;
using EasyBoxApplicationServer.RestAuthorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class TimeSheetController : BaseController
    {
        public JsonResult create(BadgeBoxCreateTimeSheetModel model)
        {
            string exmsg = string.Empty;
            Employee emp = getDBHandler().getEmployee(model.from_user).Item1;
            Customer cust = getDBHandler().getOwnerFromEmployee(emp).Item1;
            Tuple<BadgeBoxCreateTimeSheetResponse, string> response = null;
            int daysInMonth = DateTime.DaysInMonth(model.year, model.month);
            string firstOfMonth = new DateTime(model.year, model.month, 1).ToString("yyyy-MM-dd");
            string lastOfMonth = new DateTime(model.year, model.month, daysInMonth).ToString("yyyy-MM-dd");
            model.from_user = (long)emp.BBId;
            model.user_token = cust.BBToken;
            model.from = firstOfMonth;
            model.to = lastOfMonth;
            if (emp.BBId > 0)
            {
                response = BadgeBoxServer.createTimeSheet(model);
                if (response.Item1.id > 0)
                {
                    getDBHandler().insertTimeSheet(new TimeSheet()
                    {
                        BBID = response.Item1.id
                    });
                }
            }
            else
                exmsg = "Utente non sincronizzato in BadgeBox";

            return getJsonResult(response.Item2, response.Item1);
        }

        public JsonResult overAll(BadgeBoxGetOverallTimeSheetModel model)
        {
            Tuple<BadgeBoxGetOverallTimeSheetsResponse, string> response = BadgeBoxServer.getOverall(model);

            return getJsonResult(response.Item2, response.Item1);
        }

        public JsonResult download(TokenModel model)
        {
            Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
            Tuple<BadgeBoxDownloadTimesheetResponse, string> response = BadgeBoxServer.download(new BadgeBoxDownloadTimesheetModel()
            {
                user_token = emp.BBToken,
                id = model.id
            });

            return getJsonResult(response.Item2, response.Item1);
        }

        public JsonResult confirm(TokenModel model, bool isEasy)
        {
            bool ok = false;
            ok = getDBHandler().setTimeSheetConfirmed(model.id, isEasy);
            return getJsonResult(null, ok);
        }

        #region RECORDS
        public JsonResult getRecords(EBtoBBRecordModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxGetRecordResponse records = new BadgeBoxGetRecordResponse();
            int hours = 0;
            if (ModelState.IsValid)
            {
                Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
                if (emp.BBId != null && emp.BBId > 0)
                {
                    records = BadgeBoxServer.getRecords(new BadgeBoxGetRecordsModel()
                    {
                        id_empl = (long)emp.BBId,
                        user_token = emp.BBToken
                    }).Item1;
                    if (records.records != null)
                        foreach (Record rec in records.records)
                        {
                            hours += int.Parse(rec.total_hours);
                        }
                    records.hours = hours / 60;
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }
            RecordsModel record = RecordsModel.Create(records);
            if (record != null && record.records != null)
            {
                List<RecordModel> recordss = record.records.OrderBy(x => x.day).ToList();
                record.records = recordss;
            }
            StringsModel strings = new StringsModel();
            foreach (RecordModel r in record.records)
            {
                strings.stringhe.Add(r.day);
            }
            return getJsonResult(exmsg, strings);
        }

        public JsonResult getDateRecords(EBtoBBRecordModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxGetRecordResponse records = new BadgeBoxGetRecordResponse();
            BadgeBoxGetTimeSheetResponse timeSheet = new BadgeBoxGetTimeSheetResponse();
            int hours = 0;
            if (ModelState.IsValid)
            {
                Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
                if (emp.isBBSync)
                {
                    Company comp = getDBHandler().getCompanyByEmployeeCode(emp.employeeCode).Item1;
                    Customer cust = getDBHandler().getOwner((long)comp.ownerId).Item1;
                    DateTime from = new DateTime(model.year, model.month, 1);
                    DateTime to = new DateTime(model.year, model.month, DateTime.DaysInMonth(DateTime.Now.Year, model.month), 23, 59, 59);

                    records = BadgeBoxServer.getRecords(new BadgeBoxGetRecordsModel()
                    {
                        id_empl = (long)emp.BBId,
                        user_token = cust.BBToken,
                        from = from.ToString("yyyy-MM-dd HH:mm:ss"),
                        to = to.ToString("yyyy-MM-dd HH:mm:ss")
                    }).Item1;
                    if (records.records != null)
                        foreach (Record rec in records.records)
                        {
                            hours += int.Parse(rec.total_hours);
                        }
                    records.hours = hours / 60;
                    timeSheet = BadgeBoxServer.getTimeSheet(new BadgeBoxGetTimeSheetModel()
                    {
                        from = from.ToString("yyyy-MM-dd"),
                        to = to.ToString("yyyy-MM-dd"),
                        id_empl = (long)emp.BBId,
                        user_token = emp.BBToken
                    }).Item1;
                    if (timeSheet.values != null)
                    {
                        foreach (TimeSheetResponse tsr in timeSheet.values)
                        {
                            tsr.isConfirmed = getDBHandler().getTimeSheetByBBID(tsr.id).Item1.isConfirmed;
                        }
                    }
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }
            RecordsModel record = RecordsModel.Create(records);
            if (record != null && record.records != null && record.records.Count > 0)
            {
                List<RecordModel> recordss = record.records.OrderBy(x => x.day).ToList();
                record.records = recordss;
            }
            record.timeSheet = timeSheet;
            return getJsonResult(exmsg, record);
        }

        public JsonResult createRecord(EBtoBBRecordModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxCreateRecordResponse response = null;
            BadgeBoxGetRecordResponse records = null;
            DateTime target = new DateTime(model.checkin);
            int daysInMonth = DateTime.DaysInMonth(target.Year, target.Month);
            string firstOfMonth = new DateTime(target.Year, target.Month, 1, 0, 0, 0).ToString("yyyy-MM-dd HH:mm:ss");
            string lastOfMonth = new DateTime(target.Year, target.Month, daysInMonth, 23, 59, 59).ToString("yyyy-MM-dd HH:mm:ss");
            int hours = 0;
            if (ModelState.IsValid)
            {
                Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
                Tuple<Engagement, Extension> currents = getDBHandler().getCurrentEngagementWithExtensionByEmployeeID(emp.ID);

                if (!string.IsNullOrEmpty(emp.BBToken))
                {
                    if (currents.Item1 != null)
                    {
                        DateTime controller = (DateTime) currents.Item1.missionStopDate;
                        if (currents.Item2 != null)
                        {
                            controller = (DateTime) currents.Item2.endDate;
                        }
                        if (target <= controller)
                        {
                            Company comp = getDBHandler().getCompanyByEmployeeCode(emp.employeeCode).Item1;
                            Customer cust = getDBHandler().getOwner((long)comp.ownerId).Item1;
                            DateTime checkin = new DateTime(model.checkin);
                            var checkout = checkin.AddHours(model.hours);
                            response = BadgeBoxServer.createRecord(new BadgeBoxCreateRecordModel()
                            {
                                id_empl = (long)emp.BBId,
                                user_token = cust.BBToken,
                                checkin = new DateTime(model.checkin).ToString("yyyy-MM-dd HH:mm:ss"),
                                checkout = checkout.ToString("yyyy-MM-dd HH:mm:ss")
                            }).Item1;
                            records = BadgeBoxServer.getRecords(new BadgeBoxGetRecordsModel()
                            {
                                from = firstOfMonth,
                                to = lastOfMonth,
                                id_empl = (long)emp.BBId,
                                user_token = cust.BBToken
                            }).Item1;
                            if (records.records != null)
                                foreach (Record rec in records.records)
                                {
                                    hours += int.Parse(rec.total_hours);
                                }
                            records.hours = hours / 60;
                            response.hours = hours / 60;
                        }
                        else
                        {
                            exmsg = "Nessuna missione attiva per la data selezionata";
                        }
                    }
                    else
                    {
                        exmsg = "Nessuna missione attiva per la data selezionata";
                    }
                }
                else
                {
                    exmsg = "La compagnia non è esistente o non è stata confermata";
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }
            return getJsonResult(exmsg, response);
        }

        public JsonResult deleteRecord(EBtoBBRecordModel model)
        {
            string exmsg = string.Empty;
            BadgeBoxDeleteRecordResponse response = null;
            BadgeBoxGetRecordResponse records = null;
            int hours = 0;
            DateTime target = new DateTime(model.checkin);
            int daysInMonth = DateTime.DaysInMonth(target.Year, target.Month);
            string firstOfMonth = new DateTime(target.Year, target.Month, 1, 0, 0, 0).ToString("yyyy-MM-dd HH:mm:ss");
            string lastOfMonth = new DateTime(target.Year, target.Month, daysInMonth, 23, 59, 59).ToString("yyyy-MM-dd HH:mm:ss");
            Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
            Company comp = getDBHandler().getCompanyByEmployeeCode(emp.employeeCode).Item1;
            Customer cust = getDBHandler().getOwner((long)comp.ownerId).Item1;
            if (ModelState.IsValid)
            {
                response = BadgeBoxServer.deleteRecord(new BadgeBoxDeleteRecordModel()
                {
                    id = model.id_record,
                    id_empl = (long)emp.BBId,
                    user_token = cust.BBToken
                }).Item1;
                records = BadgeBoxServer.getRecords(new BadgeBoxGetRecordsModel()
                {
                    from = firstOfMonth,
                    to = lastOfMonth,
                    id_empl = (long)emp.BBId,
                    user_token = cust.BBToken
                }).Item1;
                if (records.records != null)
                    foreach (Record rec in records.records)
                    {
                        hours += int.Parse(rec.total_hours);
                    }
                records.hours = hours / 60;
                response.hours = hours / 60;
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }
            return getJsonResult(exmsg, response);
        }

        public JsonResult autoFill(EBtoBBRecordModel model)
        {
            string exmsg = string.Empty;
            if (ModelState.IsValid)
            {
                Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
                Administration admin = getDBHandler().getAdministrationByCompany(getDBHandler().getCompanyByEmployeeCode(emp.employeeCode).Item1.companyCode).Item1;
                int hours = 0;
                if ((bool)admin.fullTime)
                {
                    hours = 8;
                }
                else
                {
                    hours = (int) admin.partTimeHoursNumber;
                }
                for (int i = 1; i <= DateTime.DaysInMonth(model.year, model.month); i++)
                {
                    DateTime day = new DateTime(model.year, model.month, i);
                    if (day.DayOfWeek != DayOfWeek.Saturday && day.DayOfWeek != DayOfWeek.Sunday)
                    {
                        long checkin = day.AddHours(9).Ticks;
                        createRecord(new EBtoBBRecordModel()
                        {
                            id_empl = model.id_empl,
                            checkin = checkin,
                            hours = hours
                        });
                    }
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values);
            }

            return getJsonResult(exmsg, getDateRecords(new EBtoBBRecordModel() { id_empl = (int)model.id_empl, year = DateTime.Now.Year, month = DateTime.Now.Month }));
        }
        #endregion
    }
}