﻿using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RestAuthorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class OperatorController : BaseController
    {
        public JsonResult getCompanyDetail(GetCompanyDetailModel model)
        {
            string exmsg = string.Empty;
            GetCompanyDetailResponseModel response = null;

            if (ModelState.IsValid)
            {
                Tuple<GetCompanyDetailResponseModel, string> result = getDBHandler().getCompanyDetail(model.companyId);

                if (string.IsNullOrEmpty(result.Item2))
                    response = result.Item1;
                else
                    exmsg = result.Item2;
            }
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());


            return getJsonResult(exmsg, response);
        }

        public JsonResult getCustomers()
        {
            List<CustomerModel> response = null;

            Tuple<List<CustomerModel>, string> result = getDBHandler().getCustomers();

            if (string.IsNullOrEmpty(result.Item2) && result.Item1 != null)
                response = result.Item1;

            return getJsonResult(result.Item2, response);
        }
    }
}