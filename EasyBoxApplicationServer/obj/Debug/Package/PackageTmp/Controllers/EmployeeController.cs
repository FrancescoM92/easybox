﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.BadgeBox.Models;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RemoteServers;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Services;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class EmployeeController : BaseController
    {
        public string insertEmployee(EmployeeModel model)
        {
            string exmsg = string.Empty;

            /* encrypt password if available */
            if (!string.IsNullOrEmpty(model.password))
                model.password = MyHelper.GetEncryptedPassword(model.password);

            /* insert into EB */
            Tuple<long, string> result = getDBHandler().insertEmployee(model);

            if (string.IsNullOrEmpty(result.Item2) && result.Item1 != 0)
            {
                /* inserto into BB */
                Tuple<BadgeBoxCreateUserResponse, string> resultBB = BadgeBoxServer.createUser(BadgeBoxUserModel.Create(model));

                /* update EB with bbToken*/
                if (string.IsNullOrEmpty(resultBB.Item2))
                    exmsg = getDBHandler().setEmployeeBBSync(result.Item1, resultBB.Item1.token, resultBB.Item1.id);
                else
                    exmsg = result.Item2;
            }
            else
                exmsg = result.Item2;

            return exmsg;
        }

        public JsonResult activateEmployee(long id)
        {
            string exmsg = string.Empty;

            if (id > 0)
            {
                exmsg = getDBHandler().setEmployeeIsActive(id, true);
                return getJsonResult(exmsg, "OK");
            }
            else
            {
                return getJsonResult(exmsg, "KO");
            }
        }

        public JsonResult deactivateEmployee(long id)
        {
            string exmsg = string.Empty;

            if (id > 0)
            {
                exmsg = getDBHandler().setEmployeeIsActive(id, false);
                if (string.IsNullOrEmpty(exmsg))
                    return getJsonResult(exmsg, "OK");
                else
                    return getJsonResult(exmsg, "KO");
            }
            else
            {
                return getJsonResult(exmsg, "KO");
            }
        }

        public JsonResult updateEmployeePassword(ResetPasswordModel model, bool isEasy = false)
        {
            string result = string.Empty;
            try
            {
                Employee emp = getDBHandler().getEmployee(model.id).Item1;
                if (MyHelper.GetEncryptedPassword(model.oldPassword).Equals(emp.password))
                {
                    result = getDBHandler().updateUserPasswordCustomer(emp.email, model.newPassword);
                    if (string.IsNullOrEmpty(result))
                    {
                        EmailSender.sendEmail(emp.email, "Cambio Password", "Complimenti, la sua password è stata cambiata con successo, ora può accedere all'area riservata", isEasy);
                        return getJsonResult(null, "OK");
                    }
                }
                else
                {
                    return getJsonResult(null, "Password errata");
                }
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return getJsonResult(result, null);
        }

        public JsonResult getEmployeeDetail(IdRequest model)
        {
            string exmsg = string.Empty;
            Tuple<GetEmployeeDetailResponseModel, string> result = null;
            if (ModelState.IsValid)
            {
                result = getDBHandler().getEmployeeDetail(model.id);
            }
            else exmsg = getErrorMessage(ModelState.Values.ToList());

            return getJsonResult(exmsg, result.Item1);
        }

        public JsonResult getEmployeesProfilePhoto(IdsRequest model)
        {
            string exmsg = string.Empty;
            string image = string.Empty;
            List<Tuple<string, long>> response = new List<Tuple<string, long>>();
            if (ModelState.IsValid)
            {
                foreach(int i in model.ids)
                {
                    Media media = new DBHandler().getMediaByEmployeeId(i).Item1;
                    string base64image = string.Empty;
                    if (media != null) base64image = Convert.ToBase64String(media.data);
                    response.Add(new Tuple<string, long>(base64image, i));
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }
            return getJsonResult(exmsg, response);
        }

        public JsonResult getEmployeeDetailChart(ChartModel model)
        {
            Tuple<ChartResponse, string> response = getDBHandler().getEmployeeWorkedHours(model.year, model.id);
            return getJsonResult(response.Item2, response.Item1);
        }
    }
}