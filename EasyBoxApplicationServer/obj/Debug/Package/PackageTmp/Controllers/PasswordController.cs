﻿using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Services;
using EasyBoxApplicationServer.Utils;
using System;
using System.Linq;
using System.Web.Mvc;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class PasswordController : BaseController
    {
        public JsonResult resetPassword(ResetPasswordModel model)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
            {
                if (model.newPassword.Equals(model.newPassword))
                {
                    byte[] data = Convert.FromBase64String(model.token);
                    DateTime when = DateTime.FromBinary(BitConverter.ToInt64(data, 0));
                    if (when >= DateTime.UtcNow.AddHours(-24))
                    {
                        UserTypeEnum userType = (UserTypeEnum)Enum.Parse(typeof(UserTypeEnum), model.userType);

                        switch (userType)
                        {
                            case UserTypeEnum.Customer:
                                exmsg = getDBHandler().updateUserPasswordCustomer(model.email, model.newPassword);
                                break;
                            case UserTypeEnum.Employee:
                                exmsg = getDBHandler().updateUserPasswordEmployee(model.email, model.newPassword);
                                break;
                            default:
                                break;
                        }
                    }
                    else
                        exmsg = MyHelper.GetLocalizedString("OldTokenErrorMessage");
                }
                else
                    exmsg = MyHelper.GetLocalizedString("resetPasswordError");
            }
            else
                exmsg = getErrorMessage(ModelState.Values);

            return getJsonResult(exmsg);
        }

        public JsonResult sendNewPasswordEmail(SendNewPasswordEmailModel model, string absoluteUri, bool isEasy = false)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
            {
                byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
                byte[] key = Guid.NewGuid().ToByteArray();
                string token = absoluteUri + "Password?email=" + model.email + "&token=" + Convert.ToBase64String(time.Concat(key).ToArray());
                System.Diagnostics.Debug.WriteLine("token:" + token);

                EmailSender.sendEmail(model.email, MyHelper.GetLocalizedString("ResetPasswordEmailSubject"), token, isEasy);
            }
            else
                exmsg = getErrorMessage(ModelState.Values);

            return getJsonResult(exmsg);
        }
    }
}