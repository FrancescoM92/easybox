﻿using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    public class LoginController : BaseController
    {
        [RESTAuthorize]
        [HttpPost]
        public JsonResult Login(TokenModel model)
        {
            if (ModelState.IsValid)
                if (!string.IsNullOrEmpty(model.azure))
                {
                    return getJsonResult(null, JWTDecoder.decodePayload(model.azure).Value<string>("name"));
                }
                else
                {
                    return getJsonResult(null, "ok");
                }
            else
                return getJsonResult("ko", null);
        }

        [RESTAuthorize]
        [HttpPost]
        public JsonResult LoginAgraria(TokenModel model)
        {
            JsonResult result = getJsonResult("ko", null);
            if (ModelState.IsValid)
                if (!string.IsNullOrEmpty(model.azure))
                {
                    List<string> bus = MyHelper.getUserBusinessUnits(model.azure);
                    foreach (string x in bus)
                    {
                        if (x.Equals(Constants.AZURE_BU_AGRICOLTURA, StringComparison.InvariantCultureIgnoreCase))
                        {
                            result = getJsonResult(null, JWTDecoder.decodePayload(model.azure).Value<string>("name"));
                            break;
                        }
                        else
                            result = getJsonResult("ko", null);
                    }
                }
                else
                {
                    PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
                    bool isActive = getDBHandler().isActive(person);
                    bool isAgri = getDBHandler().isAgri(person);
                    if(isAgri)
                        return getJsonResult(null, "ok");
                    else
                        return getJsonResult("ko", null);
                }
            return result;
        }

        public JsonResult getGroups(TokenModel model)
        {
            return getJsonResult(null,MyHelper.getUserBusinessUnits(model.azure));
        }

        public string getToken(string code)
        {
            string exmsg = string.Empty;
            JToken jsonResult = string.Empty;
            try
            {
                var parameters = new Dictionary<string, string>()
                {
                    { "grant_type", ConfigurationManager.AppSettings.Get("ida:GrantType") },
                    { "client_id", ConfigurationManager.AppSettings.Get("ida:ClientID") },
                    { "code", code },
                    { "redirect_uri", ConfigurationManager.AppSettings.Get("ida:RedirectUri") },
                    { "resource", ConfigurationManager.AppSettings.Get("ida:Resource") },
                    { "client_secret", ConfigurationManager.AppSettings.Get("ida:Secret") }
                };
                var content = new FormUrlEncodedContent(parameters);
                var JWT = Utils.Request.AzurePostRequest(Constants.AZURE_EB_ENDPOINT_TOKEN, content);
                jsonResult = MyHelper.InjectGroupApartenance(JWT.Result);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            Response.ContentType = "application/json";
            return jsonResult.ToString();
        }

        public string getTokenForAgri(string code)
        {
            string exmsg = string.Empty;
            JToken jsonResult = string.Empty;
            try
            {
                var parameters = new Dictionary<string, string>()
                {
                    { "grant_type", ConfigurationManager.AppSettings.Get("ida:GrantType") },
                    { "client_id", ConfigurationManager.AppSettings.Get("ida:ClientID") },
                    { "code", code },
                    { "redirect_uri", ConfigurationManager.AppSettings.Get("ida:RedirectUriAgri") },
                    { "resource", ConfigurationManager.AppSettings.Get("ida:Resource") },
                    { "client_secret", ConfigurationManager.AppSettings.Get("ida:Secret") }
                };
                var content = new FormUrlEncodedContent(parameters);
                var JWT = Utils.Request.AzurePostRequest(Constants.AZURE_EB_ENDPOINT_TOKEN, content);
                jsonResult = MyHelper.InjectGroupApartenance(JWT.Result);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            Response.ContentType = "application/json";
            return jsonResult.ToString();
        }

        public JsonResult refreshToken(string refreshToken)
        {
            var parameters = new Dictionary<string, string>
            {
                { "client_id", ConfigurationManager.AppSettings.Get("ida:ClientID") },
                { "grant_type","refresh_token" },
                { "refresh_token", refreshToken },
                { "resource", ConfigurationManager.AppSettings.Get("ida:Resource") },
                { "client_secret", "ZsIRy1ulnF3Oq4R4xLCx3QJLvqFA+RLtN3mZNswFhuk=" }
            };
            var content = new FormUrlEncodedContent(parameters);
            var res = Utils.Request.AzurePostRequest(Constants.AZURE_EB_ENDPOINT_TOKEN, content);
            return getJsonResult(null, res);
        }
    }

    [RESTAuthorize]
    public class OperatorLoginController : BaseController
    {
        [HttpGet]
        public JsonResult Login()
        {
            return getJsonResult(null, "ok", true);
        }
    }

    #region getIP
    public class IpController : Controller
    {
        [HttpGet]
        public JsonResult Index()
        {
            return Json(CommonManager.GetIP(Request), JsonRequestBehavior.AllowGet);
        }
    }
    #endregion



}