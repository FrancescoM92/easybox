﻿app.config(function ($routeProvider) {
    $routeProvider
        .when("/login", {
            templateUrl: "Views/Home/Index.cshtml",
            controller: ""
        })
        .when("/operator", {
            templateUrl: "Views/Operator/Index.cshtml",
            controller: "OperatorController"
        })
        .when("/owner", {
            templateUrl: "Views/Operator/Owners.cshtml",
            controller: "OwnerController"
        })
        .otherwise({
            redirectTo: "/login"
        });
});