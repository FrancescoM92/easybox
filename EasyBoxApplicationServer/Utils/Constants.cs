﻿namespace EasyBoxApplicationServer.Utils
{
    public class Constants
    {
        #region BadgeBox
        public static string BADGEBOX_BASE_URL = "https://ojm.badgebox.com/server/version_rc4_0/";
        public static string BADGEBOX_APIKEY = "asfbbfwbeilrhiudsaknfaksdljhf4hrj4r9w";
        public static string BADGEBOX_USER_PSW = "5hKYTEkzsFBfdX5q2tymZ6WabphdrPaG";
        public static string TIMEZONE_IT = "Europe/Rome";
        #endregion

        public static string EASYBOX_APIKEY = "9f7Y4QGXtAM0dqiPjiuRyb3szP22gF11";
        public static string OK = "ok";

        public static int MAXDIMFILE = 2097152;

        #region AWDoc
        public static string AWDOC_URI = "http://gds.awdoc.com";
        public static string AWDOC_ORGANIZATION = "OpenjobStaging";
        public static string AWDOC_USERNAME = "sa@openjob.it";
        public static string AWDOC_PASSWORD = "sa1@openjob.it";
        public static string AWDOC_DOMAIN = "OpenjobStaging";
        public static string AWDOC_EASYBOXCATEGORYUUID = "32a61325-dd63-484a-8798-fbb85b450381";
        public static string AWDOC_PHONE_TEST_MICHELE = "+393496747501";
        public static string AWDOC_PHONE_TEST_FRANCESCO = "+393405068304";
        #endregion

        #region AWDoc Release
        public static string AWDOC_URI_RELEASE = "http://gds.awdoc.com";
        public static string AWDOC_ORGANIZATION_RELEASE = "Openjobmetis";
        public static string AWDOC_USERNAME_RELEASE = "webapp_domain@openjob.it";
        public static string AWDOC_PASSWORD_RELEASE = "-zXB#82$BU3waX_4";
        public static string AWDOC_DOMAIN_RELEASE = "Commerciale";
        public static string AWDOC_EASYBOXCATEGORYUUID_ADMINISTRATION_RELEASE = "23c41acf-76a9-4176-b020-a315ea173541";
        public static string AWDOC_EASYBOXCATEGORYUUID_ENGAGEMENT_RELEASE = "d72e180a-b67e-45ae-808c-8b99c5b68ab3";
        #endregion

        #region AZURE
        public static string AZURE_EB_ENDPOINT_TOKEN = "https://login.microsoftonline.com/7365cbd5-a5fa-4e34-89d9-a0b3470b2266/oauth2/token";
        public static string AZURE_EB_ENDPOINT_V2_TOKEN = "https://login.microsoftonline.com/7365cbd5-a5fa-4e34-89d9-a0b3470b2266/oauth2/v2.0/token";

        public static string AZURE_BU_AGRICOLTURA = "EBAgricoltura";
        public static string AZURE_BU_DIVERSITY_TALENT = "EBDiversity";
        public static string AZURE_BU_FAMILY_CARE = "EBFamilyCare";
        public static string AZURE_BU_GDO = "EBGDO";
        public static string AZURE_BU_HORECA = "EBHoreca";
        public static string AZURE_BU_ICT = "EBICT";
        public static string AZURE_BU_INDUSTRIALE = "EBIndustriale";
        public static string AZURE_BU_NAVALE = "EBNavale";
        public static string AZURE_BU_SANITA = "EBSanita";
        #endregion

        public static string DIVISIONE_AGRARIA = "AGR";

        #region ROLE IDs
        public static int BB_ROLE_ADMINISTRATOR = 1;
        public static int BB_ROLE_AGENT = 2;
        public static int BB_ROLE_HR = 3;
        #endregion

        public enum UserTypeEnum
        {
            Operator = 0,
            Customer = 2,
            Employee = 3,
            Owner = 4
        }
        public enum ChartEnum
        {
            Gennaio = 1,
            Febbraio = 2,
            Marzo = 3,
            Aprile = 4,
            Maggio = 5,
            Giugno = 6,
            Luglio = 7,
            Agosto = 8,
            Settembre = 9,
            Ottobre = 10,
            Novembre = 11,
            Dicembre = 12,
        }
        public enum DocumentCategoryEnum
        {
            Standard,
            AgriJob,
            DataBook,
            ShakeJob,
            EasyBox,
            Internal
        }
    }
}