﻿using EasyBoxApplicationServer.Utils;
using System;
using System.Web;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.RestAuthorization
{
    public class RESTAuthorizeAttribute : AuthorizeAttribute
    {
        private const string _securityToken = "token"; // Name of the url parameter.
        private const string _json = "azure"; // azure token.


        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (Authorize(filterContext))
            {
                return;
            }
            HandleUnauthorizedRequest(filterContext); 
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
            if (filterContext.HttpContext.Request.CurrentExecutionFilePath.Contains("Login") || filterContext.HttpContext.Request.CurrentExecutionFilePath.Contains("login"))
            {
                filterContext.Result = new HttpUnauthorizedResult("Username o Password errati");
            }
        }

        private bool Authorize(AuthorizationContext actionContext)
        {
            try
            {
                HttpRequestBase request = actionContext.RequestContext.HttpContext.Request;
                string token = request.Params[_securityToken];
                string json = request.Params[_json];

                if (string.IsNullOrEmpty(token)) return ValidateToken.IsAzureTokenValid(json, CommonManager.GetIP(request));

                return ValidateToken.IsTokenValid(token, CommonManager.GetIP(request), request.UserAgent);
            }
            catch (Exception e)
            {
                MyHelper.Log(new string[] { e.Message });
                return false;
            }
        }
    }
}