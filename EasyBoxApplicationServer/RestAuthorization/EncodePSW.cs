﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Utils;
using System;
using System.Security.Cryptography;
using System.Text;

namespace EasyBoxApplicationServer.RestAuthorization
{
    public class EncodePSW
    {
        private const string _alg = "HmacSHA256";
        public const string _salt = "881jCWes0rhVUwPRw0Y7";
        public static string GenerateToken(string username, string ip, string userAgent, long ticks)
        {
            string hash = string.Join(":", new string[] { username, ip, ticks.ToString() });
            string hashLeft = string.Empty;
            string hashRight = string.Empty;
            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(username));
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));
                hashLeft = Convert.ToBase64String(hmac.Hash);
                hashRight = string.Join(":", new string[] { username, ticks.ToString() });
            }
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));
        }

        public static string GetHashedPassword(string username)
        {
            DBHandler db = new DBHandler();
            string pwd = db.getUserPassword(username);
            return pwd;
        }
    }
}