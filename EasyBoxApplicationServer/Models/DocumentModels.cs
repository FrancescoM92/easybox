﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using static AWTech.AWDoc.Protocolbuffer.Document.Types;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Models
{
    public class DocumentModel
    {
        public static DocumentModel Create(Document document)
        {
            HttpPostedFileBase result = null;
            if(document.documento != null) result = new MemoryPostedFile(document.documento, document.nome);
            Tuple<string, int> sizz = MyHelper.getCurrentSize(document.documento.Length);
            return new DocumentModel()
            {
                companyId = document.companyId,
                employeeId = document.employeeId,
                fileName = document.nome.Split('.')[0],
                id = document.ID,
                category = document.category,
                extension = document.nome.Split('.')[1],
                size = sizz.Item1+ " " +sizz.Item2
            };
        }
        public HttpPostedFileBase data { get; set; }
        public long id { get; set; }
        public string fileName { get; set; }
        public long? employeeId { get; set; }
        public long? companyId { get; set; }
        public string category { get; set; }
        public string extension { get; set; }
        public string size { get; set; }
        public Guid Guid { get; set; }
    }

    public class AWDocumentInter
    {
        public static AWDocumentInter Create(AWTech.AWDoc.Protocolbuffer.Document document)
        {
            long from = 0;
            long to = long.MaxValue;

            if (document.ValidFrom > 0)
            {
                from = Math.Abs(document.ValidFrom);
            }
            if(document.ValidTo > 0)
            {
                to = Math.Abs(document.ValidTo);
            }
            bool isSigned = false;
            Tuple<string, int> tupla = MyHelper.getCurrentSize((long)document.Size);
            if (document.SignatureState == SignatureState.Signed) isSigned = true;
            string dateFrom = string.Empty;
            //if (from.ToString().Length >= 13 ) dateFrom = MyHelper.GetDateTimeFromTimeStamp(from / 1000).ToString();
            string dateTo = string.Empty;
            //if (document.ValidTo > 0) dateTo = MyHelper.GetDateTimeFromTimeStamp(document.ValidTo / 1000).ToString();
            return new AWDocumentInter()
            {
                UUID = document.Uuid.Replace("{","").Replace("}",""),
                category = DocumentCategoryEnum.Standard,
                dateFrom = dateFrom,
                dateTo = dateTo,
                extension = document.MimeType,
                name = document.Title,
                signed = isSigned,
                size = tupla.Item2 + " " + tupla.Item1,
                state = document.SignatureState,
                signState = true
            };
        }
        public static AWDocumentInter Create(AWTech.AWDoc.Protocolbuffer.Document document, AWDocument awdoc)
        {
            long from = 0;
            long to = long.MaxValue;

            if (document.ValidFrom > 0)
            {
                from = Math.Abs(document.ValidFrom);
            }
            if (document.ValidTo > 0)
            {
                to = Math.Abs(document.ValidTo);
            }
            bool isSigned = false;
            Tuple<string, int> tupla = MyHelper.getCurrentSize((long)document.Size);
            if (document.SignatureState == SignatureState.Signed) isSigned = true;
            string dateFrom = string.Empty;
            //if (from.ToString().Length >= 13 ) dateFrom = MyHelper.GetDateTimeFromTimeStamp(from / 1000).ToString();
            string dateTo = string.Empty;
            //if (document.ValidTo > 0) dateTo = MyHelper.GetDateTimeFromTimeStamp(document.ValidTo / 1000).ToString();
            return new AWDocumentInter()
            {
                UUID = document.Uuid.Replace("{", "").Replace("}", ""),
                category = DocumentCategoryEnum.Standard,
                dateFrom = dateFrom,
                dateTo = dateTo,
                extension = document.MimeType,
                name = document.Title,
                signed = isSigned,
                size = tupla.Item2 + " " + tupla.Item1,
                state = document.SignatureState,
                signState = awdoc.signing
            };
        }
        public string UUID { get; set; }
        public string name { get; set; }
        public string extension { get; set; }
        public string size { get; set; }
        public bool? signed { get; set; }
        public bool signState { get; set; }
        public SignatureState state { get; set; }
        public string dateFrom { get; set; }
        public string dateTo { get; set; }
        public DocumentCategoryEnum category { get; set; }
    }
    
    public class AWDocumentsModel
    {
        public List<AWDocumentInter> documents { get; set; }
        public int year { get; set; }
        public int month { get; set; }
    }

    public class DocumentsModel
    {
        public List<DocumentModel> documents { get; set; }
    }

    public class FileModel
    {
        public static FileModel Create(Document doc)
        {
            HttpPostedFileBase result = null;
            if (doc.documento != null) result = new MemoryPostedFile(doc.documento, doc.nome);
            return new FileModel()
            {
                file = result
            };
        }
        public HttpPostedFileBase file { get; set; }
    }

    public class getDocumentsModel
    {
        public long? employeeId { get; set; }
        public long? companyId { get; set; }
        public string token { get; set; }
    }

    public class DeleteDocumentModel
    {
        public long id { get; set; }
    }

    public class MemoryPostedFile : HttpPostedFileBase
    {
        private readonly byte[] fileBytes;

        public MemoryPostedFile(byte[] fileBytes, string fileName = null)
        {
            this.fileBytes = fileBytes;
            this.FileName = fileName;
            this.InputStream = new MemoryStream(fileBytes);
        }

        public override int ContentLength => fileBytes.Length;

        public override string FileName { get; }

        public override Stream InputStream { get; }
    }

    public class SigningModel
    {
        public int initial { get; set; }
        public int signing { get; set; }
        public string token { get; set; }
        public string azure { get; set; }
    }
}