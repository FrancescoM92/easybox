﻿namespace EasyBoxApplicationServer.Models
{
    public class ResetPasswordModel
    {
        public long id { get; set; }

        public string newPassword { get; set; }

        public string oldPassword { get; set; }

        public string token { get; set; }

        public string userType { get; set; }

        public string email { get; set; }
    }

    public class SendNewPasswordEmailModel
    {
        public string email { get; set; }
    }
}