﻿using EasyBoxApplicationServer.Models.ServiceBus;
using System.Collections.Generic;
using EasyBoxApplicationServer.Database;

namespace EasyBoxApplicationServer.Models
{
    public class BranchModel
    {
        public long id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string fax { get; set; }
        public Address address { get; set; }
        public long IDFiliale { get; set; }
        public string phone { get; set; }
    }

    public class GetBranchDetailModel
    {
        public long branchId { get; set; }
    }
}