﻿using System;

namespace EasyBoxApplicationServer.Models
{
    public class MissionModel
    {
        public string companyCode { get; set; }

        public string employeeEmail { get; set; }

        public DateTime endDate { get; set; }

        public string matriculationNumber { get; set; }

        public string  name { get; set; }

        public DateTime startDate { get; set; }
    }
}