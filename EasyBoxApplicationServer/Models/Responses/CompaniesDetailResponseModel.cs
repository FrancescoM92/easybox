﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.ServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EasyBoxApplicationServer.Models.Responses
{
    public class GetCompaniesResponseModel
    {
        public int companiesNum { get; set; }

        public List<CompanySummaryResponseModel> companies { get; set; }
    }

    public class CompanySummaryResponseModelBase
    {
        public long id { get; set; }

        public string companyCode { get; set; }

        public string endDate { get; set; }

        public bool isTemporary { get; set; }

        public string address { get; set; }

        public int countNewMessages { get; set; }

        public string name { get; set; }

        public string startDate { get; set; }

        public string logo { get; set; }

        public int? BBCompanyId { get; set; }
    }

    public class CompanySummaryResponseModel : CompanySummaryResponseModelBase
    {
        public static CompanySummaryResponseModel Create(Company company, Customer customer)
        {
            int countMess = new DBHandler().getCompanyNewMessagesCount(Utils.Constants.UserTypeEnum.Operator, company.ID).Item1;
            CustomerModel owner = null;

            if (customer != null)
                owner = new CustomerModel()
                {
                    name = customer.name + " " + customer.lastName,
                    BBId = (int?) customer.BBId                    
                };

            DateTime now = DateTime.Now;
            Administration provstart = new DBHandler().getAdministrationByCompanyID(company.ID).Item1;
            string administrationStartTime = string.Empty;
            string administrationEndTime = string.Empty;
            List<Extension> admExts = null;
            if (provstart != null)
            {
                admExts = new DBHandler().getExtensionsByAdministration(provstart.ID).Item1;
            }
            Address address = new DBHandler().getAddressByID(company.addressId).Item1;
            string addr = string.Empty;
            if (address != null)
                addr = address.street + ", " + address.city;
            if (provstart == null)
            {
                administrationStartTime = "Nessun Contratto";
                administrationEndTime = "Nessun Contratto";
            }
            else if (admExts.Count == 0)
            {
                administrationStartTime = provstart.provisionStartDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                administrationEndTime = provstart.provisionEndDate.GetValueOrDefault().ToString("dd/MM/yyyy");
            }
            else
            {
                administrationStartTime = provstart.provisionStartDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                DateTime end = (DateTime)provstart.provisionEndDate;
                foreach (Extension ext in admExts)
                {
                    if (ext.endDate != null && ext.endDate > end)
                    {
                        end = (DateTime)ext.endDate;
                    }
                }
                administrationEndTime = end.ToString("dd/MM/yyyy");
            }
            return new CompanySummaryResponseModel()
            {
                id = company.ID,
                name = company.businessName,
                companyCode = company.companyCode,
                isTemporary = false,
                address = addr,
                startDate = administrationStartTime,
                endDate = administrationEndTime,
                owner = owner,
                countNewMessages = countMess,
                BBCompanyId = (int?) company.BBId
            };
        }

        //public static CompanySummaryResponseModel Create(Company company)
        //{
        //    CompanyCustomerModel owner = null;
        //    DateTime now = DateTime.Now;
        //    CompanyContract cc = company.CompanyContract.FirstOrDefault(x => x.startDate <= now && x.endDate >= now);

        //    return new CompanySummaryResponseModel()
        //    {
        //        id = company.ID,
        //        name = company.name,
        //        companyCode = company.companyCode,
        //        isTemporary = false,
        //        startDate = MyHelper.GetJavscriptMicrosFromTicks(cc.startDate.Value.Ticks),
        //        endDate = MyHelper.GetJavscriptMicrosFromTicks(cc.endDate.Value.Ticks),
        //        owner = owner
        //    };
        //}

        public CustomerModel owner { get; set; }
    }

    public class TemporaryCompanySummaryResponseModel : CompanySummaryResponseModelBase
    {
        public static TemporaryCompanySummaryResponseModel Create(Company company)
        {
            Administration provstart = new DBHandler().getAdministrationByCompanyID(company.ID).Item1;
            string administrationStartTime = string.Empty;
            string administrationEndTime = string.Empty;
            List<Extension> admExts = null;
            if (provstart != null)
            {
                admExts = new DBHandler().getExtensionsByAdministration(provstart.ID).Item1;
            }
            Address address = new DBHandler().getAddressByID(company.addressId).Item1;
            string addr = string.Empty;
            if (address != null)
                addr = address.street + ", " + address.city;

            if (provstart == null)
            {
                administrationStartTime = "Nessun Contratto";
                administrationEndTime = "Nessun Contratto";
            }
            else if (admExts.Count == 0)
            {
                administrationStartTime = provstart.provisionStartDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                administrationEndTime = provstart.provisionEndDate.GetValueOrDefault().ToString("dd/MM/yyyy");
            }
            else
            {
                administrationStartTime = provstart.provisionStartDate.GetValueOrDefault().ToString("dd/MM/yyyy");
                DateTime end = (DateTime)provstart.provisionEndDate;
                foreach (Extension ext in admExts)
                {
                    if (ext.endDate != null && ext.endDate > end)
                    {
                        end = (DateTime)ext.endDate;
                    }
                }
                administrationEndTime = end.ToString("dd/MM/yyyy");
            }
            return new TemporaryCompanySummaryResponseModel
            {
                companyCode = company.companyCode,
                startDate = administrationStartTime,
                endDate = administrationEndTime,
                address = addr,
                id = company.ID,
                isTemporary = (bool)company.isTemporary,
                name = company.businessName
            };
        }
    }

    public class GetCompanyDetailResponseModel : CompanySummaryResponseModel
    {
        public static GetCompanyDetailResponseModel Create(Company company, Administration administration, Customer owner, List<Customer> customers, List<Employee> employees)
        {
            List<CustomerModel> customersList = new List<CustomerModel>();
            if (customers != null && customers.Count > 0)
            {
                foreach (var customer in customers)
                {
                    Service custServ = new DBHandler().getService((long)customer.serviceId).Item1;
                    customersList.Add(new CustomerModel()
                    {
                        id = customer.ID,
                        name = customer.name,
                        lastName = customer.lastName,
                        email = customer.email,
                        firma = custServ.firma,
                        rapportini = custServ.rapportini,
                        documenti = custServ.documenti
                    });
                }
            }

            List<EmployeeModel> employeesList = new List<EmployeeModel>();
            if (employees != null && employees.Count > 0)
            {
                foreach (var employee in employees)
                {
                    employeesList.Add(EmployeeModel.Create(employee));
                }
            }

            return new GetCompanyDetailResponseModel()
            {
                id = company.ID,
                name = company.businessName,
                companyCode = company.companyCode,
                email = company.email,
                vat = company.vat,
                isTemporary = false,
                owner = CustomerModel.Create(owner),
                service = new DBHandler().getService((long)company.serviceId).Item1,
                address = company.Address.street,
                employees = employeesList,
                customers = customersList,
                startDate = administration.provisionStartDate.ToString().Split(' ')[0],
                endDate = administration.provisionEndDate.ToString().Split(' ')[0],
                BBCompanyId = (int?) company.BBId
            };
        }

        public List<CustomerModel> customers { get; set; }

        public List<EmployeeModel> employees { get; set; }

        public Service service { get; set; }

        public string email { get; set; }

        public List<Function> functions { get; set; }

        public string vat { get; set; }

        public int? BBCompanyId { get; set; }

        public List<AWDocument> documents { get; set; }
    }

    public class CompanyCustomerModel
    {
        public int id { get; set; }

        public string email { get; set; }

        public string name { get; set; }

        public string lastName { get; set; }
    }

    public class Function
    {
        public string name { get; set; }

        public int id { get; set; }
    }
}