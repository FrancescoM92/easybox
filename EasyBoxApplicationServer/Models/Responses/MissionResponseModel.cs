﻿using EasyBoxApplicationServer.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.Responses
{
    public class MissionResponseModel
    {
        //public static MissionResponseModel Create(Mission mission)
        //{
        //    return new MissionResponseModel()
        //    {
        //        name = mission.name,
        //        startDate = (DateTime)mission.startDate,
        //        endDate = (DateTime)mission.endDate
        //    };
        //}

        public string name { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }

    public class MissionsResponseModel
    {
        public List<MissionResponseModel> missions { get; set; }
    }
}