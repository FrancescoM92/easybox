﻿using EasyBoxApplicationServer.Database;
using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.Responses
{
    public class GetBranchesResponseModel
    {
        public int branchesNum { get; set; }

        public List<BranchResponseModel> branches { get; set; }
    }

    public class BranchResponseModel
    {
        public static BranchResponseModel Create(UnitaDiBusiness branch, int employeesNum)
        {
            Address address = new DBHandler().getAddressByID((long)branch.IDAddress).Item1;
            return new BranchResponseModel()
            {
                id = branch.ID,
                division = branch.Divisione,
                name = branch.Filiale,
                address = address != null ? address.street + ", " + address.city : "",
                phone = branch.Telefono,
                employeeCount = employeesNum
            };
        }

        public long id { get; set; }

        public string division { get; set; }

        public string address { get; set; }

        public int employeeCount { get; set; }

        public string name { get; set; }

        public string phone { get; set; }
    }

    public class GetBranchDetailResponseModel
    {
        
        public static GetBranchDetailResponseModel Create(UnitaDiBusiness branch, Address address, List<Employee> employeesList)
        {
            
            return new GetBranchDetailResponseModel()
            {
                
                branch = new BranchResponseModel()
                {
                    id = branch.ID,
                    address = address.street,
                    name = branch.Filiale,
                    division = branch.Divisione,
                    employeeCount = employeesList.Count
                },
                employees = GetEmployeesResponseModel.Create(employeesList, branch.Filiale)
            };
        }

        public BranchResponseModel branch { get; set; }

        public GetEmployeesResponseModel employees { get; set; }
    }
}