﻿using System;

namespace EasyBoxApplicationServer.Models.BadgeBox.Requests
{
    public class BadgeBoxGetTimeSheetModel : BadgeBoxBaseModel
    {
        public long id_empl { get; set; }
        public string from { get; set; }
        public string to { get; set; }
    }

    public class BadgeBoxCreateTimeSheetModel : BadgeBoxBaseModel
    {
        public long from_user { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public int month { get; set; }
        public int year { get; set; }
    }

    public class BadgeBoxGetOverallTimeSheetModel : BadgeBoxBaseModel
    {
        public DateTime from { get; set; }
        public DateTime to { get; set; }
    }
    public class BadgeBoxDownloadTimesheetModel : BadgeBoxBaseModel
    {
        public long id { get; set; }
        public int type { get; set; } = 0;
    }
}