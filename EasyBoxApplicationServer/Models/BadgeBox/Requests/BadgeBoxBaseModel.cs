﻿using EasyBoxApplicationServer.Utils;

namespace EasyBoxApplicationServer.Models
{
    public class BadgeBoxBaseModel
    {
        public BadgeBoxBaseModel()
        {
            key = Constants.BADGEBOX_APIKEY;
        }

        public int id_agency { get; set; }
        public string key { get; set; }
        public string user_token { get; set; }
    }
}