﻿
namespace EasyBoxApplicationServer.Models.BadgeBox.Models
{
    public class BadgeBoxUserModel : BadgeBoxBaseModel
    {
        public BadgeBoxUserModel() : base()
        { }

        public static BadgeBoxUserModel Create(PersonModelBase model)
        {
            return new BadgeBoxUserModel()
            {
                name = model.name,
                surname = model.lastName,
                email = model.email,
                password = model.password,
                agree = "1",
                privacy = "1"
            };
        }

        public int id { get; set; }

        public string name { get; set; }

        public string surname { get; set; }

        public string email { get; set; }

        public string password { get; set; }

        public string agree { get; set; } = "1";

        public string privacy { get; set; } = "1";
    }

    public class BadgeBoxGetUserModel : BadgeBoxBaseModel
    {
        public BadgeBoxGetUserModel() : base()
        { }

        public static BadgeBoxGetUserModel Create(CustomerModel model)
        {
            return new BadgeBoxGetUserModel()
            {
                token = model.BBToken,
            };
        }

        public string token { get; set; }
    }

    public class BadgeBoxUpdateUserModel : BadgeBoxBaseModel
    {
        public BadgeBoxUpdateUserModel() : base() { }
        public static BadgeBoxUpdateUserModel Create(CustomerModel model)
        {
            return new BadgeBoxUpdateUserModel()
            {
                user_token = model.BBToken,
                name = model.name,
                surname = model.lastName
            };
        }

        public string name { get; set; }
        public string surname { get; set; }
        public string gender { get; set; }
    }

    public class BadgeBoxAddEmployeeModel : BadgeBoxBaseModel
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
    }

    public class BadgeBoxDeleteEmployeeModel : BadgeBoxBaseModel
    {
        public int id_empl { get; set; }
    }

}