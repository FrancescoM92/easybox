﻿namespace EasyBoxApplicationServer.Models.BadgeBox.Requests
{
    public class BadgeBoxGetRecordsModel : BadgeBoxBaseModel
    {
        public string apikey { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public long id_empl { get; set; }
    }

    public class BadgeBoxCreateRecordModel : BadgeBoxBaseModel
    {
        public long id_empl { get; set; }
        public string edit { get; set; }
        public string place { get; set; }
        public string note { get; set; }
        public string tags { get; set; }
        public int pause { get; set; }
        public string checkin { get; set; }
        public string checkout { get; set; }
    }

    public class BadgeBoxDeleteRecordModel : BadgeBoxBaseModel
    {
        public string apikey { get; set; }
        public long id_empl { get; set; }
        public long id { get; set; }
    }
    
}