﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.BadgeBox.Responses
{
    public class BadgeBoxCreateUserResponse : BadgeBoxGenericResponse
    {
        public string token { get; set; }
    }

    public class BadgeBoxAddEmployeeResponse : BadgeBoxGenericResponse
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
    }

    public class BadgeBoxDeleteEmployeeResponse : BadgeBoxGenericResponse
    {
        public string id_agency { get; set; }
    }

    public class BadgeBoxSwitchCompanyResponse : BadgeBoxGenericResponse
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string agency_to { get; set; }
    }

    public class BadgeBoxGetUserResponse
    {
        public BadgeBoxUser user { get; set; }
        public Permissions permissions { get; set; }
        public BadgeBoxAgency agency { get; set; }
        public List<BadgeBoxDevice> devices { get; set; }
        public List<BadgeBoxAgency> agencies { get; set; }

    }

    public class BadgeBoxUser
    {
        public string id { get; set; }
        public string name { get; set; }
        public string suffix { get; set; }
        public string prefix { get; set; }
        public string middlename { get; set; }
        public string avatar_id { get; set; }
        public string gender { get; set; }
        public string email { get; set; }
        public string timezone { get; set; }
        public string language { get; set; }
        public string email_notify { get; set; }
        public string current_agency { get; set; }
        public string activated { get; set; }
        public string confirm_code { get; set; }
        public string newsletter { get; set; }
        public string lng { get; set; }
        public string lat { get; set; }
        public string first_login { get; set; }
        public string system { get; set; }
        public string ref_admin { get; set; }
        public string removed { get; set; }
        public string age { get; set; }
        public string last_login { get; set; }
        public string last_page { get; set; }
        public string tuts { get; set; }
        public string registered_by { get; set; }
        public string attempts { get; set; }
        public string time_attempts { get; set; }
    }

    public class BadgeBoxAgency
    {
        public string id_user { get; set; }
        public string id_agency { get; set; }
        public string a_email { get; set; }
        public string role { get; set; }
        public string name { get; set; }
        public string vat { get; set; }
        public string cf { get; set; }
        public string address { get; set; }
        public string number { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string timezone { get; set; }
        public string created { get; set; }
        public string cap { get; set; }
        public string category { get; set; }
        public string id_owner { get; set; }
    }

    public class Permissions
    {
        public string accountant { get; set; }
        public string badge { get; set; }
        public string dashboard { get; set; }
        public string shift { get; set; }
        public string todo { get; set; }
        public string news { get; set; }
        public string tracktime { get; set; }
        public string employee { get; set; }
        public string attendance { get; set; }
        public string timesheet { get; set; }
        public string request { get; set; }
        public string expense { get; set; }
        public string team { get; set; }
        public string document { get; set; }
        public string accounting { get; set; }
        public string performance { get; set; }
        public string client { get; set; }
        public string supplier { get; set; }
        public string company { get; set; }
        public string tag { get; set; }
        public string planer { get; set; }
        public string place { get; set; }
        public string rule { get; set; }
        public string skill { get; set; }
        public string calendar { get; set; }
        public string can_buy { get; set; }
        public string can_help { get; set; }
        public string can_logo { get; set; }
        public string can_money { get; set; }
        public string can_offline { get; set; }
        public string can_add_people { get; set; }
        public string can_social { get; set; }
        public string can_alarm { get; set; }
        public string request_counters { get; set; }
        public string visibility { get; set; }
    }

    public class BadgeBoxDevice
    {
        public string id { get; set; }
        public string id_user { get; set; }
        public string uid { get; set; }
        public string push_id { get; set; }
        public string platform { get; set; }
        public string device_version { get; set; }
        public string app_version { get; set; }
        public string name { get; set; }
        public string trusted { get; set; }
        public string activated { get; set; }
    }

}