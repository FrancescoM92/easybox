﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.BadgeBox.Responses
{
    public class BadgeBoxCreateCompanyResponse : BadgeBoxGenericResponse
    {
        public int id_agency { get; set; }
    }
}