﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.BadgeBox.Responses
{
    public class BadgeBoxInfoResponse : BadgeBoxGenericResponse
    {
        public string info { get; set; }
        public string value { get; set; }
    }
}