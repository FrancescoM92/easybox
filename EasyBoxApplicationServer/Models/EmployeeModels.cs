﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.Models.ServiceBus;
using EasyBoxApplicationServer.RemoteServers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EasyBoxApplicationServer.Models
{
    public class EmployeeModel : PersonModelBase
    {
        public static EmployeeModel Create(Lavoratore employee)
        {
            return new EmployeeModel()
            {
                name = employee.Nome,
                lastName = employee.Cognome,
                fiscalCode = employee.CodiceFiscale
            };
        }

        public static Employee CreateEmployee(Lavoratore lavoratore)
        {
            long feaId = 0;
            if (lavoratore.Email != null && lavoratore.Telefono != null)
                feaId = new DBHandler().insertFEA(new Database.FirmaElettronicaAvanzata()
                {
                    cellulare = lavoratore.Telefono,
                    email = lavoratore.Email
                }).Item1;
            Employee emp = new Employee()
            {
                name = lavoratore.Nome,
                lastName = lavoratore.Cognome,
                fiscalCode = lavoratore.CodiceFiscale,
                email = lavoratore.Email,
                employeeCode = lavoratore.CodiceRiferimentoLavoratore,
                isActive = true,
                phone = lavoratore.Telefono
            };
            if (feaId > 0)
                emp.fea = feaId;
            return emp;
        }

        public static EmployeeModel Create(Employee employee)
        {
            Engagement engage = new DBHandler().getEngagementByEmployeeID(employee.ID).Item1;
            List<Extension> exts = new DBHandler().getExtensionsByEngagementID(engage.ID).Item1;
            Address address = new Address();
            DateTime to = new DateTime();
            if(exts != null && exts.Count > 0)
            {
                to = exts.Max(x => x.endDate).Value;
            }
            else
            {
                to = engage.missionStopDate.Value;
            }
            BadgeBoxGetRecordResponse res = new BadgeBoxGetRecordResponse();
            if (employee.isBBSync)
            {
                res = BadgeBoxServer.getRecords(new BadgeBox.Requests.BadgeBoxGetRecordsModel()
                {
                    id_empl = (long)employee.BBId,
                    user_token = employee.BBToken,
                    from = engage.missionStartDate.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                    to = to.ToString("yyyy-MM-dd HH:mm:ss")
                }).Item1;
            }
            int workedHours = 0;
            if(res.records != null)
                workedHours = res.records.Sum(x => int.Parse(x.total_hours)) / 60; 
            if (employee.addressId != null && employee.addressId != 0)
                address = new DBHandler().getAddressByID((long)employee.addressId).Item1;
            string startDate = string.Empty;
            string endDate = string.Empty;
            if (engage != null)
            {
                startDate = engage.missionStartDate.Value.ToString("dd/MM/yyyy");
                endDate = to.ToString("dd/MM/yyyy");
            }
            return new EmployeeModel()
            {
                id = employee.ID,
                name = employee.name,
                lastName = employee.lastName,
                email = employee.email,
                fiscalCode = employee.fiscalCode,
                bbToken = employee.BBToken,
                missionStartDate = startDate,
                missionEndDate = endDate,
                address = address.street,
                workedHours = workedHours
            };
        }

        public string address { get; set; }

        public string bbToken { get; set; }

        public long branchID { get; set; }

        public long idCompany { get; set; }

        public string missionStartDate { get; set; }

        public string missionEndDate { get; set; }

        public string serialNumber { get; set; }

        public string role { get; set; }

        public bool isActive { get; set; }

        public int workedHours { get; set; }
    }

    public class EmployeesModel
    {
        public List<EmployeeModel> employees { get; set; }
    }

    public class UpdateEmployeePhotoModel
    {
        public int employeeId { get; set; }

        public byte[] image { get; set; }
    }
}