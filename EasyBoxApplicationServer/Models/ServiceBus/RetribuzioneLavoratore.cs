﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class RetribuzioneLavoratore
    {
        public double RetribuzioneOraria { get; set; }
        public double RetribuzioneMensile { get; set; }
    }
}