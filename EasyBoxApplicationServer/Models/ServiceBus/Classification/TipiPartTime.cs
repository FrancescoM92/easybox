﻿namespace EasyBoxApplicationServer.Models.ServiceBus.Classification
{
    public enum TipiPartTime
    {
        Nessuno,
        Orizzontale,
        Verticale,
        Misto
    }
}