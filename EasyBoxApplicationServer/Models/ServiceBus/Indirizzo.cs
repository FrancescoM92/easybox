﻿namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class Indirizzo
    {
        public static Indirizzo Create(string Via, string CAP, string Comune, string Provincia, string SiglaProvincia)
        {
            return new Indirizzo()
            {
                Via = Via,
                CAP = CAP,
                Comune = Comune,
                Provincia = Provincia,
                SiglaProvincia = SiglaProvincia
            };
        }
        public string Via { get; set; }
        public string Comune { get; set; }
        public string Provincia { get; set; }
        public string SiglaProvincia { get; set; }
        public string CAP { get; set; }
    }
}