﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.ServiceBus
{
    public class Documenti
    {
        public Documenti Create (Documenti_sin doc)
        {
            return new Documenti
            {
                Sorgente = doc.Sorgente,
                ListaDocumenti = new ListaDocumenti
                {
                    element = new List<Documento>() { doc.ListaDocumenti.element }
                }
            };
        }

        public Sorgente Sorgente { get; set; }
        public ListaDocumenti ListaDocumenti { get; set; }
    }

    public class Documenti_sin
    {
        public Sorgente Sorgente { get; set; }
        public ListaDocumenti_sin ListaDocumenti { get; set; }
    }
    public enum Sorgente
    {
        AgriJob,
        DataBook,
        ShakeJob,
        EasyBox
    }

    public class Documento
    {
        public string TipoDocumento { get; set; }
        public string IDDocumento { get; set; }
        public string UID { get; set; }
    }

    public enum TipoDocumento
    {
        Somministrazione,
        Assunzione,
        ProrogaSomministrazione,
        ProrogaAssunzione
    }

    public class ListaDocumenti
    {
        public List<Documento> element { get; set; }
        //public Documento element { get; set; }
    }

    public class ListaDocumenti_sin
    {
        public Documento element { get; set; }
    }
}