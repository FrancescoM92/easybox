﻿using System;

namespace EasyBoxApplicationServer.Models
{
    public class CompanyContractModel
    {
        public long companyID { get; set; }

        public DateTime endDate { get; set; }

        public string name { get; set; }

        public string note { get; set; }

        public DateTime startDate { get; set; }
    }
}