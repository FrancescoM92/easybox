﻿using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Models
{
    public class PersonModelBase
    {
        public long id { get; set; }

        public string name { get; set; }

        public string lastName { get; set; }

        public string email { get; set; }

        public string password { get; set; }

        public UserTypeEnum type { get; set; }

        public long secondaryId { get; set; }

        public string fiscalCode { get; set; }
    }
}