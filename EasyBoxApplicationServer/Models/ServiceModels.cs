﻿using EasyBoxApplicationServer.Database;
using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models
{
    public class ServiceModel
    {
        public static ServiceModel Create(Service service)
        {
            return new ServiceModel()
            {
                documenti = service.documenti,
                rapportini = service.rapportini,
                firma = service.firma
            };
        }

        public int id { get; set; }
        public bool documenti { get; set; }
        public bool rapportini { get; set; }
        public bool firma { get; set; }
        
    }

    public class ServicesModel
    {
        List<ServiceModel> services { get; set; }
    }

    public class ServiceModelRequest
    {
        public ServiceModel service { get; set; }
    }
}