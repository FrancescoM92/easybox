﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Models
{
    public class UserTypeModel
    {
        public long id { get; set; }

        [Required]
        public UserTypeEnum type { get; set; }

        public long secondaryId { get; set; }
        
    }

    public class CustomerTypeModel : UserTypeModel
    {
        [Required]
        public long companyId { get; set; } 
    }

    public class BranchOperatorTypeModel : UserTypeModel
    {
        [Required]
        public long branchId { get; set; }
    }

    public class OperatorTypeModel : UserTypeModel { }

    public class EmployeeTypeModel : UserTypeModel { }
}