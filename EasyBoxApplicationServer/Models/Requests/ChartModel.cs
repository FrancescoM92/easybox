﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.Requests
{
    public class ChartModel
    {
        public string token { get; set; }
        public string azure { get; set; }
        public int company_id { get; set; }
        public int year { get; set; }
        public long id { get; set; }
    }
}