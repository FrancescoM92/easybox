﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.Requests
{
    public class IdentityModel
    {
        public long operatorId { get; set; }
        public long branchOperatorId { get; set; }
        public long CustomerId { get; set; }
        public long EmployeeId { get; set; }
        public long CompanyId { get; set; }
    }
}