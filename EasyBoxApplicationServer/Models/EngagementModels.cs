﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.ServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class EngagementModel
    {
        public EngagementModel()
        {
            Extension = new List<ExtensionModel>();
        }

        public static EngagementModel Create(Assunzione assunzione)
        {
            List<ExtensionModel> extenss = ExtensionsModel.Create(assunzione.Proroga).extensions;
            return new EngagementModel()
            {
                administrationNumber = assunzione.NumeroSomministrazione,
                businessUnitID = assunzione.IDUnitaDiBusiness,
                employeeCode = assunzione.CodiceRiferimentoLavoratore.ToString(),
                Extension = extenss,
                engagementDate = assunzione.DataAssunzione,
                engagementNumber = assunzione.NumeroAssunzione,
                matriculationNumber = assunzione.NumeroAssunzione,
                missionStartDate = assunzione.DataInizioMissione,
                missionStopDate = assunzione.DataFineMissione,
                employeeFiscalCode = assunzione.CodiceFiscaleLavoratore
            };
        }

        public long ID { get; set; }
        public string engagementNumber { get; set; }
        public DateTime? missionStartDate { get; set; }
        public DateTime? missionStopDate { get; set; }
        public string matriculationNumber { get; set; }
        public string businessUnitID { get; set; }
        public DateTime? engagementDate { get; set; }
        public string administrationNumber { get; set; }
        public string employeeCode { get; set; }
        public string employeeFiscalCode { get; set; }

        public virtual List<ExtensionModel> Extension { get; set; }
    }
}