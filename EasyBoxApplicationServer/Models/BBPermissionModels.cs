﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models
{
    public class BBPermissionModel
    {
        public long id { get; set; }
        public int value { get; set; }
        public string name { get; set; }
    }

    public class BBPermissionsModel
    {
        public List<BBPermissionModel> permissions { get; set; }
    }
}