﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.ServiceBus;
using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models
{
    public class CustomerModel : PersonModelBase
    {
        public CustomerModel()
        {
            roleId = 1;
        }

        public static CustomerModel Create(Customer customer)
        {
            Service serv = new DBHandler().getService((long)customer.serviceId).Item1;
            Database.FirmaElettronicaAvanzata fea = null;
            if (customer.feaId != null && customer.feaId > 0)
                fea = new DBHandler().getFEA((long)customer.feaId).Item1;
            return new CustomerModel()
            {
                id = customer.ID,
                name = customer.name,
                lastName = customer.lastName,
                email = customer.email,
                password = customer.password,
                phone = fea != null ? fea.cellulare : "",
                firma = serv.firma,
                documenti = serv.documenti,
                rapportini = serv.rapportini,
                BBId = (int?) customer.BBId
            };
        }

        public bool documenti { get; set; } = false;

        public bool rapportini { get; set; } = false;

        public bool firma { get; set; } = false;

        public string phone { get; set; }

        public string BBToken { get; set; }

        public short roleId { get; set; }

        public long idCompany { get; set; }

        public int? BBId { get; set; }
    }

    public class CustomersModel
    {
        public List<PersonModelBase> customers { get; set; }
    }

    public class SetCustomerIsActiveModel
    {
        public long id { get; set; }

        public bool isActive { get; set; }
    }
}