﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models.AWDoc
{
    public class GetAWDocumentModel
    {
        public int year { get; set; }
        public int month { get; set; }
        public string token { get; set; }
        public string azure { get; set; }
    }
    
}