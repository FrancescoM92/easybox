﻿using System.Collections.Generic;

namespace EasyBoxApplicationServer.Models.AWDoc
{
    public class FEAModel
    {
        public string phoneNumber { get; set; }
        public string pin { get; set; }
        public string otp { get; set; }
        public string domain { get; set; }
        public string[] docUuids { get; set; }
        public string signerName { get; set; }
        public string signerTaxID { get; set; } //Cod. Fiscale
        public string signerEmail { get; set; }
    }
    public class FEAModelToken : FEAModel
    {
        public string token { get; set; }
        public string azure { get; set; }
    }
}