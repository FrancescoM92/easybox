﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.Models
{
    public class CountModel
    {
        public static CountModel Create(long id, int count)
        {
            return new CountModel()
            {
                count = count,
                id = id
            };
        }

        public long id { get; set; }
        public int count { get; set; }
    }

    public class CountsModel
    {
        public List<CountModel> counts { get; set; }
    }
}