﻿using EasyBoxApplicationServer.Database;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    public class BaseController : Controller
    {
        protected DBHandler getDBHandler()
        {
            return new DBHandler();
        }

        protected string getErrorMessage(IEnumerable<ModelState> values)
        {
            string exMsg = string.Empty;
            var errors = values.Where(x => x.Errors.Count > 0);
            foreach (var error in errors)
                exMsg += error.Errors.FirstOrDefault().ErrorMessage + ". ";
            return exMsg;
        }

        protected string getErrorMessageBB(IEnumerable<ModelState> values)
        {
            string MESSAGE = string.Empty;
            var errors = values.Where(x => x.Errors.Count > 0);
            foreach (var error in errors)
                MESSAGE += error.Errors.FirstOrDefault().ErrorMessage + ". ";
            return MESSAGE;
        }

        public class ErrorMessage
        {
            public string ExMsg { get; set; }
        }

        public class ErrorMessageBB
        {
            public string MESSAGE { get; set; }
        }

        protected JsonResult getJsonResult(string exmsg, object obj = null, bool isGet = false)
        {
            if (!isGet)
            {
                if (string.IsNullOrEmpty(exmsg))
                {
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return Json(obj, "application/json", Encoding.UTF8);
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json(new ErrorMessage() { ExMsg = exmsg });
                }
            }
            else
            {
                if (string.IsNullOrEmpty(exmsg))
                {
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return Json(obj, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json(new ErrorMessage() { ExMsg = exmsg }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}