﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class CompaniesController : BaseController
    {
        public JsonResult getCompanies(TokenModel model)
        {
            string exmsg = string.Empty;
            List<CompanySummaryResponseModel> response = null;
            Tuple<List<CompanySummaryResponseModel>, string> result = null;
            if (!string.IsNullOrEmpty(model.azure))
            {
                List<string> groups = MyHelper.getUserBusinessUnits(model.azure);
                if (groups == null) exmsg = "L'utente non appartiene a nessuna filiale";
                result = getDBHandler().getCompaniesSummary(groups);
            }
            else
            {
                result = getDBHandler().getCompaniesSummary();
            }
            if (string.IsNullOrEmpty(result.Item2) && result.Item1 != null)
                response = result.Item1;
            else exmsg = result.Item2;
            return getJsonResult(exmsg, result.Item1);
        }

        public JsonResult getTemporaryCompanies(TokenModel model)
        {
            string exmsg = string.Empty;
            List<TemporaryCompanySummaryResponseModel> response = null;

            Tuple<List<TemporaryCompanySummaryResponseModel>, string> result = null;
            if (!string.IsNullOrEmpty(model.azure))
            {
                List<string> groups = MyHelper.getUserBusinessUnits(model.azure);
                if (groups == null) exmsg = "L'utente non appartiene a nessuna filiale";
                result = getDBHandler().getTemporaryCompaniesSummary(groups);
            }
            else
            {
                result = getDBHandler().getTemporaryCompaniesSummary();
            }

            if (string.IsNullOrEmpty(result.Item2) && result.Item1 != null)
                response = result.Item1;
            else exmsg = result.Item2;
            return getJsonResult(exmsg, result.Item1);
        }

        public JsonResult getCompaniesProfilePhoto(string json)
        {
            string exmsg = string.Empty;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            IdsRequest model = null;
            try
            {
                model = serializer.Deserialize<IdsRequest>(json);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }

            string image = string.Empty;
            List<Tuple<string, long>> response = new List<Tuple<string, long>>();
            if (ModelState.IsValid)
            {
                foreach (int i in model.ids)
                {
                    Media media = new DBHandler().getMediaByCompanyId(i).Item1;
                    string base64image = string.Empty;
                    if (media != null) base64image = Convert.ToBase64String(media.data);
                    response.Add(new Tuple<string, long>(base64image, i));
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }
            return getJsonResult(exmsg, response);
        }
    }
}