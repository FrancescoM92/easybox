﻿using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class BranchesController : BaseController
    {
        public JsonResult getBranches(TokenModel model)
        {
            List<BranchResponseModel> response = null;
            string exmsg = string.Empty;
            Tuple<List<BranchResponseModel>, string> result = null;
            List<BranchResponseModel> branches = new List<BranchResponseModel>();

            if (!string.IsNullOrEmpty(model.azure))
            {
                List<string> groups = MyHelper.getUserBusinessUnits(model.azure);
                if (groups == null) exmsg = "L'utente non appartiene a nessuna filiale";
                result = getDBHandler().getBranches(groups);
            }
            else
            {
                result = getDBHandler().getBranches();
            }


            if (string.IsNullOrEmpty(result.Item2) && result.Item1 != null)
            {
                response = result.Item1;
            }
            else
                exmsg = result.Item2;

            return getJsonResult(exmsg, response);
        }
    }
}