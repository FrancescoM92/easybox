﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RestAuthorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class CustomerCompanyController : BaseController
    {
        #region company
        public JsonResult getCompanyDetail(GetCompanyDetailModel model)
        {
            Tuple<Company, string> companyResult = null;
            GetCompanyDetailResponseModel response = null;
            Customer owner = null;
            List<EmployeeModel> employees = new List<EmployeeModel>();
            List<CustomerModel> customers = new List<CustomerModel>();

            companyResult = getDBHandler().getCompanyById(model.companyId);
            if (companyResult.Item1 != null && companyResult.Item1.ownerId != null && companyResult.Item1.ownerId > 0)
                owner = (getDBHandler().getCustomer((long)companyResult.Item1.ownerId)).Item1;
            string address = getDBHandler().getAddressByID((long)companyResult.Item1.addressId).Item1.street;
            string startDate = string.Empty;
            string endDate = string.Empty;
            Administration admin = new DBHandler().getAdministrationByCompanyID(model.companyId).Item1;
            if (admin != null)
            {
                startDate = admin.provisionStartDate.ToString();
                endDate = admin.provisionEndDate.ToString();
            }

            List<Employee> employeesTemp = new DBHandler().getCompanyEmployees(model.companyId).Item1;
            List<Customer> customersTemp = new DBHandler().getCompanyCustomers(model.companyId).Item1;

            if (companyResult.Item1 != null)
            {
                response = new GetCompanyDetailResponseModel()
                {
                    address = address,
                    companyCode = companyResult.Item1.companyCode,
                    customers = new List<CustomerModel>(),
                    email = companyResult.Item1.email,
                    employees = new List<EmployeeModel>(),
                    startDate = startDate.Split(' ')[0],
                    endDate = endDate.Split(' ')[0],
                    id = companyResult.Item1.ID,
                    name = companyResult.Item1.businessName,
                    vat = companyResult.Item1.vat
                };
                if (owner != null) response.owner = CustomerModel.Create(owner);

                if (employeesTemp != null)
                {
                    foreach (Employee e in employeesTemp)
                    {
                        employees.Add(EmployeeModel.Create(e));
                    }
                }
                response.employees = employees;
                if (customersTemp != null)
                {
                    foreach (Customer c in customersTemp)
                    {
                        customers.Add(CustomerModel.Create(c));
                    }
                }
                response.customers = customers;
            }
            return getJsonResult(companyResult.Item2, response);
        }
        public JsonResult setCompanyIsActive(SetCompanyIsActiveModel model)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
                exmsg = getDBHandler().setCompanyIsActive(model); // VA RIMOSSA DA BADGEBOX?!? SECONDO ME, NO
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());

            return getJsonResult(exmsg);
        }

        public JsonResult updateCompanyData(CompanyModel model)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
            {
                exmsg = getDBHandler().updateCompany(model);
            }
            else
                exmsg = getErrorMessage(ModelState.Values);

            return getJsonResult(exmsg);
        }

        public JsonResult getCompanyDetailChart(ChartModel model)
        {
            Tuple<ChartResponse, string> response = null;
            PersonModelBase person = new DBHandler().getUserFromToken(model.token, null);
            if (model.id == -1)
            {
                response = getDBHandler().getEngagedEmployees(model.year, new DBHandler().getCustomerCompanies(person.id).Item1.FirstOrDefault().ID);
            }
            else
            {
                response = getDBHandler().getEngagedEmployees(model.year, model.id);
            }
            return getJsonResult(response.Item2, response.Item1);
        }

        public JsonResult updateCompanyServices(ServiceModel model)
        {
            string exmsg = string.Empty;
            string Response = string.Empty;
            if (ModelState.IsValid)
            {
                exmsg = getDBHandler().updateServiceStatus(model);
                Response = "OK";
            }
            else
                exmsg = getErrorMessage(ModelState.Values);
            return getJsonResult(exmsg, Response);
        }

        #endregion

        #region temporary company
        public JsonResult updateTemporaryCompanyToCompany(UpdateTemporaryCompanyToCompanyModel model)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
            {
                DBHandler dbHandler = getDBHandler();
                Tuple<Company, string> result = dbHandler.updateTemporaryCompanyToCompany(model.id, model.ownerId);
            }
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());

            return getJsonResult(exmsg);
        }
        #endregion
    }
}