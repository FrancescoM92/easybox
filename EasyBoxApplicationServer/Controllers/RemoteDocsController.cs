﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.AWDoc;
using EasyBoxApplicationServer.Models.ServiceBus;
using EasyBoxApplicationServer.RemoteServers.AWDoc;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Services;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static AWTech.AWDoc.Protocolbuffer.Document.Types;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class RemoteDocsController : BaseController
    {
        private AWDocHandler handler = new AWDocHandler();

        [HttpGet]
        public List<AWTech.AWDoc.Protocolbuffer.Document> testAwDoc()
        {
            List<AWTech.AWDoc.Protocolbuffer.Document> docs = null;
            if (handler.DomainExist())
            {
                docs = handler.GetDocuments(new List<string>()
                {
                    "4a4fc473-d1e5-4e64-b408-7dbe0031fef8",
                    "4ada6898-c4f2-4ed3-8af8-cca61b40f5c0",
                    "121ea633-9d1c-48d4-9fa7-4d1dd40c553a",
                    "2aef02bf-c0df-4966-9e63-6f81694333cf",
                    "f74cff9f-68ed-48e6-83b6-4d09b5ecc24c",
                    "78d90254-8f2e-4471-a8e8-09558f816178",
                    "056c1717-cfde-4e65-8aca-4c062c531f6f",
                    "ff38c333-4ec5-4656-9775-440d4096b1cb",
                    "9a8dee04-675a-487f-95a8-28aa6ae34e5d"
                });
            }
            return docs;
        }

        public JsonResult getList(GetAWDocumentModel model)//long employeeID
        {
            string exmsg = string.Empty;
            List<AWTech.AWDoc.Protocolbuffer.Document> docs = null;
            List<AWDocumentInter> result = new List<AWDocumentInter>();
            try
            {
                PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
                List<AWDocument> AWDocs = getDBHandler().getAWDocuments(person).Item1;
                //AWDocs.RemoveAll(x => x.source != Sorgente.DataBook.ToString());
                MyHelper.Log("NUM DOCUMENTI IN DB : " + AWDocs.Count);
                docs = handler.GetDocumentsByAW(AWDocs, person);

                foreach (AWTech.AWDoc.Protocolbuffer.Document d in docs)
                {
                    if (d.SignatureState.Equals(SignatureState.Unsigned))
                    {
                        AWDocument awd = getDBHandler().getAWDocument(new Guid(d.Uuid)).Item1;
                        if (awd != null)
                        {
                            if (awd.signing)
                            {
                                if (DateTime.Now.Subtract((DateTime)awd.signingTime).TotalMinutes > 5)
                                    awd = getDBHandler().setAWDocumentsUnsigned(awd.UID);
                            }
                            result.Add(AWDocumentInter.Create(d, awd));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
                exmsg = e.Message;
            }
            return getJsonResult(exmsg, new AWDocumentsModel() { documents = result, month = model.month, year = model.year }, true);
        }

        public JsonResult getSignedList(GetAWDocumentModel model)//long employeeID
        {
            string exmsg = string.Empty;
            List<AWTech.AWDoc.Protocolbuffer.Document> docs = null;
            List<AWDocumentInter> result = new List<AWDocumentInter>();
            try
            {
                PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
                List<Database.AWDocument> AWDocs = getDBHandler().getAWDocuments(person).Item1;
                AWDocs.RemoveAll(x => x.source != Sorgente.DataBook.ToString());
                docs = handler.GetDocumentsByAW(AWDocs, person);

                foreach (AWTech.AWDoc.Protocolbuffer.Document d in docs)
                {
                    MyHelper.AWDLog(new string[] { "documento: " + d.ToString() });
                    Database.AWDocument awd = getDBHandler().getAWDocument(new Guid(d.Uuid)).Item1;
                    if (d.SignatureState.Equals(SignatureState.Signed))
                        result.Add(AWDocumentInter.Create(d, awd));
                }
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            return getJsonResult(exmsg, new AWDocumentsModel() { documents = result, month = model.month, year = model.year }, true);
        }

        public JsonResult getCategorizedList()//long employeeID
        {
            return getJsonResult(null, new List<AWDocumentInter>(), true);
        }

        public JsonResult getOTP(TokenModel model)
        {
            string phoneNumber = string.Empty;
            PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
            switch (person.type)
            {
                case Constants.UserTypeEnum.Employee:
                    Database.Employee e = getDBHandler().getEmployee(person.id).Item1;
                    Database.FirmaElettronicaAvanzata fea = getDBHandler().getFEA((long)e.fea).Item1;
                    if (fea != null)
                        phoneNumber = fea.cellulare;
                    break;
                default:
                    Database.Customer c = getDBHandler().getCustomer(person.id).Item1;
                    if (c.feaId != null && c.feaId > 0)
                        phoneNumber = getDBHandler().getFEA((long)c.feaId).Item1.cellulare;
                    else
                        phoneNumber = getDBHandler().getCustomerCompanies(person.id).Item1.First().phone;
                    break;
            }
            handler.getOTP(phoneNumber); // da rimettere apposto
            Response.StatusCode = 200;
            return getJsonResult(null, "ok");
        }

        public JsonResult signDocuments(FEAModelToken model, bool isEasy = false)
        {
            string result = string.Empty;
            PersonModelBase person = new Database.DBHandler().getUserFromToken(model.token, model.azure);

            Task firma = new Task(() =>
            {
                if (!handler.signDocumentsFEA(model, person))
                {
                    result = "OK";
                }
                else
                {
                    result = "KO";
                }
            });

            firma.Start();

            //while(firma.Status != TaskStatus.RanToCompletion)
            //{
            //    System.Threading.Thread.Sleep(1000);
            //}

            System.Threading.Thread.Sleep(3000);
            MyHelper.Log("CHIAMATA FIRMA DOC AWDOC: " + result);

            try
            {
                string[] uuids = model.docUuids.First().Split(',');
                //DOCUMENTO SOMM --> RECUPERO TUTTI I DOC ASSUNZIONE DI OGNI EMPLOYEE --> RECUPERO ASSUNZIONI --> CONTROLLO CODICE SOMMINISTRAZIONE --> ABILITO FIRMA ASSUNZIONE
                getDBHandler().setAWDocumentsSigning(model);
                EmailSender.sendEmail(model.signerEmail, "Conferma Firma Documento", "Salve, è stata richiesta la firma di " + uuids.Count() + " documenti, a breve potrà visualizzare ed o scaricare i documenti firmati dalla piattaforma", isEasy);
                if (person.type.Equals(Constants.UserTypeEnum.Customer) || person.type.Equals(Constants.UserTypeEnum.Owner))
                {
                    
                    foreach (string uuid in uuids)
                    {
                        MyHelper.Log("CERCO DOCUMENTO: " + uuid);
                        AWDocument admDoc = getDBHandler().getAWDocument(Guid.Parse(uuid)).Item1;
                        //GESTIRE FIRMA SOMMINISTRAZIONI PER ABILITARE VISUALIZZAZIONE ASSUNZIONI 

                        List<Engagement> toEnableEngs = getDBHandler().getEngagementFromAdministrationCode(admDoc.IDDocumento).Item1;
                        MyHelper.Log("TROVATE: " + toEnableEngs.Count + " ASSUNZIONI");
                        foreach (Engagement eng in toEnableEngs)
                        {
                            MyHelper.Log("TROVATA ASSUNZIONE: " + eng.engagementNumber);
                            Employee emp = getDBHandler().getEmployeeByCode(eng.employeeCode).Item1;
                            List<AWDocument> docs = getDBHandler().getAWDocumentsByEngagement(eng).Item1;
                            MyHelper.Log("TROVATI " + docs.Count + " DOCUMENTI PER L'ASSUNZIONE");
                            List<AWDocument> toUpdate = new List<AWDocument>();
                            foreach (AWDocument awd in docs)
                            {
                                if (!awd.signable)
                                {
                                    MyHelper.Log("AGGIUNGO Documento: " + awd.UID + " su documenti firmabili");
                                    toUpdate.Add(awd);
                                }
                            }
                            try
                            {
                                if (toUpdate.Count > 0)
                                {
                                    MyHelper.Log("DOCUMENTI DA AGGIORNARE: " + toUpdate.Count);
                                    foreach (AWDocument d in toUpdate)
                                    {
                                        getDBHandler().updateSignabilityAWDocument(d);
                                    }

                                    if (emp != null)
                                    {
                                        EmailSender.sendEmail(emp.email, "Nuovi Documenti Disponibili", "Salve, ha " + toUpdate.Count + " nuovi documenti da firmare, li troverà sulla piattaforma a breve", true);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MyHelper.Log(ex.Message);

                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
                MyHelper.Log(e.StackTrace);
            }

            Response.StatusCode = 200;
            return getJsonResult(null, result);
        }

        [HttpGet]
        public void download()
        {
            string docUUID = Request.Params["Uuid"];
            string exmsg = string.Empty;
            byte[] response = null;
            try
            {
                if (docUUID != null && docUUID.Length > 0)
                {
                    if (handler.DomainExist())
                    {
                        using (MemoryStream stream = new MemoryStream(handler.DownloadDocument(docUUID)))
                        {
                            stream.CopyTo(Response.OutputStream);
                        }
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", @"filename=" + DateTime.Now.ToString("yyyyMMdd") + ".pdf");
                    }
                    else exmsg = "Errore in AWDoc";
                }
                else exmsg = "Identificativo del documento errato o inesistente";
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            if (exmsg != string.Empty)
            {
                response = Encoding.ASCII.GetBytes(exmsg);
            }
        }

        [HttpGet]
        public void downloadSigned()
        {
            string docUUID = Request.Params["Uuid"];
            string exmsg = string.Empty;
            byte[] response = null;
            try
            {
                if (docUUID != null && docUUID.Length > 0)
                {
                    if (handler.DomainExist())
                    {
                        using (MemoryStream stream = new MemoryStream(handler.DownloadSignedDocument(docUUID)))
                        {
                            stream.CopyTo(Response.OutputStream);
                        }
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("Content-Disposition", @"filename=" + DateTime.Now.ToString("yyyyMMdd") + ".pdf");
                    }
                    else exmsg = "Errore in AWDoc";
                }
                else exmsg = "Identificativo del documento errato o inesistente";
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
            if (exmsg != string.Empty)
            {
                response = Encoding.ASCII.GetBytes(exmsg);
            }
        }
    }
}
