﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.RestAuthorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class CompanyMessageController : BaseController
    {
        public JsonResult sendMessage(string json)
        {
            string exmsg = string.Empty;
            Tuple<long, string> response = new Tuple<long, string>(0, null);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            CompanyMessageRequest model = null;
            try
            {
                model = serializer.Deserialize<CompanyMessageRequest>(json);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }

            if (!string.IsNullOrEmpty(model.token) || !string.IsNullOrEmpty(model.azure))
            {
                PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
                if (ModelState.IsValid)
                {
                    response = getDBHandler().insertCompanyMessage(model.message, new UserTypeModel() { type = person.type, id = person.id, secondaryId = person.secondaryId });
                    exmsg = response.Item2;
                }
                else
                {
                    exmsg = getErrorMessage(ModelState.Values.ToList());
                }
            }
            return getJsonResult(response.Item2, response.Item1);
        }

        public JsonResult getMessages(string request)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            TokenModel token = serializer.Deserialize<TokenModel>(request);
            string exmsg = string.Empty;
            Tuple<List<CompanyMessageModel>, string> response = null;
            PersonModelBase person = getDBHandler().getUserFromToken(token.token, token.azure);
            if (ModelState.IsValid)
            {
                List<CompanyMessageModel> messages = new List<CompanyMessageModel>();
                List<CompanyMessage> dbMessages = getDBHandler().getMessagesByCompanyId(token.id).Item1;

                foreach (CompanyMessage m in dbMessages)
                {
                    messages.Add(new CompanyMessageModel()
                    {
                        body = m.body,
                        companyId = m.companyId,
                        timeStamp = m.timeStamp,
                        idFrom = m.idFrom,
                        idTo = m.idTo,
                        sentByCompany = m.sentByCompany
                    });
                    if (m.idFrom != person.secondaryId && m.hasBeenRead != true)
                        exmsg = getDBHandler().setCompanyMessageRead(m);
                }
                response = new Tuple<List<CompanyMessageModel>, string>(messages, exmsg);
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }

            return getJsonResult(exmsg, response);
        }

        public JsonResult countNewMessages(TokenModel model)
        {
            PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
            string exmsg = string.Empty;
            Tuple<int, string> result = null;
            if (ModelState.IsValid) { result = getDBHandler().getCompanyNewMessagesCount(person.type, model.id); }
            else { exmsg = getErrorMessage(ModelState.Values.ToList()); }
            return getJsonResult(exmsg, result);
        }

        public JsonResult countNewMessagesList(TokenModel model)
        {
            PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
            CountsModel counts = null;
            if (model.azure != null && model.azure != "" || person.type == Utils.Constants.UserTypeEnum.Operator) { counts = getDBHandler().getOperatorCompaniesCountMessages(); }
            return getJsonResult(null, counts);
        }
    }
}
