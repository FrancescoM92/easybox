﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.RemoteServers.ServiceBus;
using EasyBoxApplicationServer.RestAuthorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class MessageController : BaseController
    {
        [HttpGet]
        public JsonResult provaMessage()
        {
            string result = new ServiceBusMessaging().test();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult sendMessage(string json)
        {
            string exmsg = string.Empty;
            Tuple<long, string> response = new Tuple<long, string>(0,null);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            MessageRequest model = null;
            try
            {
                model = serializer.Deserialize<MessageRequest>(json);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }

            if (!string.IsNullOrEmpty(model.token) || !string.IsNullOrEmpty(model.azure))
            {
                PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
                if (ModelState.IsValid)
                {
                    response = getDBHandler().insertMessage(model.message, new UserTypeModel() { type = person.type, id = person.id });
                    exmsg = response.Item2;
                }
                else
                {
                    exmsg = getErrorMessage(ModelState.Values.ToList());
                }
            }
            return getJsonResult(response.Item2, response.Item1);
        }

        public JsonResult getMessages(string request)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            TokenModel token = serializer.Deserialize<TokenModel>(request);
            string exmsg = string.Empty;
            Tuple<List<MessageModel>, string> response = null;
            PersonModelBase person = getDBHandler().getUserFromToken(token.token, token.azure);
            if (ModelState.IsValid)
            {
                List<MessageModel> messages = new List<MessageModel>();
                List<Message> dbMessages = getDBHandler().getMessagesByEmployeeId(token.id).Item1;

                foreach (Message m in dbMessages)
                {
                    messages.Add(new MessageModel()
                    {
                        body = m.body,
                        employeeId = m.employeeId,
                        timeStamp = m.timeStamp,
                        idFrom = m.idFrom,
                        idTo = m.idTo,
                        sentByEmployee = m.sentByEmployee
                    });
                    if (m.idFrom != person.id && m.hasBeenRead != true)
                        exmsg = getDBHandler().setMessageRead(m);
                }
                response = new Tuple<List<MessageModel>, string>(messages, exmsg);
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }

            return getJsonResult(exmsg, response);
        }

        public JsonResult countNewMessages(TokenModel model)
        {
            PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
            string exmsg = string.Empty;
            Tuple<int, string> result = null;
            if (ModelState.IsValid) { result = getDBHandler().getCountNewMessages(person.type, model.id); }
            else { exmsg = getErrorMessage(ModelState.Values.ToList()); }
            return getJsonResult(exmsg, result);
        }

        public JsonResult countNewMessagesList(TokenModel model)
        {
            PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
            CountsModel counts = null;
            if (model.azure != null && model.azure != "" && person.type == Utils.Constants.UserTypeEnum.Operator) { counts = getDBHandler().getOperatorEmployeesCountMessages(); } 
            return getJsonResult(null, counts);
        }
    }
}