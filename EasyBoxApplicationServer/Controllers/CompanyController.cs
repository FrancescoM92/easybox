﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class CompanyController : BaseController
    {
        #region company
        public JsonResult getCompanyDetail(GetCompanyDetailModel model)
        {
            var DBHandler = new DBHandler();

            Tuple<Company, string> companyResult = null;
            GetCompanyDetailResponseModel response = null;
            Customer owner = null;
            List<EmployeeModel> employees = new List<EmployeeModel>();
            List<CustomerModel> customers = new List<CustomerModel>();

            companyResult = getDBHandler().getCompanyById(model.companyId);
            Service service = getDBHandler().getService((long)companyResult.Item1.serviceId).Item1;
            if (companyResult.Item1 != null && companyResult.Item1.ownerId != null && companyResult.Item1.ownerId > 0)
                owner = (getDBHandler().getCustomer((long)companyResult.Item1.ownerId)).Item1;
            string address = getDBHandler().getAddressByID((long)companyResult.Item1.addressId).Item1.street;
            string startDate = string.Empty;
            string endDate = string.Empty;
            Administration admin = new DBHandler().getAdministrationByCompanyID(model.companyId).Item1;
            if (admin != null)
            {
                startDate = admin.provisionStartDate.ToString();
                endDate = admin.provisionEndDate.ToString();
            }

            List<AWDocument> awDocs = DBHandler.getAWDocuments(new PersonModelBase()
            {
                type = Utils.Constants.UserTypeEnum.Owner,
                id = owner.ID
            }).Item1;


            List<Employee> employeesTemp = DBHandler.getCompanyEmployees(model.companyId).Item1;
            List<Customer> customersTemp = DBHandler.getCompanyCustomers(model.companyId).Item1;

            if (companyResult.Item1 != null)
            {
                response = new GetCompanyDetailResponseModel()
                {
                    address = address,
                    companyCode = companyResult.Item1.companyCode,
                    customers = new List<CustomerModel>(),
                    email = companyResult.Item1.email,
                    employees = new List<EmployeeModel>(),
                    startDate = startDate.Split(' ')[0],
                    endDate = endDate.Split(' ')[0],
                    id = companyResult.Item1.ID,
                    name = companyResult.Item1.businessName,
                    vat = companyResult.Item1.vat,
                    BBCompanyId = (int?)companyResult.Item1.BBId,
                    documents = awDocs
                };
                if (owner != null) response.owner = CustomerModel.Create(owner);

                if (employeesTemp != null)
                {
                    foreach (Employee e in employeesTemp)
                    {
                        employees.Add(EmployeeModel.Create(e));
                    }
                }
                response.employees = employees;
                if (customersTemp != null)
                {
                    foreach (Customer c in customersTemp)
                    {
                        customers.Add(CustomerModel.Create(c));
                    }
                }
                response.customers = customers;
            }
            response.service = service;
            return getJsonResult(companyResult.Item2, response);
        }

        public JsonResult setCompanyIsActive(SetCompanyIsActiveModel model)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
                exmsg = getDBHandler().setCompanyIsActive(model); // VA RIMOSSA DA BADGEBOX?!? SECONDO ME, NO
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());

            return getJsonResult(exmsg);
        }

        public JsonResult updateCompanyData(CompanyModel model)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
            {
                exmsg = getDBHandler().updateCompany(model);
            }
            else
                exmsg = getErrorMessage(ModelState.Values);

            return getJsonResult(exmsg);
        }

        public JsonResult getCompanyDetailChart(ChartModel model)
        {
            if (model.id == -1)
            {
                List<ChartResponse> responses = new List<ChartResponse>();
                PersonModelBase person = getDBHandler().getUserFromToken(model.token, model.azure);
                List<Company> companies = getDBHandler().getCustomerCompanies(person.id).Item1;
                string exmsg = string.Empty;
                foreach (Company c in companies)
                {
                    Tuple<ChartResponse, string> r = getDBHandler().getEngagedEmployees(model.year, c.ID);
                    responses.Add(r.Item1);
                    exmsg += r.Item2;
                }
                return getJsonResult(exmsg, responses);
            }
            else
            {
                Tuple<ChartResponse, string> r = getDBHandler().getEngagedEmployees(model.year, model.id);
                return getJsonResult(r.Item2, r.Item1);
            }

        }

        public JsonResult updateCompanyServices(ServiceModel service)
        {
            string exmsg = string.Empty;
            string Response = string.Empty;
            if (ModelState.IsValid)
            {
                Company comp = getDBHandler().getcompanyByServiceId(service.id).Item1;
                List<Customer> custs = getDBHandler().getCompanyCustomers(comp.ID).Item1;
                exmsg = getDBHandler().updateServiceStatus(service);
                foreach (Customer c in custs)
                {
                    if (c.serviceId != null && c.serviceId > 0)
                        getDBHandler().updateServiceStatusMod(service, (long)c.serviceId);
                }
                Response = "OK";
            }
            else
                exmsg = getErrorMessage(ModelState.Values);
            return getJsonResult(exmsg, Response);
        }

        #endregion

        #region temporary company
        public JsonResult updateTemporaryCompanyToCompany(UpdateTemporaryCompanyToCompanyModel model, bool isEasy = false)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
            {
                DBHandler dbHandler = getDBHandler();
                Tuple<Company, string> result = dbHandler.updateTemporaryCompanyToCompany(model.id, model.ownerId);
                Customer customer = dbHandler.getCustomer(model.ownerId).Item1;
                if (string.IsNullOrEmpty(result.Item2))
                {
                    EmailSender.sendEmailMichele("Owner abilitato", "Salve, è stato aggiunto " + customer.email + " come owner dell'azienda: " + result.Item1.businessName, isEasy);
                }
            }
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());

            return getJsonResult(exmsg, "OK");
        }
        #endregion
    }
}