﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.BadgeBox;
using EasyBoxApplicationServer.Models.BadgeBox.Requests;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.RemoteServers;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    public class PunchingController : BaseController
    {
        public JsonResult checkIn(BadgeBoxCheckInModel model)
        {
            string MESSAGE = string.Empty;
            Tuple<BadgeBoxGenericMessageResponse, string> response = null;
            if (ModelState.IsValid)
            {
                if (model.apikey != null && model.apikey.Equals(Constants.EASYBOX_APIKEY))
                {
                    response = BadgeBoxServer.checkIn(model);
                    MESSAGE = response.Item2;
                }
                else
                {
                    MESSAGE = "not authorized to access the service";
                }
            }
            else
            {
                MESSAGE = getErrorMessageBB(ModelState.Values);
            }
            if (response != null)
                return getJsonResult(MESSAGE, response.Item1);
            else
                return getJsonResult(MESSAGE, null);
        }

        public JsonResult checkOut(BadgeBoxCheckOutModel model)
        {
            string MESSAGE = string.Empty;
            Tuple<BadgeBoxGenericMessageResponse, string> response = null;
            if (ModelState.IsValid)
            {
                if (model.apikey != null && model.apikey.Equals(Constants.EASYBOX_APIKEY))
                {
                    response = BadgeBoxServer.checkOut(model);
                    MESSAGE = response.Item2;
                }
                else
                {
                    MESSAGE = "not authorized to access the service";
                }
            }
            else
            {
                MESSAGE = getErrorMessageBB(ModelState.Values);
            }
            if (response != null)
                return getJsonResult(MESSAGE, response.Item1);
            else
                return getJsonResult(MESSAGE, null);
        }

        public JsonResult insertRecord(BadgeBoxInsertRecordModel model)
        {
            string MESSAGE = string.Empty;
            string response = null;
            BadgeBoxErrorMessageResponse bberror = null;
            BadgeBoxGenericMessageResponse bbmessage = null;
            if (ModelState.IsValid)
            {
                try
                {
                    if (model.apikey != null && model.apikey.Equals(Constants.EASYBOX_APIKEY))
                    {
                        try
                        {
                            Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
                            Company comp = getDBHandler().getCompanyByEmployeeCode(emp.employeeCode).Item1;
                            Customer cust = getDBHandler().getOwner((long)comp.ownerId).Item1;
                            response = BadgeBoxServer.createRecordAPI(new BadgeBoxCreateRecordModel()
                            {
                                id_empl = (long)emp.BBId,
                                user_token = cust.BBToken,
                                checkin = model.checkin,
                                checkout = model.checkout
                            }).Item1;
                            if (response.Contains("ERROR"))
                            {
                                bberror = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxErrorMessageResponse>(response);
                                MESSAGE = bberror.ERROR.MESSAGE;
                            }
                            else
                                bbmessage = Newtonsoft.Json.JsonConvert.DeserializeObject<BadgeBoxGenericMessageResponse>(response);
                        }
                        catch (NullReferenceException)
                        {
                            MESSAGE = "The requested object is not existing";
                        }
                    }
                    else
                    {
                        MESSAGE = "not authorized to access the service";
                    }
                }
                catch (Exception e)
                {
                    MESSAGE = e.Message;
                }
            }
            else
            {
                MESSAGE = getErrorMessageBB(ModelState.Values);
            }
            return getJsonResult(MESSAGE, bbmessage);
        }

        public JsonResult deleteRecord(BadgeBoxDeleteRecordModel model)
        {
            string MESSAGE = string.Empty;
            BadgeBoxDeleteRecordResponse result = null;
            if (ModelState.IsValid)
            {
                if (model.apikey != null && model.apikey.Equals(Constants.EASYBOX_APIKEY))
                {
                    try
                    {
                        Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
                        Company comp = getDBHandler().getCompanyByEmployeeCode(emp.employeeCode).Item1;
                        Customer cust = getDBHandler().getOwner((long)comp.ownerId).Item1;
                        result = BadgeBoxServer.deleteRecord(new BadgeBoxDeleteRecordModel()
                        {
                            id = model.id,
                            id_empl = (long)emp.BBId,
                            user_token = cust.BBToken
                        }).Item1;
                        if (string.IsNullOrEmpty(result.MESSAGE)) result.MESSAGE = "Record already deleted or not existent";
                    }
                    catch (Exception)
                    {
                        MESSAGE = "The requested object is not existing";
                    }
                }
                else
                {
                    MESSAGE = "not authorized to access the service";
                }
            }
            else
            {
                MESSAGE = getErrorMessageBB(ModelState.Values);
            }
            return getJsonResult(MESSAGE, new BadgeBoxGenericMessageResponse() { MESSAGE = result.MESSAGE });
        }

        public JsonResult getMonthRecords(BadgeBoxGetRecordsModel model)
        {
            string MESSAGE = string.Empty;
            BadgeBoxGetRecordResponse records = null;
            BadgeBoxRecordsResponse result = new BadgeBoxRecordsResponse() { records = new List<BadgeBoxRecordResponse>() };

            if (ModelState.IsValid)
            {

                if (model.apikey != null && model.apikey.Equals(Constants.EASYBOX_APIKEY))
                {

                    Employee emp = getDBHandler().getEmployee(model.id_empl).Item1;
                    if (emp != null && emp.BBId != null && emp.BBId > 0)
                    {
                        Company comp = getDBHandler().getCompanyByEmployeeCode(emp.employeeCode).Item1;
                        Customer cust = getDBHandler().getOwner((long)comp.ownerId).Item1;
                        DateTime from = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        DateTime to = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month), 23, 59, 59);

                        records = BadgeBoxServer.getRecords(new BadgeBoxGetRecordsModel()
                        {
                            id_empl = (long)emp.BBId,
                            user_token = cust.BBToken,
                            from = from.ToString("yyyy-MM-dd HH:mm:ss"),
                            to = to.ToString("yyyy-MM-dd HH:mm:ss")
                        }).Item1;
                        if (records != null)
                            foreach (Record r in records.records)
                            {
                                result.records.Add(new BadgeBoxRecordResponse()
                                {
                                    id = r.id,
                                    checkin = r.checkin,
                                    checkout = r.checkout,
                                    day = r.day
                                });
                            }
                    }
                    else
                    {
                        MESSAGE = "The requested object is not existing";
                    }
                }
                else
                {
                    MESSAGE = "not authorized to access the service";
                }
            }
            else
            {
                MESSAGE = getErrorMessage(ModelState.Values.ToList());
            }
            return getJsonResult(MESSAGE, result);
        }
    }
}