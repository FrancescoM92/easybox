﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class EmployeesController : BaseController
    {
        public JsonResult getCustomerEmployees(TokenModel model)
        {
            GetEmployeesResponseModel response = null;
            string exmsg = string.Empty;

            if (ModelState.IsValid)
            {
                Tuple<List<Employee>, string> result = getDBHandler().getCustomerEmployees(model);
                List<EmployeeResponseModel> employees = new List<EmployeeResponseModel>();

                if (string.IsNullOrEmpty(result.Item2) && result.Item1 != null)
                {
                    foreach (Employee employee in result.Item1)
                    {
                        employees.Add(new EmployeeResponseModel()
                        {
                            email = employee.email,
                            id = employee.ID,
                            lastName = employee.lastName,
                            name = employee.name,
                            phone = employee.phone
                        });
                    }
                    response = new GetEmployeesResponseModel() { employees = employees };
                }
                else
                    exmsg = result.Item2;
            }
            else
                exmsg = getErrorMessage(ModelState.Values);

            return getJsonResult(exmsg, response);
        }

        public JsonResult getMonthlyEmployees(TokenModel model)
        {
            List<EmployeeResponseModel> response = new List<EmployeeResponseModel>();
            string exmsg = string.Empty;

            Tuple<List<EmployeeResponseModel>, string> result = getDBHandler().getCompanyMonthlyEmployees(model);

            if (string.IsNullOrEmpty(result.Item2) && result.Item1 != null)
            {
                response = result.Item1;
            }
            else
                exmsg = result.Item2;


            return getJsonResult(exmsg, response);
        }

        public JsonResult getEmployeesProfilePhoto(string json)
        {
            string exmsg = string.Empty;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            IdsRequest model = null;
            try
            {
                model = serializer.Deserialize<IdsRequest>(json);
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }

            string image = string.Empty;
            List<Tuple<string, long>> response = new List<Tuple<string, long>>();
            if (ModelState.IsValid)
            {
                foreach (int i in model.ids)
                {
                    Media media = new DBHandler().getMediaByEmployeeId(i).Item1;
                    string base64image = string.Empty;
                    if (media != null) base64image = Convert.ToBase64String(media.data);
                    response.Add(new Tuple<string, long>(base64image, i));
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
            }
            return getJsonResult(exmsg, response);
        }

        public JsonResult getOperatorEmployees(TokenModel model)
        {
            List<EmployeeResponseModel> response = new List<EmployeeResponseModel>();
            string exmsg = string.Empty;

            Tuple<List<EmployeeResponseModel>, string> result = null;

            if (!string.IsNullOrEmpty(model.azure))
            {
                List<string> groups = MyHelper.getUserBusinessUnits(model.azure);
                if (groups == null) exmsg = "L'utente non appartiene a nessuna filiale";
                result = getDBHandler().getOperatorEmployees(groups);
            }
            else
            {
                result = getDBHandler().getOperatorEmployees();
            }

            response = result.Item1;
            exmsg = result.Item2;

            return getJsonResult(exmsg, response);
        }
        
    }
}