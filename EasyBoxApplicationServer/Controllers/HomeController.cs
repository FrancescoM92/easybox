﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.Requests;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RestAuthorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static EasyBoxApplicationServer.Utils.Constants;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class HomeController : BaseController
    {
        public JsonResult getMain(TokenModel model)
        {
            string exmsg = string.Empty;
            if (model.azure != null && model.azure != "")
            {
                List<CompanySummaryResponseModel> responseCompanies = null;
                Tuple<List<CompanySummaryResponseModel>, string> result = getDBHandler().getCompaniesSummary();
                if (string.IsNullOrEmpty(result.Item2) && result.Item1 != null)
                    responseCompanies = result.Item1;
                return getJsonResult(result.Item2, result.Item1);
            }
            PersonModelBase person = getDBHandler().getUserFromToken(model.token, null);
            if (ModelState.IsValid)
            {
                DBHandler dbh = new DBHandler();
                switch (person.type)
                {
                    case UserTypeEnum.Operator:
                        List<CompanySummaryResponseModel> responseCompanies = null;
                        Tuple<List<CompanySummaryResponseModel>, string> result = getDBHandler().getCompaniesSummary();
                        if (string.IsNullOrEmpty(result.Item2) && result.Item1 != null)
                            responseCompanies = result.Item1;
                        return getJsonResult(result.Item2, result.Item1);
                    case UserTypeEnum.Customer:
                        List<Company> companies = dbh.getCustomerCompanies(dbh.getCustomerByUsername(person.email).Item1.ID).Item1;
                        List<GetCompanyDetailResponseModel> details = new List<GetCompanyDetailResponseModel>();
                        foreach (Company c in companies) details.Add(dbh.getCompanyDetail(c.ID).Item1);
                        return getJsonResult(string.Empty, details);
                    case UserTypeEnum.Employee:
                        GetEmployeeDetailResponseModel response = null;
                        Employee employee = dbh.getEmployeeByUsername(person.email).Item1;
                        response = dbh.getEmployeeDetail(employee.ID).Item1;
                        return getJsonResult(string.Empty, response);
                    case UserTypeEnum.Owner:
                        List<Company> companiesOwner = dbh.getCustomerCompanies(dbh.getCustomerByUsername(person.email).Item1.ID).Item1;
                        List<GetCompanyDetailResponseModel> detailsOwner = new List<GetCompanyDetailResponseModel>();
                        foreach (Company c in companiesOwner) detailsOwner.Add(dbh.getCompanyDetail(c.ID).Item1);
                        return getJsonResult(string.Empty, detailsOwner);
                    default:
                        return getJsonResult("Tipo utente non valido", null);
                }
            }
            else
            {
                exmsg = getErrorMessage(ModelState.Values.ToList());
                return Json(exmsg);
            }
        }

        public JsonResult getPermissions(TokenModel model)
        {
            if (!string.IsNullOrEmpty(model.azure)) return getJsonResult(null, PermissionModel.Create(UserTypeEnum.Operator));
            PersonModelBase person = getDBHandler().getUserFromToken(model.token, null);
            PermissionModel perModel = PermissionModel.Create(person.type);

            if (person.type.Equals(UserTypeEnum.Customer) || person.type.Equals(UserTypeEnum.Owner))
            {
                perModel.service = getDBHandler().getServiceByCustomerId(person.id).Item1;
            }
            else if(person.type.Equals(UserTypeEnum.Employee))
            {
                Company comp = getDBHandler().getCompanyById(person.secondaryId).Item1;
                perModel.service = getDBHandler().getService((long)comp.serviceId).Item1;
            }
            return getJsonResult(null, perModel);
        }

        public JsonResult getFunctionsPermissions(IdRequest model)
        { 
            Tuple<Service, string> result = null;
            if (model.id != 0)
                result = getDBHandler().getService(model.id);
            return getJsonResult(result.Item2, result.Item1);
        }

        
    }
}
