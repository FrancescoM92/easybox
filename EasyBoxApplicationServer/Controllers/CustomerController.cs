﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.BadgeBox.Models;
using EasyBoxApplicationServer.Models.Responses;
using EasyBoxApplicationServer.RemoteServers;
using EasyBoxApplicationServer.RestAuthorization;
using EasyBoxApplicationServer.Services;
using EasyBoxApplicationServer.Utils;
using System;
using System.Linq;
using System.Web.Mvc;

namespace EasyBoxApplicationServer.Controllers
{
    [RESTAuthorize]
    public class CustomerController : BaseController
    {
        public JsonResult insertCustomer(CustomerModel model)
        {
            string exmsg = string.Empty;
            InsertPersonResponseModel respModel = null;

            if (ModelState.IsValid)
            {
                DBHandler dbHandler = getDBHandler();
                PersonModelBase person = dbHandler.getUserFromCredential(model.email).Item1;
                if (person != null) return getJsonResult("Utente già esistente, utilizzare un'altra email", null);
                /* encrypt password if available */
                string psw = "";

                if (string.IsNullOrEmpty(model.password))
                {
                    psw = MyHelper.CreateNewPassword(8);
                    model.password = MyHelper.GetEncryptedPassword(psw);
                }
                else
                {
                    model.password = MyHelper.GetEncryptedPassword(model.password);
                }

                long feaId = 0;
                if (model.email != null && !string.IsNullOrEmpty(model.phone))
                {
                    feaId = new DBHandler().insertFEA(new FirmaElettronicaAvanzata()
                    {
                        cellulare = model.phone,
                        email = model.email
                    }).Item1;
                }
                else
                {
                    return getJsonResult("Numero di telefono ed email obbligatori");
                }
                /* insert into EB */
                Tuple<long, string> result = dbHandler.insertCustomer(model, feaId);

                if (string.IsNullOrEmpty(result.Item2))
                {
                    respModel = new InsertPersonResponseModel()
                    {
                        customerId = result.Item1
                    };
                    EmailSender.sendEmail(model.email, "Nuova Utenza", "Salve, la sua nuova utenza è stata creata ed abilitata, User: " + model.email + ", Password: " + psw, true);
                }
                else
                {
                    exmsg = result.Item2;
                }
            }
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());

            return getJsonResult(exmsg, respModel);
        }

        public JsonResult setCustomerIsActive(SetCustomerIsActiveModel model)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
                exmsg = getDBHandler().setCustomerIsActive(model);
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());

            return getJsonResult(exmsg);
        }

        public JsonResult updateCustomer(CustomerModel model)
        {
            string exmsg = string.Empty;

            if (ModelState.IsValid)
            {
                exmsg = getDBHandler().updateCustomer(model);
                if (string.IsNullOrEmpty(exmsg))
                {
                    exmsg = BadgeBoxServer.updateUser(BadgeBoxUpdateUserModel.Create(model)).Item2;
                }
            }
            else
                exmsg = getErrorMessage(ModelState.Values.ToList());

            if (string.IsNullOrEmpty(exmsg)) return getJsonResult(exmsg);
            else return getJsonResult(null, "OK");

        }

        public JsonResult updateCustomerServices(ServiceModelRequest model)
        {
            string exmsg = string.Empty;
            string Response = string.Empty;
            if (ModelState.IsValid)
            {
                exmsg = getDBHandler().updateServiceStatus(model.service);
                Response = "OK";
            }
            else
                exmsg = getErrorMessage(ModelState.Values);
            return getJsonResult(exmsg, Response);
        }

        public JsonResult updateCustomerPassword(ResetPasswordModel model, bool isEasy = false)
        {
            string result = string.Empty;
            try
            {
                PersonModelBase person = getDBHandler().getUserFromToken(model.token, null);
                Customer cust = null;
                if (model.id > 0)
                {
                    cust = getDBHandler().getCustomer(model.id).Item1;
                }
                else
                {
                    cust = getDBHandler().getCustomer(person.id).Item1;
                }

                if (MyHelper.GetEncryptedPassword(model.oldPassword).Equals(cust.password))
                {
                    result = getDBHandler().updateUserPasswordCustomer(cust.email, model.newPassword);
                    if (string.IsNullOrEmpty(result))
                    {
                        EmailSender.sendEmail(cust.email, "Cambio Password", "Complimenti, la sua password è stata cambiata con successo, ora può accedere all'area riservata", isEasy);
                        return getJsonResult(null, "OK");
                    }
                }
                else
                {
                    return getJsonResult(null, "Password errata");
                }
            }
            catch (Exception e)
            {
                result = e.Message;
            }

            return getJsonResult(result, null);
        }
    }
}