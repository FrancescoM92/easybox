﻿using EasyBoxApplicationServer.Controllers;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.ServiceBus;
using EasyBoxApplicationServer.Utils;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace EasyBoxApplicationServer.RemoteServers
{
    public class ServiceBusHandler : BaseController
    {
        private string connectionString, entityPath, topicName, topicPath, subscriptionName;
        NameValueCollection config = ConfigurationManager.AppSettings;
        public ServiceBusHandler()
        {
            topicName = config.Get("ServiceBus.TopicName");
            topicPath = config.Get("ServiceBus.TopicPath");
            connectionString = config.Get("Microsoft.ServiceBus.ConnectionString");
            entityPath = string.Format("{0}/subscriptions/", topicName);
            subscriptionName = config.Get("ServiceBus.SubscriptionName");

            if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeLavoratore"))) config.Set("ServiceBus.LastMessageTimeLavoratore", DateTime.MinValue.ToString());
            if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeCliente"))) config.Set("ServiceBus.LastMessageTimeCliente", DateTime.MinValue.ToString());
            if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeAssunzione"))) config.Set("ServiceBus.LastMessageTimeAssunzione", DateTime.MinValue.ToString());
            if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeSomministrazione"))) config.Set("ServiceBus.LastMessageTimeSomministrazione", DateTime.MinValue.ToString());
            if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeUnitaDiBusiness"))) config.Set("ServiceBus.LastMessageTimeUnitaDiBusiness", DateTime.MinValue.ToString());

        }

        //public void initMessagesTest()
        //{
        //    SubscriptionClient subscriptionClient = SubscriptionClient.CreateFromConnectionString(connectionString, topicName, subscriptionName);

        //    OnMessageOptions options = new OnMessageOptions()
        //    {
        //        AutoComplete = false,
        //        MaxConcurrentCalls = 1,
        //        AutoRenewTimeout = TimeSpan.FromMinutes(5)
        //    };
        //    options.ExceptionReceived += (s, ex) =>
        //    {
        //        Debug.WriteLine("Exception occurred in OnMessage: {0}" + ex.ToString());
        //    };
        //    subscriptionClient.OnMessage(
        //        message =>
        //        {
        //            string error = string.Empty;
        //            bool complete = false;
        //            try
        //            {
        //                Debug.Write(" ****** LABEL: " + message.Label + " ****** ");

        //                Stream stream = message.GetBody<Stream>();
        //                if (stream != null)
        //                {

        //                    JToken json = JToken.Parse(new StreamReader(stream, true).ReadToEnd());

        //                    Debug.Write("****** BODY: " + json.ToString() + " ****** ");

        //                    complete = true;
        //                }
        //                else
        //                {
        //                    Object obj = message.GetBody<Object>();
        //                    Debug.Write(obj.ToString());
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                error = e.Message;
        //            }
        //            if (complete)
        //                message.Complete();
        //            else
        //                message.Abandon();
        //        },
        //        options);
        //}

        public void initMessages()
        {
            SubscriptionClient subscriptionClient = SubscriptionClient.CreateFromConnectionString(connectionString, topicName, subscriptionName);

            if (subscriptionClient == null)
            {
                MyHelper.Log("IMPOSSIBILE CREARE CONNESSIONE DA STRINGA: {}, TOPIC: {}, SOTTOSCRIZIONE: {}");
            }

            OnMessageOptions options = new OnMessageOptions()
            {
                AutoComplete = false,
                MaxConcurrentCalls = 1,
                AutoRenewTimeout = TimeSpan.FromMinutes(5)
            };
            options.ExceptionReceived += (s, ex) =>
            {
                MyHelper.Log("Exception occurred in OnMessage: " + s.ToString() + " ERRORE: " + ex.Exception.StackTrace);
            };

            subscriptionClient.OnMessageAsync(
                 async message =>
                 {
                     //executeMessage(message);
                     Stream stream = message.GetBody<Stream>();
                     JToken json = JToken.Parse(new StreamReader(stream, true).ReadToEnd());

                     bool complete = false;
                     if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("LavoratoreUtilizzabile", StringComparison.InvariantCultureIgnoreCase))
                     {
                         MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
                         MyHelper.LogSBMessage(json.ToString());
                         if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeLavoratore"))) > 0)
                         {
                             Tuple<long, string> result = null;
                             json = json.First.First;
                             var body = json.ToObject<Lavoratore>();

                             if (body != null)
                                 result = getDBHandler().insertEmployee(EmployeeModel.CreateEmployee(body));
                             if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeLavoratore", message.EnqueuedTimeUtc.ToString());
                             if (!string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);
                             if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeLavoratore"))) config.Set("ServiceBus.LastMessageTimeLavoratore", DateTime.MinValue.ToString());
                             complete = true;
                         }
                     }
                     else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("ClienteUtilizzabile", StringComparison.InvariantCultureIgnoreCase))
                     {
                         MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
                         MyHelper.LogSBMessage(json.ToString());
                         if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeCliente"))) > 0)
                         {
                             Tuple<long, string> result = null;
                             json = json.First.First;
                             JArray jar = new JArray();
                             var tok = json.SelectToken("ElencoSedi");
                             if (tok != null && ((tok is JArray == false) || !tok.ToString().Contains("[")))
                             {
                                 jar.Add(tok);
                                 json.SelectToken("ElencoSedi").Replace(jar);
                             }
                             var body = json.ToObject<Cliente>();
                             if (body != null)
                                 result = getDBHandler().insertTemporaryCompany(CompanyModel.Create(body));
                             if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeCliente", message.EnqueuedTimeUtc.ToString());
                             if (!string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);
                             if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeCliente"))) config.Set("ServiceBus.LastMessageTimeCliente", DateTime.MinValue.ToString());
                             complete = true;
                         }
                     }
                     else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("AssunzioneConsolidata", StringComparison.InvariantCultureIgnoreCase))
                     {
                         MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
                         MyHelper.LogSBMessage(json.ToString());
                         if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeAssunzione"))) > 0)
                         {
                             Tuple<long, string> result = null;

                             json = json.First.First;
                             JArray jar = new JArray();
                             var tok = json.SelectToken("Proroga");
                             if (tok != null && ((tok is JArray == false) || !tok.ToString().Contains("[")))
                             {
                                 jar.Add(tok);
                                 json.SelectToken("Proroga").Replace(jar);
                             }
                             var body = json.ToObject<Assunzione>();
                             if (body != null)
                                 result = getDBHandler().insertEngagement(EngagementModel.Create(body));
                             if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeAssunzione", message.EnqueuedTimeUtc.ToString());
                             if (!string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);

                             if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeAssunzione"))) config.Set("ServiceBus.LastMessageTimeAssunzione", DateTime.MinValue.ToString());
                             complete = true;
                         }
                     }
                     else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("SomministrazioneConsolidata", StringComparison.InvariantCultureIgnoreCase))
                     {
                         MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
                         MyHelper.LogSBMessage(json.ToString());
                         if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeSomministrazione"))) > 0)
                         {
                             Tuple<long, string> result = null;

                             json = json.First.First;
                             JArray jar = new JArray();
                             var tok = json.SelectToken("Proroga");
                             if (tok != null && ((tok is JArray == false) || !tok.ToString().Contains("[")))
                             {
                                 jar.Add(tok);
                                 json.SelectToken("Proroga").Replace(jar);
                             }
                             var body = json.ToObject<Somministrazione>();
                             if (body != null)
                                 result = getDBHandler().insertAdministration(AdministrationModel.Create(body));
                             if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeSomministrazione", message.EnqueuedTimeUtc.ToString());
                             if (!string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);

                             if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeSomministrazione"))) config.Set("ServiceBus.LastMessageTimeSomministrazione", DateTime.MinValue.ToString());
                             complete = true;
                         }
                     }
                     else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("NuovaUnitaDiBusiness", StringComparison.InvariantCultureIgnoreCase))
                     {
                         MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
                         MyHelper.LogSBMessage(json.ToString());
                         if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeUnitaDiBusiness"))) > 0)
                         {
                             Tuple<long, string> result = null;
                             json = json.First.First;
                             var body = json.ToObject<BusinessUnit>();
                             if (body != null)
                                 result = getDBHandler().insertBusinessUnit(BusinessUnitModel.Create(body));
                             if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeUnitaDiBusiness", message.EnqueuedTimeUtc.ToString());
                             if (!string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);

                             if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeUnitaDiBusiness"))) config.Set("ServiceBus.LastMessageTimeUnitaDiBusiness", DateTime.MinValue.ToString());
                             complete = true;
                         }
                     }
                     else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("MessaggioCorrelazioneDocumenti", StringComparison.InvariantCultureIgnoreCase))
                     {
                         MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
                         MyHelper.LogSBMessage(json.ToString());
                         MyHelper.Log("MESSAGGIO_CORRELAZIONE_DOCUMENTI START");
                         json = json.First.First;
                         Documenti body = null;
                         try
                         {
                             body = json.ToObject<Documenti>();
                         }
                         catch
                         {
                         }
                         if (body == null)
                         {
                             MyHelper.Log("BODY NULL, PROVO BODY SIN");
                             var body2 = json.ToObject<Documenti_sin>();
                             if (body2 != null)
                             {
                                 MyHelper.Log("BODY SORGENTE: " + body2.Sorgente.ToString());
                                 Documenti doc = new Documenti().Create(body2);
                                 if (doc != null) MyHelper.Log("DOC OK");
                                 getDBHandler().pollingAWDocuments(doc);
                                 config.Set("ServiceBus.LastMessageTimeConfermaDocumenti", message.EnqueuedTimeUtc.ToString());
                             }
                         }
                         else
                         {
                             MyHelper.Log("BODY SORGENTE: " + body.Sorgente.ToString());
                             getDBHandler().pollingAWDocuments(body);
                             config.Set("ServiceBus.LastMessageTimeConfermaDocumenti", message.EnqueuedTimeUtc.ToString());
                         }
                         complete = true;
                         MyHelper.Log("MESSAGGIO_CORRELAZIONE_DOCUMENTI END");
                     }
                     if (complete)
                         await message.CompleteAsync();
                     else
                         await message.AbandonAsync();
                 },
                options);
        }


        //private async void executeMessage(BrokeredMessage message)
        //{
        //    await new Task(async () =>
        //    {
        //        Stream stream = message.GetBody<Stream>();
        //        JToken json = JToken.Parse(new StreamReader(stream, true).ReadToEnd());

        //        bool complete = false;
        //        if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("LavoratoreUtilizzabile", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
        //            MyHelper.LogSBMessage(json.ToString());
        //            if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeLavoratore"))) > 0)
        //            {
        //                Tuple<long, string> result = null;
        //                json = json.First.First;
        //                var body = json.ToObject<Lavoratore>();

        //                if (body != null)
        //                    result = getDBHandler().insertEmployee(EmployeeModel.CreateEmployee(body));
        //                if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeLavoratore", message.EnqueuedTimeUtc.ToString());
        //                if (!string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);
        //                if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeLavoratore"))) config.Set("ServiceBus.LastMessageTimeLavoratore", DateTime.MinValue.ToString());
        //                complete = true;
        //            }
        //        }
        //        else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("ClienteUtilizzabile", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
        //            MyHelper.LogSBMessage(json.ToString());
        //            if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeCliente"))) > 0)
        //            {
        //                Tuple<long, string> result = null;
        //                json = json.First.First;
        //                JArray jar = new JArray();
        //                var tok = json.SelectToken("ElencoSedi");
        //                if (tok != null && ((tok is JArray == false) || !tok.ToString().Contains("[")))
        //                {
        //                    jar.Add(tok);
        //                    json.SelectToken("ElencoSedi").Replace(jar);
        //                }
        //                var body = json.ToObject<Cliente>();
        //                if (body != null)
        //                    result = getDBHandler().insertTemporaryCompany(CompanyModel.Create(body));
        //                if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeCliente", message.EnqueuedTimeUtc.ToString());
        //                if (!string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);
        //                if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeCliente"))) config.Set("ServiceBus.LastMessageTimeCliente", DateTime.MinValue.ToString());
        //                complete = true;
        //            }
        //        }
        //        else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("AssunzioneConsolidata", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
        //            MyHelper.LogSBMessage(json.ToString());
        //            if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeAssunzione"))) > 0)
        //            {
        //                Tuple<long, string> result = null;

        //                json = json.First.First;
        //                JArray jar = new JArray();
        //                var tok = json.SelectToken("Proroga");
        //                if (tok != null && ((tok is JArray == false) || !tok.ToString().Contains("[")))
        //                {
        //                    jar.Add(tok);
        //                    json.SelectToken("Proroga").Replace(jar);
        //                }
        //                var body = json.ToObject<Assunzione>();
        //                if (body != null)
        //                    result = getDBHandler().insertEngagement(EngagementModel.Create(body));
        //                if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeAssunzione", message.EnqueuedTimeUtc.ToString());
        //                if (string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);

        //                if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeAssunzione"))) config.Set("ServiceBus.LastMessageTimeAssunzione", DateTime.MinValue.ToString());
        //                complete = true;
        //            }
        //        }
        //        else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("SomministrazioneConsolidata", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
        //            MyHelper.LogSBMessage(json.ToString());
        //            if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeSomministrazione"))) > 0)
        //            {
        //                Tuple<long, string> result = null;

        //                json = json.First.First;
        //                JArray jar = new JArray();
        //                var tok = json.SelectToken("Proroga");
        //                if (tok != null && ((tok is JArray == false) || !tok.ToString().Contains("[")))
        //                {
        //                    jar.Add(tok);
        //                    json.SelectToken("Proroga").Replace(jar);
        //                }
        //                var body = json.ToObject<Somministrazione>();
        //                if (body != null)
        //                    result = getDBHandler().insertAdministration(AdministrationModel.Create(body));
        //                if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeSomministrazione", message.EnqueuedTimeUtc.ToString());
        //                if (string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);

        //                if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeSomministrazione"))) config.Set("ServiceBus.LastMessageTimeSomministrazione", DateTime.MinValue.ToString());
        //                complete = true;
        //            }
        //        }
        //        else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("NuovaUnitaDiBusiness", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
        //            MyHelper.LogSBMessage(json.ToString());
        //            if (message.EnqueuedTimeUtc.CompareTo(DateTime.Parse(config.Get("ServiceBus.LastMessageTimeUnitaDiBusiness"))) > 0)
        //            {
        //                Tuple<long, string> result = null;
        //                json = json.First.First;
        //                var body = json.ToObject<BusinessUnit>();
        //                if (body != null)
        //                    result = getDBHandler().insertBusinessUnit(BusinessUnitModel.Create(body));
        //                if (result.Item1 > 0) config.Set("ServiceBus.LastMessageTimeUnitaDiBusiness", message.EnqueuedTimeUtc.ToString());
        //                if (string.IsNullOrEmpty(result.Item2)) MyHelper.Log("ERRORE: " + result.Item2);

        //                if (string.IsNullOrEmpty(config.Get("ServiceBus.LastMessageTimeUnitaDiBusiness"))) config.Set("ServiceBus.LastMessageTimeUnitaDiBusiness", DateTime.MinValue.ToString());
        //                complete = true;
        //            }
        //        }
        //        else if (!string.IsNullOrEmpty(message.Label) && !string.IsNullOrEmpty(message.ContentType) && message.Label.Equals("MessaggioCorrelazioneDocumenti", StringComparison.InvariantCultureIgnoreCase))
        //        {
        //            MyHelper.LogSBMessage(message.Label + " MESSAGGIO: ");
        //            MyHelper.LogSBMessage(json.ToString());
        //            MyHelper.Log("MESSAGGIO_CORRELAZIONE_DOCUMENTI START");
        //            json = json.First.First;
        //            Documenti body = null;
        //            try
        //            {
        //                body = json.ToObject<Documenti>();
        //            }
        //            catch
        //            {
        //            }
        //            if (body == null)
        //            {
        //                MyHelper.Log("BODY NULL, PROVO BODY SIN");
        //                var body2 = json.ToObject<Documenti_sin>();
        //                if (body2 != null)
        //                {
        //                    MyHelper.Log("BODY SORGENTE: " + body2.Sorgente.ToString());
        //                    Documenti doc = new Documenti().Create(body2);
        //                    if (doc != null) MyHelper.Log("DOC OK");
        //                    getDBHandler().pollingAWDocuments(doc);
        //                    config.Set("ServiceBus.LastMessageTimeConfermaDocumenti", message.EnqueuedTimeUtc.ToString());
        //                }
        //            }
        //            else
        //            {
        //                MyHelper.Log("BODY SORGENTE: " + body.Sorgente.ToString());
        //                getDBHandler().pollingAWDocuments(body);
        //                config.Set("ServiceBus.LastMessageTimeConfermaDocumenti", message.EnqueuedTimeUtc.ToString());
        //            }
        //            complete = true;
        //            MyHelper.Log("MESSAGGIO_CORRELAZIONE_DOCUMENTI END");
        //        }
        //        if (complete)
        //            await message.CompleteAsync();
        //        else
        //            await message.AbandonAsync();
        //    });
        //}
    }
}