﻿using AWTech.AWDoc.Dao;
using AWTech.AWDoc.Dao.Rest;
using AWTech.AWDoc.Protocolbuffer;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyBoxApplicationServer.RemoteServers.AWDoc
{
    public class AWTest
    {
        private Uri gdsUri = new Uri(Constants.AWDOC_URI);
        private string organization = Constants.AWDOC_ORGANIZATION;
        private string username = Constants.AWDOC_USERNAME;
        private string password = Constants.AWDOC_PASSWORD;
        private string domain = Constants.AWDOC_DOMAIN;
        private string category = Constants.AWDOC_EASYBOXCATEGORYUUID;

        public List<Category> getcategories()
        {
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    factory.Initialize(gdsUri, organization, username, password);
                }
                ICategoryDao cd = factory.GetCategoryDao();
                DaoResult result = cd.GetCategories(domain, out List<Category> categories);
                if (result.Equals(DaoResult.OK))
                {
                    foreach(Category c in categories)
                    {
                        Console.WriteLine(c.Name + " - UUID: " + c.Uuid);
                    }
                }
                return categories;
            }
        }
    }
}