﻿using AWTech.AWDoc.Dao;
using AWTech.AWDoc.Dao.Rest;
using AWTech.AWDoc.Protocolbuffer;
using EasyBoxApplicationServer.Models;
using EasyBoxApplicationServer.Models.AWDoc;
using EasyBoxApplicationServer.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static AWTech.AWDoc.Protocolbuffer.Document.Types;

namespace EasyBoxApplicationServer.RemoteServers.AWDoc
{
    public class AWDocHandler
    {
#if DEBUG
        private Uri gdsUri = new Uri(Constants.AWDOC_URI);
        private string organization = Constants.AWDOC_ORGANIZATION;
        private string username = Constants.AWDOC_USERNAME;
        private string password = Constants.AWDOC_PASSWORD;
        private string domain = Constants.AWDOC_DOMAIN;
        private string category_adm = Constants.AWDOC_EASYBOXCATEGORYUUID;
        private string category_eng = Constants.AWDOC_EASYBOXCATEGORYUUID;
#else
        private Uri gdsUri = new Uri(Constants.AWDOC_URI_RELEASE);
        private string organization = Constants.AWDOC_ORGANIZATION_RELEASE;
        private string username = Constants.AWDOC_USERNAME_RELEASE;
        private string password = Constants.AWDOC_PASSWORD_RELEASE;
        private string domain = Constants.AWDOC_DOMAIN_RELEASE;
        private string category_adm = Constants.AWDOC_EASYBOXCATEGORYUUID_ADMINISTRATION_RELEASE;
        private string category_eng = Constants.AWDOC_EASYBOXCATEGORYUUID_ENGAGEMENT_RELEASE; //TODO
#endif
        public List<Document> GetDocuments(List<string> documentsUUID)
        {
            List<Document> docs = new List<Document>();
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    factory.Initialize(gdsUri, organization, username, password);
                }
                IDocumentDao dao = factory.GetDocumentDao();

                foreach (string s in documentsUUID)
                {
                    DaoResult result = dao.GetDocument(domain, new Guid(s), out Document doc);
                    if (result.Equals(DaoResult.OK)) docs.Add(doc);
                }
                return docs;
            }
        }

        public List<Document> GetDocumentsByAW(List<Database.AWDocument> documents, PersonModelBase person)
        {
            MyHelper.Log("GetDocumentsByAW");

            List<Document> docs = new List<Document>();
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    MyHelper.Log("INIZIALIZZO FACTORY AWDOC DAO");
                    factory.Initialize(gdsUri, organization, username, password);
                }
                IDocumentDao dao = factory.GetDocumentDao();

                foreach (Database.AWDocument d in documents)
                {
                    MyHelper.Log("UID PRIMA DELLA RICERCA DAO AWDOC : " + d.UID.ToString());
                    DaoResult result = dao.GetDocument(domain, d.UID, out Document toUpdate);
                    MyHelper.Log("AWDOC DAO RESULT: " + result.ToString());
                    if (result.Equals(DaoResult.OK))
                    {
                        docs.Add(toUpdate);
                        new Database.DBHandler().updateAWDocument(d.UID, toUpdate);
                    }
                }
                return docs;
            }
        }

        public void updateDocumentByUUID(Guid UID)
        {
            MyHelper.Log("updateDocumentByUUID UUID: " + UID.ToString());

            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    MyHelper.Log("INIZIALIZZO FACTORY AWDOC DAO");
                    factory.Initialize(gdsUri, organization, username, password);
                }
                IDocumentDao dao = factory.GetDocumentDao();

                MyHelper.Log("UID PRIMA DELLA RICERCA DAO AWDOC : " + UID.ToString());

                DaoResult result = dao.GetDocument(domain, UID, out Document toUpdate);
                MyHelper.Log("AWDOC DAO RESULT: " + result.ToString());

                if (result.Equals(DaoResult.OK))
                {
                    new Database.DBHandler().updateAWDocument(UID, toUpdate);
                }
            }
        }

        public Document GetDocument(string docGUID)
        {
            MyHelper.Log("GetDocument");
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    factory.Initialize(gdsUri, organization, username, password);
                }
                IDocumentDao dao = factory.GetDocumentDao();

                DaoResult result = dao.GetDocument(domain, new Guid(docGUID), out Document doc);
                if (result.Equals(DaoResult.OK)) return doc;
                return null;
            }
        }

        public bool DomainExist()
        {
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    DaoResult initRes = factory.Initialize(gdsUri, organization, username, password);
                    if (!initRes.Equals(DaoResult.OK))
                    {
                        Console.WriteLine(initRes);
                        return false;
                    }
                }
                IDomainDao dd = factory.GetDomainDao();
                DaoResult result = dd.GetDomains(out DirectoryServiceMessage dsm);
                if (result.Equals(DaoResult.OK))
                {
                    if (dsm.Domains.Values.Any(userDomainAssociation => userDomainAssociation.EntityUuid == domain)) return true;
                    else return false;
                }
                else return false;
            }
        }

        public List<Category> GetCategories()
        {
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    factory.Initialize(gdsUri, organization, username, password);
                }
                ICategoryDao cd = factory.GetCategoryDao();
                DaoResult result = cd.GetCategories(domain, out List<Category> categories);
                if (result.Equals(DaoResult.OK))
                {
                    return categories;
                }
                return new List<Category>();
            }
        }

        public bool isAWDocDateTimeValid(long from, long to)
        {
            //da rivedere
            bool result = false;
            DateTime dateFrom = MyHelper.GetDateTimeFromTimeStamp(from / 1000);
            DateTime dateTo = MyHelper.GetDateTimeFromTimeStamp(to / 1000);
            if (dateTo > dateFrom)
            {
                if (dateFrom.Year >= (DateTime.Now.Year - 40) && dateFrom.Year <= (DateTime.Now.Year + 20))
                    if (dateTo.Year >= (DateTime.Now.Year - 40) && dateTo.Year <= (DateTime.Now.Year + 20))
                        result = true;
                if (dateFrom.Year >= 2000 && dateFrom.Year <= (DateTime.Now.Year + 20) && dateTo.Year < dateFrom.Year || dateTo.Year > DateTime.Now.Year + 100)
                    result = true;
            }
            return result;
        }

        public List<Document> GetDocumentsInCategory(GetAWDocumentModel model)
        {
            List<Document> all = null;
            List<Document> notSigned = new List<Document>();
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    factory.Initialize(gdsUri, organization, username, password);
                }
                IDocumentDao dd = factory.GetDocumentDao();
                DaoResult result = dd.GetDocumentsInCategory(domain, new Guid(category_adm), out all);
                if (result.Equals(DaoResult.OK))
                {
                    foreach (Document d in all)
                    {
                        if (!d.SignatureState.Equals(SignatureState.Signed))
                        {
                            //if (!isAWDocDateTimeValid(d.ValidFrom, d.ValidTo))
                            //    notSigned.Add(d);
                            //else
                            //{
                            //    if (MyHelper.GetDateTimeFromTimeStamp(d.ValidFrom) <= MyHelper.GetDateTimeFromYearMonthWithFirstOfMonth(model.year, model.month) && MyHelper.GetDateTimeFromTimeStamp(d.ValidTo) <= MyHelper.GetDateTimeFromYearMonthWithLastOfMonth(model.year, model.month))
                            //    {
                            //        notSigned.Add(d);
                            //    }
                            //}
                            notSigned.Add(d);
                        }
                    }
                }
                DaoResult result2 = dd.GetDocumentsInCategory(domain, new Guid(category_eng), out all);
                if (result.Equals(DaoResult.OK))
                {
                    foreach (Document d in all)
                    {
                        if (!d.SignatureState.Equals(SignatureState.Signed))
                        {
                            //if (!isAWDocDateTimeValid(d.ValidFrom, d.ValidTo))
                            //    notSigned.Add(d);
                            //else
                            //{
                            //    if (MyHelper.GetDateTimeFromTimeStamp(d.ValidFrom) <= MyHelper.GetDateTimeFromYearMonthWithFirstOfMonth(model.year, model.month) && MyHelper.GetDateTimeFromTimeStamp(d.ValidTo) <= MyHelper.GetDateTimeFromYearMonthWithLastOfMonth(model.year, model.month))
                            //    {
                            //        notSigned.Add(d);
                            //    }
                            //}
                            notSigned.Add(d);
                        }
                    }
                }
            }
            return notSigned;
        }

        public List<Document> GetSignedDocumentsInCategory(GetAWDocumentModel model)
        {
            List<Document> all = null;
            List<Document> Signed = new List<Document>();
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    factory.Initialize(gdsUri, organization, username, password);
                }
                IDocumentDao dd = factory.GetDocumentDao();
                DaoResult result = dd.GetDocumentsInCategory(domain, new Guid(category_adm), out all);
                if (result.Equals(DaoResult.OK))
                {
                    foreach (Document d in all)
                    {
                        if (d.SignatureState.Equals(SignatureState.Signed))
                        {
                            //if (!isAWDocDateTimeValid(d.ValidFrom, d.ValidTo))
                            //    Signed.Add(d);
                            //else
                            //{
                            //    if (MyHelper.GetDateTimeFromTimeStamp(d.ValidFrom) <= MyHelper.GetDateTimeFromYearMonthWithFirstOfMonth(model.year, model.month) && MyHelper.GetDateTimeFromTimeStamp(d.ValidTo) <= MyHelper.GetDateTimeFromYearMonthWithLastOfMonth(model.year, model.month))
                            //    {
                            //        Signed.Add(d);
                            //    }
                            //}
                            Signed.Add(d);
                        }
                    }
                }
                DaoResult result2 = dd.GetDocumentsInCategory(domain, new Guid(category_eng), out all);
                if (result.Equals(DaoResult.OK))
                {
                    foreach (Document d in all)
                    {
                        if (d.SignatureState.Equals(SignatureState.Signed))
                        {
                            //if (!isAWDocDateTimeValid(d.ValidFrom, d.ValidTo))
                            //    Signed.Add(d);
                            //else
                            //{
                            //    if (MyHelper.GetDateTimeFromTimeStamp(d.ValidFrom) <= MyHelper.GetDateTimeFromYearMonthWithFirstOfMonth(model.year, model.month) && MyHelper.GetDateTimeFromTimeStamp(d.ValidTo) <= MyHelper.GetDateTimeFromYearMonthWithLastOfMonth(model.year, model.month))
                            //    {
                            //        Signed.Add(d);
                            //    }
                            //}
                            Signed.Add(d);
                        }
                    }
                }
            }
            return Signed;
        }

        public byte[] DownloadDocument(string docGUID)
        {
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    factory.Initialize(gdsUri, organization, username, password);
                }
                IDocumentDao dd = factory.GetDocumentDao();
                DaoResult res = dd.DownloadDocument(domain, Guid.Parse(docGUID), out Stream input);
                if (res.Equals(DaoResult.OK))
                {
                    using (MemoryStream memStream = new MemoryStream())
                    {
                        input.CopyTo(memStream);
                        return memStream.ToArray();
                    }
                }
                return null;
            }
        }

        public byte[] DownloadSignedDocument(string docGUID)
        {
            using (IDaoFactory factory = new RestDaoFactory())
            {
                if (!factory.IsInitialized)
                {
                    factory.Initialize(gdsUri, organization, username, password);
                }
                IDocumentDao dd = factory.GetDocumentDao();
                DaoResult res = dd.DownloadSignedDocument(domain, Guid.Parse(docGUID), out Stream input);
                if (res.Equals(DaoResult.OK))
                {
                    using (MemoryStream memStream = new MemoryStream())
                    {
                        input.CopyTo(memStream);
                        return memStream.ToArray();
                    }
                }
                return null;
            }
        }

        public void getOTP(string phoneNumber)
        {
            string exmsg = string.Empty;
            try
            {
                JObject jobj = JObject.FromObject(new OTPModel()
                {
                    phoneNumber = phoneNumber
                });
                string URL = "http://52.174.63.179/tomcat/feeder/rest/fea/otp";
                Request.PostRequest(URL, jobj.ToString());
            }
            catch (Exception e)
            {
                exmsg = e.Message;
            }
        }

        public bool signDocumentsFEA(FEAModelToken model)
        {
            PersonModelBase person = new Database.DBHandler().getUserFromToken(model.token, model.azure);
            return signDocumentsFEA(model, person);
        }

        public bool signDocumentsFEA(FEAModelToken model, PersonModelBase person)
        {
            bool inError = false;
            Database.DBHandler handler = new Database.DBHandler();
            string exmsg = string.Empty;
            string phone = string.Empty;
            string name = string.Empty;
            string lastName = string.Empty;
            switch (person.type)
            {
                case Constants.UserTypeEnum.Employee:
                    Database.Employee e = handler.getEmployee(person.id).Item1;
                    Database.FirmaElettronicaAvanzata fea = handler.getFEA((long)e.fea).Item1;
                    phone = fea.cellulare;
                    name = e.name;
                    lastName = e.lastName;
                    break;
                case Constants.UserTypeEnum.Operator:
                    phone = Constants.AWDOC_PHONE_TEST_MICHELE;
                    name = "Francesco";
                    lastName = "Melito";
                    break;
                default:
                    Database.Customer c = handler.getCustomer(person.id).Item1;
                    Database.FirmaElettronicaAvanzata feaC = handler.getFEA((long)c.feaId).Item1;
                    if (feaC != null)
                        phone = feaC.cellulare;
                    else
                        phone = handler.getCustomerCompanies(c.ID).Item1.First().phone;
                    name = c.name;
                    lastName = c.lastName;
                    break;
            }
            try
            {
                JToken jobj = JToken.FromObject(new
                {
                    pin = Constants.AWDOC_PASSWORD_RELEASE,
                    docUuids = model.docUuids.First().Split(','),
                    otp = model.pin,
                    domain = Constants.AWDOC_DOMAIN_RELEASE,
                    phoneNumber = phone,
                    signerEmail = person.email,
                    signerName = name + " " + lastName,
                    signerTaxID = person.fiscalCode
                });
                string URL = "http://52.174.63.179/tomcat/feeder/rest/fea/multisign";
                string req = jobj.Root.ToString();
                req = JsonConvert.SerializeObject(jobj);
                using (StreamReader stream = Request.PostRequest(URL, req))
                {
                    MyHelper.AWDLog(new string[] { stream.ReadToEnd() });
                }
            }
            catch (Exception e)
            {
                MyHelper.Log(exmsg = e.Message);
                inError = true;
            }
            return inError;
        }
    }
}