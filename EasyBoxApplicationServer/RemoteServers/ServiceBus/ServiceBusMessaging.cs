﻿using Microsoft.ServiceBus.Messaging;
using System;
using System.Configuration;

namespace EasyBoxApplicationServer.RemoteServers.ServiceBus
{
    public class ServiceBusMessaging
    {
        public string test()
        {
            string connectionString = "Endpoint=sb://ojmservicebus.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=UuyeMcWLgIB58zQUf+KFXvgU5sXMNNmiTMMecHgX+UU=";
            string queueName = "DatabookDefaultTopic/subscriptions/EasyBox";
            string result = string.Empty;
            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["LastMessageTime"]))
                ConfigurationManager.AppSettings["LastMessageTime"] = new DateTime(0001, 1, 1, 0, 0, 0, 0, DateTimeKind.Local).ToString();
            DateTime lastMessageTime = DateTime.Parse(ConfigurationManager.AppSettings["LastMessageTime"]);
            DateTime date = new DateTime(0001,1,1,0,0,0,0,DateTimeKind.Local);

            var client = QueueClient.CreateFromConnectionString(connectionString, queueName);

            client.OnMessage(message =>
            {
               date = message.EnqueuedTimeUtc;
            });
            if (date != null && date.Ticks > lastMessageTime.Ticks)
            {
                ConfigurationManager.AppSettings["LastMessageTime"] = date.ToString();
                result = "Updated. Last message time: " + date;
            }
            else
            {
                result = "No new Messages";
            }
            return result;
        }
    }
}