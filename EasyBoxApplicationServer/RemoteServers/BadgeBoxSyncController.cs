﻿using EasyBoxApplicationServer.Database;
using EasyBoxApplicationServer.Models.BadgeBox.Models;
using EasyBoxApplicationServer.Models.BadgeBox.Responses;
using EasyBoxApplicationServer.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace EasyBoxApplicationServer.RemoteServers
{
    public class BadgeBoxSyncController
    {
        public void syncronizeBB()
        {
            try
            {
                //per ogni owner
                List<Customer> owners = new DBHandler().getOwners().Item1;
                MyHelper.Log("NUMERO OWNER: " + owners.Count);
                foreach (Customer c in owners)
                {
                    List<Company> ownerCompanies = new DBHandler().getCustomerCompanies(c.ID).Item1;
                    MyHelper.Log("COMPAGNIE OWNER " + c.name + ": " + ownerCompanies.Count);
                    if (!c.isBBSync)
                    {
                        MyHelper.Log("SINCRONIZZO " + c.name);
                        //sincronizziamo l'owner non ancora sincronizzato
                        Tuple<BadgeBoxCreateUserResponse, string> BBResponse = null;
                        BBResponse = BadgeBoxServer.createUser(new BadgeBoxUserModel()
                        {
                            email = c.email,
                            name = c.name,
                            surname = c.lastName,
                            password = c.password
                        });
                        if (BBResponse.Item1 != null && !string.IsNullOrEmpty(BBResponse.Item1.token))
                        {
                            MyHelper.Log("AGGIORNO " + c.name);
                            new DBHandler().setCustomerBBSync(c.ID, BBResponse.Item1);
                            //sincronizziamo la/e sua/e azienda/e
                            foreach (Company comp in ownerCompanies)
                            {
                                Address addressObj = new DBHandler().getAddressByID((long)comp.addressId).Item1;
                                List<Employee> employees = new DBHandler().getCompanyEmployees(comp.ID).Item1;

                                MyHelper.Log("ID ADDRESS: " + addressObj.ID);
                                MyHelper.Log("NUMERO LAVORATORI: " + employees.Count);
                                if (addressObj != null)
                                {
                                    MyHelper.Log("SINCRONIZZO COMPAGNIA");
                                    Tuple<BadgeBoxCreateCompanyResponse, string> resp = BadgeBoxServer.createCompany(new BadgeBoxCreateCompanyModel()
                                    {
                                        user_token = BBResponse.Item1.token,
                                        name = comp.businessName,
                                        email = c.email,
                                        a_email = comp.email,
                                        vat = comp.vat,
                                        address = addressObj.street,
                                        cap = addressObj.zip,
                                        city = addressObj.city
                                    });
                                    if (string.IsNullOrEmpty(resp.Item2) && resp.Item1 != null && resp.Item1.id_agency != 0)
                                    {
                                        MyHelper.Log("AGGIORNO COMPAGNIA");
                                        new DBHandler().setCompanyBBSync(comp.ID, resp.Item1.id_agency);
                                        //sincronizziamo tutti i dipendenti
                                        foreach (Employee emp in employees)
                                        {
                                            BadgeBoxAddEmployeeResponse createdEmployee = BadgeBoxServer.addEmployee(new BadgeBoxAddEmployeeModel()
                                            {
                                                user_token = c.BBToken,
                                                email = emp.ID + "@easybox.it",
                                                id_agency = resp.Item1.id_agency,
                                                name = emp.name,
                                                surname = emp.lastName
                                            }).Item1;
                                            if (createdEmployee != null && createdEmployee.id > 0)
                                                MyHelper.Log("AGGIORNO EMPLOYEE: " + emp.name);
                                                new DBHandler().setEmployeeBBSync(emp.ID, c.BBToken, createdEmployee.id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //sincronizziamo l'azienda non sincronizzata
                        foreach (Company comp in ownerCompanies)
                        {
                            List<Employee> employees = new DBHandler().getCompanyEmployees(comp.ID).Item1;
                            if (!comp.isBBSync)
                            {
                                Address addressObj = new DBHandler().getAddressByID((long)comp.addressId).Item1;
                                if (addressObj != null)
                                {
                                    Tuple<BadgeBoxCreateCompanyResponse, string> resp = BadgeBoxServer.createCompany(new BadgeBoxCreateCompanyModel()
                                    {
                                        user_token = c.BBToken,
                                        name = comp.businessName,
                                        email = c.email,
                                        a_email = comp.email,
                                        vat = comp.vat,
                                        address = addressObj.street,
                                        cap = addressObj.zip,
                                        city = addressObj.city
                                    });
                                    if (string.IsNullOrEmpty(resp.Item2) && resp.Item1 != null && resp.Item1.id_agency != 0)
                                    {
                                        new DBHandler().setCompanyBBSync(comp.ID, resp.Item1.id_agency);
                                        //sincronizziamo tutti i dipendenti
                                        foreach (Employee emp in employees)
                                        {
                                            {
                                                BadgeBoxAddEmployeeResponse createdEmployee = BadgeBoxServer.addEmployee(new BadgeBoxAddEmployeeModel()
                                                {
                                                    user_token = c.BBToken,
                                                    email = emp.ID + "@easybox.it",
                                                    id_agency = resp.Item1.id_agency,
                                                    name = emp.name,
                                                    surname = emp.lastName
                                                }).Item1;
                                                if (createdEmployee != null && createdEmployee.id > 0)
                                                    new DBHandler().setEmployeeBBSync(emp.ID, c.BBToken, createdEmployee.id);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //sincronizziamo tutti i dipendenti non sincronizzati
                                foreach (Employee emp in employees)
                                {
                                    if (!emp.isBBSync)
                                    {
                                        {
                                            BadgeBoxAddEmployeeResponse createdEmployee = BadgeBoxServer.addEmployee(new BadgeBoxAddEmployeeModel()
                                            {
                                                user_token = c.BBToken,
                                                email = emp.ID + "@easybox.it",
                                                id_agency = (int)comp.BBId.Value,
                                                name = emp.name,
                                                surname = emp.lastName
                                            }).Item1;
                                            if (createdEmployee != null && createdEmployee.id > 0)
                                                new DBHandler().setEmployeeBBSync(emp.ID, c.BBToken, createdEmployee.id);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MyHelper.Log(e.StackTrace);
            }
        }
    }
}