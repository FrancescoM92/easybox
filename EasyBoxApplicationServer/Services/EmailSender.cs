﻿using EasyBoxApplicationServer.Utils;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.IO;

namespace EasyBoxApplicationServer.Services
{
    public class EmailSender
    {
        private static string easybox = "http://easybox.staging.openjobmetis.it/";
        private static string agribox = "http://easyboxforagri.openjobmetis.it/";
        private static string easybox_html_path = "C:/inetpub/easyboxServer/Assets/mail easybox.html";
        private static string agribox_html_path = "C:/inetpub/easyboxServer/Assets/mail agribox.html";

        public static void sendEmail(string userEmail, string subject, string body, bool isEasy)
        {
            MyHelper.Log("START SEND MAIL");

            var html = string.Empty;
            try
            {
                html = File.ReadAllText(isEasy ? easybox_html_path : agribox_html_path);
                html = html.Replace("{BODY}", body);
                html = html.Replace("{BUTTON_LINK}", isEasy ? easybox : agribox);
                html = html.Replace("{TITLE}", isEasy ? "EASYBOX" : "AGRIBOX");
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }

            RestResponse rs = null;
            try
            {
                RestClient client = new RestClient()
                {
                    BaseUrl = new Uri("https://api.mailgun.net/v3")
                };
                client.Authenticator = new HttpBasicAuthenticator("api", "key-ba653a694136aa02eca30310a04488fe");

                RestRequest request = new RestRequest();
                request.AddParameter("domain", "mail.openjobmetis.it", ParameterType.UrlSegment);
                request.Resource = "mail.openjobmetis.it/messages";
                request.AddParameter("from", isEasy ? "EasyBox" + " <no-reply@openjobmetis.it>" : "AgriBox" + " <no-reply@openjobmetis.it>");
                request.AddParameter("to", userEmail);
                request.AddParameter("subject", subject);
                request.AddParameter("html", html);
                request.Method = Method.POST;
                rs = (RestResponse)client.Execute(request);

                MyHelper.Log("CONTENT: " + rs.Content + " ERROR: " + rs.ErrorMessage + "EXC: " + rs.ErrorException);
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
        }

        public static void sendEmailMichele(string subject, string body, bool isEasy)
        {
            MyHelper.Log("START SEND MAIL");

            var html = body;
            //try
            //{
            //    html = File.ReadAllText(isEasy ? easybox_html_path : agribox_html_path);
            //    html = html.Replace("{BODY}", body);
            //    html = html.Replace("{BUTTON_LINK}", isEasy ? easybox : agribox);
            //    html = html.Replace("{TITLE}", isEasy ? "EASYBOX" : "AGRIBOX");
            //}
            //catch (Exception e)
            //{
            //    MyHelper.Log(e.Message);
            //}

            RestResponse rs = null;
            try
            {
                RestClient client = new RestClient()
                {
                    BaseUrl = new Uri("https://api.mailgun.net/v3")
                };
                client.Authenticator = new HttpBasicAuthenticator("api", "key-ba653a694136aa02eca30310a04488fe");

                RestRequest request = new RestRequest();
                request.AddParameter("domain", "mail.openjobmetis.it", ParameterType.UrlSegment);
                request.Resource = "mail.openjobmetis.it/messages";
                request.AddParameter("from", isEasy ? "EasyBox" + " <no-reply@openjobmetis.it>" : "AgriBox" + " <no-reply@openjobmetis.it>");
                request.AddParameter("to", "michele.solari@openjob.it");
                request.AddParameter("to", "melito.francesco.92@gmail.com");
                request.AddParameter("subject", subject);
                request.AddParameter("html", html);
                request.Method = Method.POST;
                rs = (RestResponse)client.Execute(request);

                MyHelper.Log("CONTENT: " + rs.Content + " ERROR: " + rs.ErrorMessage + "EXC: " + rs.ErrorException);

            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
        }

        public static void sendEmailPasswordRecover(string userEmail, string subject, string token, bool isEasy)
        {
            MyHelper.Log("START SEND MAIL");

            var html = string.Empty;
            try
            {
                html = File.ReadAllText(isEasy ? easybox_html_path : agribox_html_path);
                html = html.Replace("{BODY}", MyHelper.GetLocalizedString("VerifyEmailTokenMessage1") + "<a href=\"" + token + "\">" + MyHelper.GetLocalizedString("VerifyEmailTokenMessage2") + "</a>");
                html = html.Replace("{BUTTON_LINK}", isEasy ? easybox : agribox);
                html = html.Replace("{TITLE}", isEasy ? "EASYBOX" : "AGRIBOX");
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }

            RestResponse rs = null;
            try
            {
                RestClient client = new RestClient()
                {
                    BaseUrl = new Uri("https://api.mailgun.net/v3")
                };
                client.Authenticator = new HttpBasicAuthenticator("api", "key-ba653a694136aa02eca30310a04488fe");

                RestRequest request = new RestRequest();
                request.AddParameter("domain", "mail.openjobmetis.it", ParameterType.UrlSegment);
                request.Resource = "mail.openjobmetis.it/messages";
                request.AddParameter("from", isEasy ? "EasyBox" + " <no-reply@openjobmetis.it>" : "AgriBox" + " <no-reply@openjobmetis.it>");
                request.AddParameter("to", "michele.solari@openjob.it");
                request.AddParameter("to", "melito.francesco.92@gmail.com");
                request.AddParameter("subject", subject);
                request.AddParameter("html", html);
                request.Method = Method.POST;
                rs = (RestResponse)client.Execute(request);

                MyHelper.Log("CONTENT: " + rs.Content + " ERROR: " + rs.ErrorMessage + "EXC: " + rs.ErrorException);

            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }


        }

        public static void sendEmailPassword(string userEmail, string subject, string token, bool isEasy)
        {
            MyHelper.Log("START SEND MAIL");

            var html = string.Empty;
            try
            {
                html = File.ReadAllText(isEasy ? easybox_html_path : agribox_html_path);
                html = html.Replace("{BODY}", "Congratulazioni, questa è la sua nuova password di accesso all'area riservata: " + token);
                html = html.Replace("{BUTTON_LINK}", isEasy ? easybox : agribox);
                html = html.Replace("{TITLE}", isEasy ? "EASYBOX" : "AGRIBOX");
            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
            RestResponse rs = null;
            try
            {
                RestClient client = new RestClient()
                {
                    BaseUrl = new Uri("https://api.mailgun.net/v3")
                };
                client.Authenticator = new HttpBasicAuthenticator("api", "key-ba653a694136aa02eca30310a04488fe");

                RestRequest request = new RestRequest();
                request.AddParameter("domain", "mail.openjobmetis.it", ParameterType.UrlSegment);
                request.Resource = "mail.openjobmetis.it/messages";
                request.AddParameter("from", isEasy ? "EasyBox" + " <no-reply@openjobmetis.it>" : "AgriBox" + " <no-reply@openjobmetis.it>");
                request.AddParameter("to", "michele.solari@openjob.it");
                request.AddParameter("to", "melito.francesco.92@gmail.com");
                request.AddParameter("subject", subject);
                request.AddParameter("html", html);
                request.Method = Method.POST;
                rs = (RestResponse)client.Execute(request);

                MyHelper.Log("CONTENT: " + rs.Content + " ERROR: " + rs.ErrorMessage + "EXC: " + rs.ErrorException);

            }
            catch (Exception e)
            {
                MyHelper.Log(e.Message);
            }
        }
    }
}